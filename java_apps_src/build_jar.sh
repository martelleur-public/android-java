#!/bin/bash

COMPILE_DIR=out
BUILD_DIR=build
MANIFESTO_DIR=META-INF

# Associated arreys, key = Java folder/package name, 
# value = Java main class
declare -A apps
apps["server"]="ServerApp"
apps["client"]="ClientApp"
apps["draw_datagram_socket_p2p"]="DrawApp"
apps["client_sender"]="ClientSender"
apps["client_receiver"]="ClientReceiver"

# Create build dir, if old dir exist remove it first.
if [ -d ${BUILD_DIR} ]; then
    rm -Rf ${BUILD_DIR}
fi
mkdir -p ${BUILD_DIR}

# Copy compiled files to separated build folders.
for dir in "${!apps[@]}"; do
    APP_NAME="${apps[$dir]}.class"
    mkdir -p ${BUILD_DIR}/${dir}/${MANIFESTO_DIR}

    # Copy Java main folder and files if exist.
    if [ -d ${COMPILE_DIR}/${dir} ]; then
    cp -r ${COMPILE_DIR}/${dir} \
        ${BUILD_DIR}/${dir}
    fi
    
    # Copy Java main class
    cp ${COMPILE_DIR}/${APP_NAME} \
        ${BUILD_DIR}/${dir}
done

# Create manifest files and build jar files.
cd ${BUILD_DIR}
for dir in "${!apps[@]}"; do
    # Change to fir and store app_name in variable.
    APP_NAME="${apps[$dir]}"
    cd ${dir}

    # Create Mainfesto file.   
    echo Main-Class: ${APP_NAME} > \
        ${MANIFESTO_DIR}/MANIFEST.MF

    # Build the jar file.
    jar cvfm "${APP_NAME}.jar" \
        ${MANIFESTO_DIR}/MANIFEST.MF \
        *
    
    # cd back
    cd ..
done

# Draw app notes

## Test DrawApp (.class) two terminals on same machine

- __$1__```java DrawApp 2000 localhost 2001```

- __$2__```java DrawApp 2001 localhost 2000```

## Test DrawApp (.jar) two terminals on same machine

- __$1__```java -jar DrawApp.jar 2000 localhost 2001```

- __$2__```java -jar DrawApp.jar 2001 localhost 2000```

# Java Source

Java applikationer, e.g., en simpel java server, som bland annat kan användas för att testa Android applikationerna lokalt.

# Java apps

## Compile and Creating a jar Files

1. __Compile your class(es):__ 
```./compile.sh```


2. __Create a manifest file and jar file:__

```./build_jar.sh```


3. __Test jar in build folder:__

```<path_to_jar_file>.jar```

or

```java -jar <path_to_jar_file>.jar```
#!/bin/bash

# Run this script from root directory src.
ROOT_DIR="src"
COMPILE_DIR="out"

# Cd to rootfolder
cd $ROOT_DIR

# Compile all packages in src
javac -d ../$COMPILE_DIR/ ./**/*.java

# Compile all java files directly in root folder src
javac -d ../$COMPILE_DIR/ ./*.java


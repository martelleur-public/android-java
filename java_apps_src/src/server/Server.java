package server;

import java.net.UnknownHostException;
import java.net.InetAddress;
import java.io.PrintWriter;
import java.net.Socket;
import java.io.IOException;
import javax.swing.JScrollPane;
import java.awt.event.WindowListener;
import javax.swing.JTextArea;
import java.util.Vector;
import java.net.ServerSocket;
import javax.swing.JFrame;

import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

public class Server extends JFrame implements Runnable, ServerCallback {
    private Thread thread;
    private ServerSocket ss;
    private Vector<Socket> clientVector;
    public static int port;
    public static boolean visible;
    private JTextArea textArea;
    
    public Server() {
        this.thread = new Thread(this);
        this.clientVector = new Vector<Socket>();
        this.textArea = new JTextArea();
        if (Server.visible) {
            // this.addWindowListener((WindowListener) new Server.L(this));
            this.addWindowListener((WindowListener) new WindowAdapter() {
                @Override
                public void windowClosing(final WindowEvent windowEvent) {
                    dispose();
                }
                
                @Override
                public void windowClosed(final WindowEvent windowEvent) {
                    System.exit(1);
                }        
            });
            this.getContentPane().add(new JScrollPane(this.textArea));
            this.setLocation(0, 0);
            this.setSize(400, 200);
            this.setVisible(true);
        }
        try {
            this.ss = new ServerSocket(Server.port);
            this.setTitle(0);
        }
        catch (IOException e) {
            System.out.println("FAILED: COULD NOT LISTEN ON PORT: " + Server.port);
            System.exit(1);
        }
        this.thread.start();
    }
    
    @Override
    public void run() {
        while (true) {
            Socket acceptedClientSocket = null;
            try {
                acceptedClientSocket = this.ss.accept();
            }
            catch (IOException e) {
                System.out.println("FAILED: ACCEPTING CLIENT");
            }
            this.addClient(acceptedClientSocket);
        }
    }
    
    private synchronized void addClient(final Socket socket) {
        this.broadcast(socket, "CLIENT CONNECTED: " + this.getHost(socket));
        this.clientVector.addElement(socket);
        this.setTitle(this.clientVector.size());
        new ClientHandler(socket, this);
    }
    
    public synchronized void removeClient(final Socket socket) {
        this.clientVector.removeElement(socket);
        this.broadcast(socket, "CLIENT DISSCONNECTED: " + this.getHost(socket));
        this.setTitle(this.clientVector.size());
    }
    
    public synchronized void broadcast(final Socket socket, final String str) {
        this.textArea.append("CLIENT: " + this.getHost(socket) + " BROADCAST: " + str + "\n");
        for (int i = 0; i < this.clientVector.size(); ++i) {
            this.sendToOne((Socket)this.clientVector.elementAt(i), str);
        }
    }
    
    private synchronized void sendToOne(final Socket socket, final String x) {
        try {
            new PrintWriter(socket.getOutputStream(), true).println(x);
        }
        catch (IOException e) {
            System.out.println("FAILED: SEND TO ONE");
        }
    }
    
    public synchronized void who(final Socket socket) {
        this.textArea.append("CLIENT: " + this.getHost(socket) + " ASK: wwhhoo" + "\n");
        for (int i = 0; i < this.clientVector.size(); ++i) {
            this.sendToOne(socket, "WWHHOO: " + this.getHost((Socket)this.clientVector.elementAt(i)));
        }
    }
    
    private String getHost(final Socket socket) {
        return socket.getInetAddress().getHostName();
    }
    
    private String getHost(final ServerSocket serverSocket) {
        try {
            serverSocket.getInetAddress();
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e) {
            return "FAILED: GET SERVER HOST";
        }
    }
    
    private void setTitle(final int i) {
        this.setTitle("JAVA CHAT SERVER ON: " + this.getHost(this.ss) + " - PORT: " + Server.port + " - N CLIENTS: " + i);
    }
    
    static {
        Server.port = 2000;
        Server.visible = true;
    }
}

package server;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

class ClientHandler extends Thread {
    private ServerCallback serverCallback;
    private Socket s;

    public ClientHandler(final Socket s, final ServerCallback serverCallback) {
        this.s = s;
        this.serverCallback = serverCallback;
        this.start();
    }

    @Override
    public void run() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(this.s.getInputStream()));
        }
        catch (IOException e) {
            System.out.println("FAILED: GET INPUTSTREAM FROM CLIENT SOCKET");
            this.serverCallback.removeClient(this.s);
            return;
        }
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals("wwhhoo")) {
                    this.serverCallback.who(this.s);
                }
                else {
                    this.serverCallback.broadcast(this.s, line);
                }
            }
            bufferedReader.close();
            this.s.close();
            System.out.println("CLIENT DISCONNECTED NICELY");
        }
        catch (IOException e) {
            System.out.println("CLIENT DISCONNECTED BRUTALLY");
        }
        this.serverCallback.removeClient(this.s);
    }
}

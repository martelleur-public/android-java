package server;

import java.net.Socket;

interface ServerCallback {
    void removeClient(Socket socket);
    void who(Socket socket);
    void broadcast(Socket socket, String line); 
}

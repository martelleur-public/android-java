import java.io.IOException;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.net.Socket;

public class ClientReceiver
{
    private boolean running;
    private Socket s;
    private BufferedReader in;
    private static String host;
    private static int port;
    
    public static void main(final String[] array) {
        if (array.length != 0) {
            ClientReceiver.host = array[0];
            if (array.length != 1) {
                ClientReceiver.port = Integer.parseInt(array[1]);
            }
        }
        new ClientReceiver();
    }
    
    public ClientReceiver() {
        this.running = true;
        this.s = null;
        this.in = null;
        try {
            this.s = new Socket(ClientReceiver.host, ClientReceiver.port);
            System.out.println("CONNECTED TO: " + ClientReceiver.host + " - ON PORT: " + ClientReceiver.port);
            this.in = new BufferedReader(new InputStreamReader(this.s.getInputStream()));
            this.receiveAndShow();
        }
        catch (IOException obj) {
            System.out.println("IOException generated: " + obj);
        }
    }
    
    public void receiveAndShow() {
        String line = "";
        while (this.running) {
            try {
                line = this.in.readLine();
            }
            catch (NullPointerException obj) {
                System.out.println("NullPointerException generated: " + obj);
                this.killMe();
            }
            catch (IOException obj2) {
                System.out.println("IOException generated: " + obj2);
                this.killMe();
            }
            System.out.println("Meddelande: " + line);
        }
    }
    
    public void killMe() {
        this.running = false;
        try {
            this.in.close();
            this.s.close();
        }
        catch (IOException obj) {
            System.out.println("IOException generated: " + obj);
        }
        System.exit(1);
    }
    
    static {
        ClientReceiver.host = "127.0.0.1";
        ClientReceiver.port = 2000;
    }
}

package draw_datagram_socket_p2p;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JPanel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

public class Paper extends JPanel {
    private Net net;
    HashSet<Point> hs = new HashSet<Point>();
    public static final int STOP_X = -1;
    public static final int STOP_Y = -1;

    public Paper(String[] arrstring) {
        this.setBackground(Color.white);

        this.addMouseListener((MouseListener) new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
                addPoint(mouseEvent.getPoint());
            }        

            public void mouseReleased(MouseEvent mouseEvent) {
                addPoint(new Point(STOP_X, STOP_Y));
            }

        });

        this.addMouseMotionListener((MouseMotionListener) new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent mouseEvent) {
                addPoint(mouseEvent.getPoint());
            }
        });

        this.net = new Net(this, arrstring);
    }

    public synchronized void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        graphics.setColor(Color.black);
        Iterator<Point> iterator = this.hs.iterator();
        while (iterator.hasNext()) {
            Point point = (Point)iterator.next();
            graphics.fillOval(point.x, point.y, 2, 2);
        }
    }

    public synchronized void addPoint(Point point) {
        this.hs.add(point);
        this.repaint();
        this.net.send(Integer.toString(point.x) + " " + Integer.toString(point.y));
    }

    public synchronized void addPoint(String string) {
        String[] arrstring = string.split(" ");
        Point point = new Point(Integer.parseInt(arrstring[0]), Integer.parseInt(arrstring[1]));
        this.hs.add(point);
        this.repaint();
    }
}

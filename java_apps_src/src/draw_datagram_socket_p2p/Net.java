package draw_datagram_socket_p2p;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Net extends Thread {
    private Paper paper;
    private String host;
    private int myPort;
    private int toPort;

    public Net(Paper paper, String[] arrstring) {
        this.paper = paper;
        this.myPort = Integer.parseInt(arrstring[0]);
        this.host = arrstring[1];
        this.toPort = Integer.parseInt(arrstring[2]);
        this.start();
    }

    public void send(String string) {
        try {
            byte[] arrby = string.getBytes();
            InetAddress inetAddress = InetAddress.getByName(this.host);
            DatagramPacket datagramPacket = new DatagramPacket(arrby, arrby.length, inetAddress, this.toPort);
            DatagramSocket datagramSocket = new DatagramSocket();
            datagramSocket.send(datagramPacket);
        }
        catch (Exception exception) {
            System.out.println(exception);
        }
    }

    public void run() {
        try {
            DatagramSocket datagramSocket = new DatagramSocket(this.myPort);
            while (true) {
                byte[] arrby = new byte[8192];
                DatagramPacket datagramPacket = new DatagramPacket(arrby, arrby.length);
                datagramSocket.receive(datagramPacket);
                String string = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                this.paper.addPoint(string);
            }
        }
        catch (Exception exception) {
            System.out.println(exception);
            return;
        }
    }
}
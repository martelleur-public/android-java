import client.Client;

public class ClientApp {
    public static void main(final String[] array) {
        if (array.length != 0) {
            Client.host = array[0];
            if (array.length != 1) {
                Client.port = Integer.parseInt(array[1]);
            }
        }
        new Client();
    }
}
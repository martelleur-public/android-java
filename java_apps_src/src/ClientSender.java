import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientSender
{
    private boolean running;
    private Socket s;
    private PrintWriter out;
    private static String host;
    private static int port;
    
    public static void main(final String[] array) {
        if (array.length != 0) {
            ClientSender.host = array[0];
            if (array.length != 1) {
                ClientSender.port = Integer.parseInt(array[1]);
            }
        }
        new ClientSender();
    }
    
    public ClientSender() {
        this.running = true;
        this.s = null;
        this.out = null;
        try {
            this.s = new Socket(ClientSender.host, ClientSender.port);
            System.out.println("CONNECTED TO: " + ClientSender.host + " - ON PORT: " + ClientSender.port);
            this.out = new PrintWriter(this.s.getOutputStream(), true);
            this.inputAndSend();
        }
        catch (IOException obj) {
            System.out.println("IOException generated: " + obj);
        }
    }
    
    public void inputAndSend() {
        final Scanner scanner = new Scanner(System.in);
        while (this.running) {
            System.out.print("Ange meddelande: ");
            final String nextLine = scanner.nextLine();
            if (nextLine.equals("exit")) {
                this.killMe();
            }
            else {
                this.out.println(nextLine);
            }
        }
    }
    
    public void killMe() {
        this.running = false;
        try {
            this.out.close();
            this.s.close();
        }
        catch (IOException obj) {
            System.out.println("IOException generated: " + obj);
        }
        System.exit(1);
    }
    
    static {
        ClientSender.host = "127.0.0.1";
        ClientSender.port = 2000;
    }
}

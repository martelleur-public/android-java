
import draw_datagram_socket_p2p.*;

import java.awt.Component;
import javax.swing.JFrame;

public class DrawApp
extends JFrame {
    private Paper p;

    public static void main(String[] arrstring) {
        new DrawApp(arrstring);
    }

    public DrawApp(String[] arrstring) {
        this.setDefaultCloseOperation(3);
        this.p = new Paper(arrstring);
        this.getContentPane().add((Component)this.p, "Center");
        this.setSize(640, 480);
        this.setVisible(true); // show();
    }
}
package client;

import java.net.UnknownHostException;
import java.net.InetAddress;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.Reader;
import java.io.InputStreamReader;
import java.awt.event.WindowListener;
import javax.swing.JScrollPane;
import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import javax.swing.JFrame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Client 
extends JFrame 
implements Runnable
{
    private Thread thread;
    private boolean running;
    private Socket s;
    private PrintWriter out;
    private BufferedReader in;
    public static String host;
    public static int port;
    private JTextField textField;
    private JTextArea textArea;
    
    public Client() {
        this.thread = new Thread(this);
        this.running = true;
        this.s = null;
        this.out = null;
        this.in = null;
        this.textField = new JTextField();
        this.textArea = new JTextArea();
        
        this.textField.addActionListener((ActionListener) new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent actionEvent) {
                Client.this.sendMessage(Client.this.textField.getText());
                Client.this.textField.setText("");
            }
        });
        
        this.getContentPane().add("North", this.textField);
        this.getContentPane().add("Center", new JScrollPane(this.textArea));
        this.setLocation(420, 0);
        this.setSize(400, 200);
        
        this.addWindowListener((WindowListener) new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent windowEvent) {
                dispose();
            }
            
            @Override
            public void windowClosed(final WindowEvent windowEvent) {
                System.exit(1);
            }
        });
        
        this.setVisible(true);
        try {
            this.s = new Socket(Client.host, Client.port);
            this.setTitle("CHAT CLIENT CONNECTED TO: " + Client.host + " - ON PORT: " + Client.port);
            this.in = new BufferedReader(new InputStreamReader(this.s.getInputStream()));
            this.out = new PrintWriter(this.s.getOutputStream(), true);
        }
        catch (IOException obj) {
            System.out.println("COULD NOT CONNECT: " + obj);
            this.killMe();
        }
        this.thread.start();
    }
    
    @Override
    public void run() {
        String line = "";
        while (this.running) {
            try {
                line = this.in.readLine();
            }
            catch (NullPointerException obj) {
                System.out.println("NullPointer generated: " + obj);
                System.out.println("Stopping client!");
                this.running = false;
                this.killMe();
            }
            catch (IOException obj2) {
                System.out.println("IOException generated: " + obj2);
                System.out.println("Stopping client!");
                this.running = false;
                this.killMe();
            }
            this.textArea.append(line + "\n");
            this.textArea.setCaretPosition(this.textArea.getText().length());
            Toolkit.getDefaultToolkit().beep();
        }
    }
    
    private void sendMessage(final String x) {
        this.out.println(x);
        if (x.equals("exit")) {
            this.killMe();
        }
    }
    
    private String getHost(final Socket socket) {
        try {
            socket.getInetAddress();
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException ex) {
            return "unknown";
        }
    }
    
    public void killMe() {
        this.running = false;
        this.dispose();
        try {
            this.out.close();
            this.in.close();
            this.s.close();
        }
        catch (IOException obj) {
            System.out.println("IOException generated: " + obj);
        }
        System.exit(1);
    }
    
    static {
        Client.host = "127.0.0.1";
        Client.port = 2000;
    }
}

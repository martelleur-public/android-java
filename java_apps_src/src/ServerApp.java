

import server.Server;

public class ServerApp {
    public static void main(final String[] array) {
        if (array.length != 0) {
            Server.port = Integer.parseInt(array[0]);
            if (array.length != 1 && array[1].equals("nv")) {
                Server.visible = false;
            }
        }
        new Server();
    }
}

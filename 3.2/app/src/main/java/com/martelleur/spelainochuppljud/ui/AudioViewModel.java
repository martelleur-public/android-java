package com.martelleur.spelainochuppljud.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.spelainochuppljud.model.AudioRecordCallback;
import com.martelleur.spelainochuppljud.model.AudioRepository;
import com.martelleur.spelainochuppljud.model.AudioItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * View model class used to keep audio livedata and current
 * state of the application. The class is also used to
 * communicate app functionality with the repository for
 * persistent data.
 */
public class AudioViewModel extends ViewModel {
    AudioRepository repository;
    private boolean isRecording = false;
    private boolean isPlaying = false;

    private final MutableLiveData<List<AudioItem>> audioItemsLiveData =
            new MutableLiveData<>(new ArrayList<>());

    public AudioViewModel(AudioRepository repository) {
        this.repository = repository;
        audioItemsLiveData.setValue(this.repository.getAllAudioItems());
    }

    public MutableLiveData<List<AudioItem>> getAudioItemsLiveData() {
        return audioItemsLiveData;
    }

    public void startRecordAudio(String fileName) {
        if (!isRecording) {
            isRecording = true;
            repository.startRecordAudio(fileName);
        }
    }

    /**
     * Used to stop audio.
     */
    public void stopRecordAudio() {
        if (isRecording) {
            isRecording = false;
            repository.stopRecordAudio(audioRecordCallback);
        }
    }

    /**
     * Used as callback to be used when audio recording is stopped.
     */
    private final AudioRecordCallback audioRecordCallback = new AudioRecordCallback() {
        @Override
        public void onStopRecording() {
            audioItemsLiveData.setValue(repository.getAllAudioItems());
        }
    };

    /**
     * Used to play audio.
     */
    public void playAudio(AudioItem audioItem) {
        if (!isPlaying) {
            isPlaying = true;
            updateAudioItemsLiveData(audioItem);
            repository.playAudio(audioItem.getName());
        }
    }

    /**
     * Used to update audio items livedata.
     */
    private void updateAudioItemsLiveData(AudioItem currentAudioItem) {
        List<AudioItem> audioItems = getAudioItemsLiveData().getValue();
        if (audioItems != null) {
            audioItems.forEach(item -> {
                item.setPlaying(Objects.equals(currentAudioItem.getName(), item.getName()));
            });
            audioItemsLiveData.setValue(audioItems);
        }
    }

    /**
     * Used to stop playing audio.
     */
    public void stopAudio() {
        if (isPlaying) {
            isPlaying = false;
            setAllAudioItemsToNotPlaying();
            repository.stopAudio();
        }
    }

    /**
     * Used to stop audio for all audio items.
     */
    private void setAllAudioItemsToNotPlaying() {
        List<AudioItem> audioItems = getAudioItemsLiveData().getValue();
        if (audioItems != null) {
            audioItems.forEach(item -> {
                item.setPlaying(false);
            });
            audioItemsLiveData.setValue(audioItems);
        }
    }
}

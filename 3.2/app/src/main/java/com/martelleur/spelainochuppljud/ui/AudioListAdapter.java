package com.martelleur.spelainochuppljud.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.spelainochuppljud.R;
import com.martelleur.spelainochuppljud.model.AudioItem;

import java.util.List;

/**
 * An adapter used when displaying recorded audio files.
 */
public class AudioListAdapter  extends RecyclerView.Adapter<AudioListAdapter.ViewHolder> {

    final private int audioItemLayout;
    private List<AudioItem> audioItems;
    private final AudioItemCallback audioItemCallback;

    public AudioListAdapter(int layoutId,
                            List<AudioItem> audioItems,
                            AudioItemCallback audioItemCallback) {
        audioItemLayout = layoutId;
        this.audioItems = audioItems;
        this.audioItemCallback = audioItemCallback;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setAudioList(List<AudioItem> audioFiles) {
        this.audioItems = audioFiles;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of audio items.
     */
    @Override
    public int getItemCount() {
        return audioItems == null ? 0 : audioItems.size();
    }

    /**
     * Inflate layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(audioItemLayout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update audio item view and add listeners.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        Button playButton = holder.playButton;
        Button stopButton = holder.stopButton;
        TextView audioFileName = holder.audioFileName;
        String fileNameContent = audioItems.get(listPosition).getName();
        audioFileName.setText(fileNameContent);
        updateAudioItemView(audioItems.get(listPosition).isPlaying(), playButton, stopButton);
        setupPlayListener(audioItems.get(listPosition), playButton, stopButton);
    }

    /**
     * Update audio items. If audio item  is playing show
     * a stop button, else show a play button
     */
    private void updateAudioItemView(boolean isAudioItemPlaying,
                                     Button playButton,
                                     Button stopButton) {
        if (isAudioItemPlaying) {
            stopButton.setVisibility(View.VISIBLE);
            playButton.setVisibility(View.GONE);
        } else {
            stopButton.setVisibility(View.GONE);
            playButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Add listeners on stop and play button that communicate back
     * to the caller when event is happening with a callback object.
     */
    private void setupPlayListener(AudioItem audioItem,
                                   Button playButton,
                                   Button stopButton) {
        playButton.setOnClickListener(view ->
                audioItemCallback.onPlayAudio(audioItem));
        stopButton.setOnClickListener(view ->
                audioItemCallback.onStopAudio());
    }

    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        Button playButton;
        Button stopButton;
        TextView audioFileName;
        ViewHolder(View itemView) {
            super(itemView);
            playButton = itemView.findViewById(R.id.play_button);
            stopButton = itemView.findViewById(R.id.stop_button);
            audioFileName = itemView.findViewById(R.id.file_name_input_container);
        }
    }
}

package com.martelleur.spelainochuppljud.infrastructure;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.martelleur.spelainochuppljud.R;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class used to store and acquire persistent audio files.
 */
public class AudioManager extends FileProvider {
    String TAG = "AudioManager";
    private Context context;
    private String audioDirName;
    private MediaRecorder mediaRecorder;
    private MediaPlayer mediaPlayer;

    public AudioManager() {
        super(R.xml.audio);
    }

    public void initialize(Application application,
                           String audioDirName,
                           MediaRecorder mediaRecorder,
                           MediaPlayer mediaPlayer) {
        this.context = application;
        this.audioDirName = audioDirName;
        this.mediaRecorder = mediaRecorder;
        this.mediaPlayer = mediaPlayer;
        createImageDir();
    }

    /**
     * Used to create an directory used for audio files.
     */
    private void createImageDir() {
        File imageDir = new File(context.getFilesDir(), audioDirName);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
    }

    /**
     * Used to start and store a recorded audio to a file.
     */
    public void startRecordAudio(String fileName) {
        String fileExtension = ".3gp";
        String audioFilePath =
                context.getFilesDir() + "/" + audioDirName + "/" + fileName + fileExtension;
        try {
            mediaRecorder = getNewMediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(
                    MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setOutputFile(audioFilePath);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (Exception e) {
            Log.e(TAG, "Error" + e.getMessage());
        }
    }

    /**
     * Create a media recorder dependent on Android system API level.
     */
    private MediaRecorder getNewMediaRecorder() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ?
                new MediaRecorder(context): // if system API >= 31
                new MediaRecorder(); // If system API < 31
    }

    /**
     * Used to stop recorded audio.
     */
    public void stopRecordAudio() {
        try {
            mediaRecorder.stop();
            mediaRecorder.release();
            mediaRecorder = null;
        } catch (Exception e) {
            Log.e(TAG, "Error" + e.getMessage());
        }
    }

    /**
     * Used to start playing audio from a file.
     */
    public void playAudio(String fileName) {
        try {
            mediaPlayer = new MediaPlayer();
            String audioFilePath =
                    context.getFilesDir() + "/" + audioDirName + "/" + fileName;
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(audioFilePath);
            mediaPlayer.prepare();
            mediaPlayer.setLooping(true);
            mediaPlayer.start();
        } catch (IOException e) {
            Log.e(TAG, "Error" + e.getMessage());
        }
    }

    /**
     * Used to stop playing current audio.
     */
    public void stopAudio() {
        mediaPlayer.release();
        mediaPlayer = null;
    }

    /**
     * Used to get all stored audios as files.
     */
    public List<File> getAllAudioFiles() {
        String dirPath = context.getFilesDir() + "/" + audioDirName;
        return Stream.of(new File(dirPath).listFiles())
                .filter(file -> !file.isDirectory())
                .collect(Collectors.toList());
    }

    /**
     * Used to get an uri for a file.
     */
    public Uri getUriForFile(File file) {
        return getUriForFile(context,
                "com.martelleur.spelainochuppljud.fileprovider",
                file);
    }
}


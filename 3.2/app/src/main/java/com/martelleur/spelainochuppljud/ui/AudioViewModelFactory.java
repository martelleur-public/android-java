package com.martelleur.spelainochuppljud.ui;

import android.app.Application;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.spelainochuppljud.infrastructure.AudioManager;
import com.martelleur.spelainochuppljud.model.AudioRepository;

public class AudioViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final AudioRepository repository;

    public AudioViewModelFactory(Application application) {
        AudioManager audioManager = new AudioManager();
        MediaRecorder mediaRecorder = new MediaRecorder();
        MediaPlayer mediaPlayer = new MediaPlayer();
        this.repository = new AudioRepository(application,
                audioManager, mediaRecorder, mediaPlayer);
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new AudioViewModel(repository);
    }
}
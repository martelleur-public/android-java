package com.martelleur.spelainochuppljud.model;

import android.app.Application;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import com.martelleur.spelainochuppljud.infrastructure.AudioManager;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Repository class used to store and acquire persistent audio files.
 */
public class AudioRepository {
    private final AudioManager audioManager;

    /**
     * Constructor used to create this class.
     */
    public AudioRepository(Application application,
                            AudioManager audioManager,
                            MediaRecorder mediaRecorder,
                            MediaPlayer mediaPlayer) {
        this.audioManager = audioManager;
        String audioDir = "audio";
        audioManager.initialize(application, audioDir, mediaRecorder, mediaPlayer);
    }

    /**
     * Used to start record audio with a specific name.
     */
    public void startRecordAudio(String fileName) {
        audioManager.startRecordAudio(fileName);
    }

    /**
     * Used to stop record current audio.
     */
    public void stopRecordAudio(AudioRecordCallback callback) {
        audioManager.stopRecordAudio();
        callback.onStopRecording(); // Communicate back to caller.
    }

    /**
     * Used to start playing audio with specific filename.
     */
    public void playAudio(String fileName) {
        audioManager.playAudio(fileName);
    }

    /**
     * Used to stop playing current audio.
     */
    public void stopAudio() {
        audioManager.stopAudio();
    }

    /**
     * Used to get all stored audios files mapped
     * as encapsulated AudioItem types.
     */
    public List<AudioItem> getAllAudioItems() {
        return audioManager.getAllAudioFiles()
                .stream()
                .map(file -> new AudioItem(false, file.getName()))
                .collect(Collectors.toList());
    }
}

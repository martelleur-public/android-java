package com.martelleur.spelainochuppljud.model;

import java.util.Objects;

/**
 * Class representing a audio whit a
 * playing state and a name.
 */
public class AudioItem {
    private boolean isPlaying;
    private final String name;

    public AudioItem(boolean isPlaying, String name) {
        this.isPlaying = isPlaying;
        this.name = name;
    }

    /**
     * Used to get is playing state.
     */
    public boolean isPlaying() {
        return isPlaying;
    }

    /**
     * Used to set is playing state.
     */
    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

    /**
     * Used to get the name of this audio.
     */
    public String getName() {
        return name;
    }

    /**
     * Used to check for equality based on the instance variable 'name'.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AudioItem)) {
            return false;
        }
        return Objects.equals(((AudioItem) o).getName(), this.getName());
    }
}

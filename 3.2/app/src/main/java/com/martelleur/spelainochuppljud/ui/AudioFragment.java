package com.martelleur.spelainochuppljud.ui;

import android.app.Application;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.spelainochuppljud.R;
import com.martelleur.spelainochuppljud.databinding.FragmentAudioBinding;
import com.martelleur.spelainochuppljud.model.AudioItem;

import java.util.List;
import java.util.Random;

/**
 * A fragment used for record and play audio.
 */
public class AudioFragment extends Fragment {
    private FragmentAudioBinding binding;
    private AudioViewModel audioViewModel;
    private AudioListAdapter audioListAdapter;

    public AudioFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     **/
    public static AudioFragment newInstance() {
        return new AudioFragment();
    }

    /**
     * Lock screen rotation to simplify recording and
     * play and functionality.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    /**
     * Bind layout using view binding
     * FragmentAudioBinding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAudioBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance with factory class, add listeners,
     * and a recycler view used for displaying audio items.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Application application = requireActivity().getApplication();
        AudioViewModelFactory factory = new AudioViewModelFactory(application);
        audioViewModel = new ViewModelProvider(requireActivity(), factory).get(AudioViewModel.class);
        updateView();
        setupAudioRecordListener();
        setUpAudioRecycler(audioViewModel.getAudioItemsLiveData().getValue());
        setupAudioObserver();
    }

    /**
     * Used to update view when fragment view is created.
     */
    private void updateView() {
        binding.recordAudioStop.setVisibility(View.GONE);
    }

    /**
     * Add listeners for start and stop audio recording.
     */
    private void setupAudioRecordListener() {
        binding.recordAudio.setOnClickListener(view -> {
            updateRecordingView(true);
            audioViewModel.startRecordAudio(getFileName());
        });
        binding.recordAudioStop.setOnClickListener(view -> {
            updateRecordingView(false);
            audioViewModel.stopRecordAudio();
        });
    }

    /**
     * Update layout when recording audio or not.
     */
    private void updateRecordingView(boolean isRecording) {
        if (isRecording) {
            binding.audioRecycler.setVisibility(View.INVISIBLE);
            binding.recordAudio.setVisibility(View.GONE);
            binding.fileNameInputContainer.setVisibility(View.INVISIBLE);
            binding.recordAudioStop.setVisibility(View.VISIBLE);
        } else {
            binding.audioRecycler.setVisibility(View.VISIBLE);
            binding.recordAudio.setVisibility(View.VISIBLE);
            binding.recordAudioStop.setVisibility(View.GONE);
            binding.fileNameInputContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Used to get file name. If user do not enter
     * a filename then a random filename will be used,
     */
    private String getFileName() {
        if (!binding.fileNameInput.getText().toString().equals("")) {
            return binding.fileNameInput.getText().toString();
        } else {
            return getRandomFileName();
        }
    }
    /**
     * Used to get a random filename.
     */
    private String getRandomFileName() {
        int minCharacter = 97; // represent A
        int maxCharacter = 122; // represent Z
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(2);
        for(int i = 0; i < 2; i++) {
            int character = minCharacter + random.nextInt() * (maxCharacter - minCharacter + 1);
            buffer.append(character);
        }
        return buffer.toString();
    }

    /**
     * Set up a recycler view with an adapter
     * that displays record audio files by this application.
     */
    private void setUpAudioRecycler(List<AudioItem> audioItems) {
        audioListAdapter = new AudioListAdapter(R.layout.audio_item,
                audioItems, audioItemCallback);
        RecyclerView recyclerView = binding.audioRecycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(audioListAdapter);
    }

    /**
     * Callback used for handling playing and stopping audio files
     * and matching layout when user select paying or stopping a
     * audio file in the recycle view.
     */
    private final AudioItemCallback audioItemCallback = new AudioItemCallback() {
        @Override
        public void onPlayAudio(AudioItem audioItem) {
            audioViewModel.stopAudio(); // stop previous audio if playing.
            updatePlayingView(true);
            audioViewModel.playAudio(audioItem); // start playing new audio.
        }

        @Override
        public void onStopAudio() {
            updatePlayingView(false);
            audioViewModel.stopAudio();
        }
    };

    /**
     * Update layout when playing audio or not.
     */
    private void updatePlayingView(boolean isPlayingAudio) {
        if (isPlayingAudio) binding.audioRecordContainer.setVisibility(View.INVISIBLE);
        else binding.audioRecordContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Update adapter and recycler when audio items are updated.
     */
    private void setupAudioObserver() {
        audioViewModel.getAudioItemsLiveData().observe(requireActivity(), audioItems ->
                audioListAdapter.setAudioList(audioItems));
    }

    /**
     * Stop audio recording and play when fragment is destroy, e.g.,
     * a user rotate app or press the back button.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (audioViewModel != null) {
            audioViewModel.stopAudio();
            audioViewModel.stopRecordAudio();
        }
        binding = null;
    }
}
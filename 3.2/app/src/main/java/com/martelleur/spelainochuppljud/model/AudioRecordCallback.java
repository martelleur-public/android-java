package com.martelleur.spelainochuppljud.model;

/**
 * A callback interface used for communicating back from repository
 * {@link AudioRepository} to view model
 * {@link com.martelleur.spelainochuppljud.ui.AudioViewModel}.
 */
public interface AudioRecordCallback {
    void onStopRecording();
}

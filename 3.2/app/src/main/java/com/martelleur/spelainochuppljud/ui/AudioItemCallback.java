package com.martelleur.spelainochuppljud.ui;

import com.martelleur.spelainochuppljud.model.AudioItem;


/**
 * A callback interface used for communicating between
 * fragment {@link AudioFragment} and adapter {@link AudioListAdapter}.
 */
public interface AudioItemCallback {
    void onPlayAudio(AudioItem audioItem);
    void onStopAudio();
}

# Spela in och upp ljud

Prototypen introducerar till hur man använder mikrofonen för att spela in ljud samt hur man spelar upp ljudet för användaren.

## Prototyp

Gör en app som möjliggör att:

- Användaren kan spela in ljud
- Användaren kan spela upp senast inspelade ljud

Man kan ta hjälp av den app som redan finns i Android (använda en Intent) eller göra en egen kustomiserad variant (svårare).
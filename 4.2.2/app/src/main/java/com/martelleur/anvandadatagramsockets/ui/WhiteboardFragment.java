package com.martelleur.anvandadatagramsockets.ui;


import android.graphics.PointF;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.anvandadatagramsockets.databinding.FragmentWhiteBoardBinding;
import com.martelleur.anvandadatagramsockets.model.Stroke;

import java.util.List;

/**
 * A fragment used as a whiteboard.
 */
public class WhiteboardFragment
        extends Fragment implements WhiteboardFragmentCallback {
    FragmentWhiteBoardBinding binding;
    WhiteboardViewModel whiteboardViewModel;
    private static final String ARG_HOST_PORT = "whiteboard_fragment_host_port";
    private static final String ARG_TARGET_HOST = "whiteboard_fragment_target_host";
    private static final String ARG_TARGET_PORT = "whiteboard_fragment_target_port";
    private int hostPort = 0;
    private String targetHost = "";
    private int targetPort = 0;

    public WhiteboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static WhiteboardFragment newInstance(int hostPort,
                                                 String targetHost,
                                                 int targetPort) {
        WhiteboardFragment fragment = new WhiteboardFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_HOST_PORT, hostPort);
        args.putString(ARG_TARGET_HOST, targetHost);
        args.putInt(ARG_TARGET_PORT, targetPort);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Used to recreate fragment host and port data from
     * a previous saved state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            hostPort = getArguments().getInt(ARG_HOST_PORT);
            targetHost = getArguments().getString(ARG_TARGET_HOST);
            targetPort = getArguments().getInt(ARG_TARGET_PORT);
        }
    }

    /**
     * Bind layout using view binding and restore whiteboard from state
     * in bundle, if state have been stored before.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentWhiteBoardBinding.inflate(inflater, container, false);
        whiteboardViewModel = getWhiteboardViewModel();
        addCallbackToWhiteboardView();
        restoreDrawingsState();
        return binding.getRoot();
    }

    /**
     * Used to get the whiteboard view model.
     */
    private WhiteboardViewModel getWhiteboardViewModel() {
        WhiteboardViewModelFactory factory = new WhiteboardViewModelFactory(
                hostPort, targetHost, targetPort);
        return new ViewModelProvider(requireActivity(),
                factory).get(WhiteboardViewModel.class);
    }

    /**
     * Adding callback to whiteboard view to be used when new
     * lines are drawn.
     */
    private void addCallbackToWhiteboardView() {
        binding.whiteBoard.whiteboardFragmentCallback = this;
    }

    /**
     * Implementing "WhiteboardFragmentCallback.sendDrawMessage"
     * to be used when new lines are drawn.
     */
    @Override
    public void sendDrawMessage(int x, int y) {
        whiteboardViewModel.sendDrawMessage(x, y);
    }

    /**
     * Implementing "WhiteboardFragmentCallback.storeStroke"
     * to be used when a new stroke are created.
     */
    @Override
    public void storeStroke(List<PointF> points) {
        whiteboardViewModel.storeWhiteboardStroke(points);
    }

    /**
     * Used to restore the drawing state from view model.
     */
    private void restoreDrawingsState() {
        if (whiteboardViewModel != null) {
            List<Stroke> strokes = whiteboardViewModel.getWhiteboardStrokes();
            binding.whiteBoard.updateCanvas(strokes);
        }
    }

    /**
     * Used to add an observer for drawing.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupWhiteboardStrokeObserver();
    }

    /**
     * Used to observe stokes when a peer are drawing.
     */
    private void setupWhiteboardStrokeObserver() {
        whiteboardViewModel.getStrokeLiveData().observe(requireActivity(), points -> {
            try {
                binding.whiteBoard.updateCanvas(points, true);
            } catch (NullPointerException ignore) {}
        });
    }

    /**
     * Used to save port and host to bundle object.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_HOST_PORT, hostPort);
        outState.putString(ARG_TARGET_HOST, targetHost);
        outState.putInt(ARG_TARGET_PORT, targetPort);
    }

    /**
     * Use to pause chat when fragment is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}

package com.martelleur.anvandadatagramsockets.infrastructure;

/**
 * Callback interface used for communication between
 * {@link WhiteboardSocket} and {@link com.martelleur.anvandadatagramsockets.model.Whiteboard}
 */
public interface WhiteboardCallback {
    void handleIncomingMsg(String content);
}

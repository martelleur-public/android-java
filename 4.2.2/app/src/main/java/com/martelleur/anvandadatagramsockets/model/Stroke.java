package com.martelleur.anvandadatagramsockets.model;

import android.graphics.PointF;

import java.util.ArrayList;
import java.util.List;

/**
 * A model class representing a stroke done by the user.
 * A Stroke is a continues painted line without any spaces.
 */
public class Stroke {
    private final List<PointF> points;
    Stroke(List<PointF> points) {
        this.points = points;
    }

    public List<PointF> getPoints() {
        return points;
    }
}

package com.martelleur.anvandadatagramsockets.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.martelleur.anvandadatagramsockets.model.Stroke;
import com.martelleur.anvandadatagramsockets.model.Whiteboard;

import java.util.ArrayList;
import java.util.List;


/**
 * A custom view used to display a whiteboard
 * where a user can draw lines.
 */
public class WhiteboardView extends View {
    private Paint paint;
    private Path path;
    private List<PointF> drawPoints = new ArrayList<>();
    WhiteboardFragmentCallback whiteboardFragmentCallback;

    public WhiteboardView(Context context) {
        super(context);
        init();
    }

    public WhiteboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WhiteboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Used to setup and configure paint and path.
     */
    private void init() {
        paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        path = new Path();
    }

    /**
     * Used determine behavior of on draw events.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }

    /**
     * Used to implement touch event for drawing.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                path.moveTo(x, y);
                break;

            case MotionEvent.ACTION_MOVE:
                path.lineTo(x, y);
                whiteboardFragmentCallback.sendDrawMessage(
                        Math.round(x), Math.round(y));
                drawPoints.add(new PointF(x, y));
                break;

            case MotionEvent.ACTION_UP:
                whiteboardFragmentCallback.sendDrawMessage(
                        Math.round(Whiteboard.STOP_X),
                        Math.round(Whiteboard.STOP_Y));
                storeStrokeFromDrawPoints();
                performClick(); // On touch event compliance.
                break;

            default:
                return false;
        }

        invalidate(); // Repaint the View.
        return true;
    }

    /**
     * Used to store the stroke and start on a new stroke.
     */
    private void storeStrokeFromDrawPoints() {
        whiteboardFragmentCallback.storeStroke(drawPoints);
        drawPoints = new ArrayList<>(); // Start a new stroke
    }

    /**
     * Default onclick called when MotionEvent ACTION_UP is triggered.
     */
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    /**
     * Used to update canvas
     */
    public void updateCanvas(List<Stroke> strokes) {
        strokes.forEach(stroke -> {
            updateCanvas(stroke.getPoints(), false);
        });
        invalidate(); // Repaint the view.
    }

    /**
     * Used to update canvas
     */
    public void updateCanvas(List<PointF> points, boolean repaint) {
        for (int i = 0; i < points.size() -1; i++) {
            PointF p1 = points.get(i);
            PointF p2 = points.get(i + 1);
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
        }
        if (repaint) {
            invalidate(); // Repaint the view.
        }
    }
}

package com.martelleur.anvandadatagramsockets.model;

import android.graphics.PointF;

import androidx.lifecycle.MutableLiveData;

import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * A class representing a whiteboard. A whiteboard
 * can contain multiple strokes where a stroke is a
 * continues line painted without any spaces.
 */
public class Whiteboard implements WhiteboardCallback {
    private final List<Stroke> strokes = new ArrayList<>();

    private final MutableLiveData<List<PointF>> strokeLivedata =
            new MutableLiveData<>(new ArrayList<>());

    public static final float STOP_X = -1f;
    public static final float STOP_Y = -1f;

    /**
     * Used to add a stroke
     */
    public void addStroke(List<PointF> points) {
        Stroke stroke = new Stroke(points);
        strokes.add(stroke);
    }

    /**
     * Used to get stroke livedata.
     */
    public MutableLiveData<List<PointF>> getStrokeLivedata() {
        return strokeLivedata;
    }

    /**
     * Used to get all strokes.
     */
    public List<Stroke> getStrokes() {
        return strokes;
    }

    /**
     * Implementing WhiteboardCallback used when whiteboard
     * socket get incoming messages.
     */
    @Override
    public void handleIncomingMsg(String msg) {
        String[] coordinates = msg.split(" ");
        float x = Float.parseFloat(coordinates[0]);
        float y = Float.parseFloat(coordinates[1]);
        if (isDrawMessageFinished(x, y)) {
            flushDrawPointsLiveData();
        } else {
            updateDrawPointsLiveData(x, y);
        }
    }

    /**
     * Used to check if the peer app have stopped drawing.
     */
    private boolean isDrawMessageFinished(float x, float y) {
        return x == STOP_X && y == STOP_Y;
    }

    /**
     * Used to flush the stroke live data to the list of strokes,
     * this is used when the peer app stop drawing.
     */
    private void flushDrawPointsLiveData() {
        List<PointF> points = strokeLivedata.getValue();
        strokes.add(new Stroke(points));
        List<PointF> emptyList = new ArrayList<>();
        strokeLivedata.postValue(emptyList);
    }

    /**
     * Used to update stroke live data. This is used when the peer
     * app is drawing.
     */
    private void updateDrawPointsLiveData(float x, float y) {
        PointF point = new PointF(x, y);
        List<PointF> points = strokeLivedata.getValue();
        if (points != null) { points.add(point); }
        strokeLivedata.postValue(points);
    }
}

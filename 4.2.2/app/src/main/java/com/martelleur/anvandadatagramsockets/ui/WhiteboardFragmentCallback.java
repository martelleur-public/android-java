package com.martelleur.anvandadatagramsockets.ui;

import android.graphics.PointF;

import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardSocket;

import java.util.List;

/**
 * Callback interface used for communication between
 * {@link WhiteboardFragment} and {@link WhiteboardView}
 */
public interface WhiteboardFragmentCallback {
    void sendDrawMessage(int x, int y);
    void storeStroke(List<PointF> drawPoints);
}

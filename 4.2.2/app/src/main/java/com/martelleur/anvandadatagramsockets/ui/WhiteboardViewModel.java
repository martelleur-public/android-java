package com.martelleur.anvandadatagramsockets.ui;

import android.graphics.PointF;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvandadatagramsockets.model.Stroke;
import com.martelleur.anvandadatagramsockets.model.Whiteboard;
import com.martelleur.anvandadatagramsockets.model.WhiteboardRepository;

import java.util.List;

/**
 * View model class used to keep current state
 * of the whiteboard UI.
 * The class is also used to communicate app functionality
 * with the whiteboard repository for network drawing.
 */
public class WhiteboardViewModel extends ViewModel {
    private final Whiteboard whiteboard;
    private final WhiteboardRepository repository;

    WhiteboardViewModel(WhiteboardRepository repository) {
        this.repository = repository;
        whiteboard = this.repository.getWhiteboard();
    }

    /**
     * Used to get stroke live data from whiteboard.
     */
    public MutableLiveData<List<PointF>> getStrokeLiveData() {
        return whiteboard.getStrokeLivedata();
    }

    /**
     * Used to send a draw message.
     */
    public void sendDrawMessage(int x, int y) {
        String message = x + " " + y;
        repository.sendDrawMessage(message);
    }

    /**
     * Used to store a whiteboard stroke.
     */
    public void storeWhiteboardStroke(List<PointF> points) {
        whiteboard.addStroke(points);
    }

    /**
     * Used to get whiteboard strokes.
     */
    public List<Stroke> getWhiteboardStrokes() {
        return whiteboard.getStrokes();
    }
}

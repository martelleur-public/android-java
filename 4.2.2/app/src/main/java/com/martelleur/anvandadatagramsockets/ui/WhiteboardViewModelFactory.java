package com.martelleur.anvandadatagramsockets.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardExecutor;
import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardSocket;
import com.martelleur.anvandadatagramsockets.model.Whiteboard;
import com.martelleur.anvandadatagramsockets.model.WhiteboardRepository;

/**
 * Used to create a WhiteboardViewModel.
 */
public class WhiteboardViewModelFactory extends AbstractSavedStateViewModelFactory {

    private final WhiteboardRepository repository;

    public WhiteboardViewModelFactory(int hostPort, String targetHost, int targetPort) {
        WhiteboardExecutor whiteboardExecutor = new WhiteboardExecutor();
        Whiteboard whiteboard = new Whiteboard();
        WhiteboardSocket whiteboardSocket = new WhiteboardSocket(
                hostPort, targetHost, targetPort);
        this.repository = new WhiteboardRepository(
                whiteboard, whiteboardSocket, whiteboardExecutor
        );
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new WhiteboardViewModel(repository);
    }
}

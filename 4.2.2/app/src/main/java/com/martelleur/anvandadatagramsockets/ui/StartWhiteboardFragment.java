package com.martelleur.anvandadatagramsockets.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.martelleur.anvandadatagramsockets.R;
import com.martelleur.anvandadatagramsockets.databinding.FragmentStartWhiteboardBinding;

import java.util.Objects;

/**
 * A fragment used to start the whiteboard app whit
 * a host port, target host, and target port decided
 * from user input.
 */
public class StartWhiteboardFragment extends Fragment {
    FragmentStartWhiteboardBinding binding;

    public StartWhiteboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static StartWhiteboardFragment newInstance() {
        return new StartWhiteboardFragment();
    }

    /**
     * Inflate the layout for this fragment with view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentStartWhiteboardBinding.inflate(inflater,container, false);
        return binding.getRoot();
    }

    /**
     * Lifecycle method used to take user input for
     * starting whiteboard app.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupStartWhiteboardListener();
    }

    /**
     * Used to take user input for starting whiteboard app.
     */
    private void setupStartWhiteboardListener() {
        binding.startWhiteboard.setOnClickListener(view -> {
            String hostPort = Objects.requireNonNull(binding.hostPortInput.getText()).toString();
            String targetHost = Objects.requireNonNull(binding.targetHostInput.getText()).toString();
            String targetPort = Objects.requireNonNull(binding.targetPortInput.getText()).toString();

            if (hostPort.isEmpty() || targetHost.isEmpty() || targetPort.isEmpty()) {
                Toast.makeText(getContext(),
                        "All fields must be entered.",
                        Toast.LENGTH_LONG).show();
            } else {
                navigateToWhiteboard(
                        Integer.parseInt(hostPort),
                        targetHost,
                        Integer.parseInt(targetPort));
            }
        });
    }

    /**
     * Used to navigate to whiteboard fragment
     * with user defined host port, target host, and target port.
     */
    private void navigateToWhiteboard(int hostPort, String targetHost, int targetPort) {
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, WhiteboardFragment.newInstance(
                        hostPort, targetHost, targetPort))
                .addToBackStack(null)
                .commit();
    }
}
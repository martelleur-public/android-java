package com.martelleur.anvandadatagramsockets.infrastructure;

import java.util.concurrent.Executor;

/**
 * An Executor used to start and control
 * a whiteboard socket.
 */
public class WhiteboardExecutor implements Executor {
    private WhiteboardSocket whiteboardSocket;

    /**
     * Used to start start whiteboard socket in a separated thread.
     */
    @Override
    public void execute(Runnable whiteboardSocket) {
        if (!(whiteboardSocket instanceof WhiteboardSocket)) {
            throw  new IllegalArgumentException("Runner should be an instance of WhiteboardSocket");
        }
        this.whiteboardSocket = (WhiteboardSocket) whiteboardSocket;
        new Thread(this.whiteboardSocket).start();
    }

    /**
     * Used to pause whiteboard socket.
     * TODO NOT IMPLEMENTED! CAN BE USED IF PAUSE OR CREATE NEW SOCKET.
     */
    public void pauseSocket() {
        whiteboardSocket.pauseSocket();
    }

    /**
     * Used to restart whiteboard socket.
     * TODO NOT IMPLEMENTED! CAN BE USED IF PAUSE OR CREATE NEW SOCKET.
     */
    public void restartSocket() {
        whiteboardSocket.restartSocket();
    }
}

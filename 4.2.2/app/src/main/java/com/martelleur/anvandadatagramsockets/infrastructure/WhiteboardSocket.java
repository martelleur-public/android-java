package com.martelleur.anvandadatagramsockets.infrastructure;


import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A whiteboard datagram socket used to communicate with
 * other whiteboard sockets for P2P drawing.
 */
public class WhiteboardSocket implements Runnable {
    private String TAG = "WhiteboardSocket";
    private final int hostPort;
    private final String targetHost;
    private final int targetPort;
    private WhiteboardCallback whiteboardCallback;
    private final AtomicBoolean socketIsOpen = new AtomicBoolean(false);

    public WhiteboardSocket(int hostPort, String targetHost, int targetPort) {
        this.hostPort = hostPort;
        this.targetHost = targetHost;
        this.targetPort = targetPort;
    }

    /**
     * Used to add a callback to be used for incoming draw messages.
     */
    public void addWhiteboardCallback(WhiteboardCallback whiteboardCallback) {
        this.whiteboardCallback = whiteboardCallback;
    }

    /**
     * Used to listen and read incoming messages
     * while the socket is open
     */
    @Override
    public void run() {
        socketIsOpen.set(true);
        try (DatagramSocket ds = new DatagramSocket(hostPort)) {
            while (socketIsOpen.get()) {
                // USE Maximum transmission unit (MTU)
                // normal size 1500 bytes as
                byte[] bytes = new byte[1500];
                DatagramPacket dp = new DatagramPacket(bytes, bytes.length);
                ds.receive(dp);
                String message = new String(dp.getData(), 0, dp.getLength());
                whiteboardCallback.handleIncomingMsg(message);
            }
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
            socketIsOpen.set(false);
        }
    }

    /**
     * Used to send draw messages.
     */
    public synchronized void sendDrawMessage(String message) {
        try (DatagramSocket ds = new DatagramSocket()) {
            byte[] bytes = message.getBytes();
            InetAddress targetInet = InetAddress.getByName(this.targetHost);
            DatagramPacket dp = new DatagramPacket(
                    bytes, bytes.length, targetInet, targetPort);
            ds.send(dp);
        }
        catch (IOException | SecurityException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * Use to pause the socket.
     */
    public void pauseSocket() {
        socketIsOpen.set(false);
    }

    /**
     * Use to restart the socket.
     */
    public void restartSocket() {
        socketIsOpen.set(true);
    }
}

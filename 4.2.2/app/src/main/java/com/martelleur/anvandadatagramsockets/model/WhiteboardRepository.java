package com.martelleur.anvandadatagramsockets.model;


import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardCallback;
import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardExecutor;
import com.martelleur.anvandadatagramsockets.infrastructure.WhiteboardSocket;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Used as an abstract layer for whiteboard datagram
 * socket operations.
 */
public class WhiteboardRepository {

    private final Whiteboard whiteboard;
    private final WhiteboardSocket whiteboardSocket;
    private final ExecutorService executorService
            = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

    public WhiteboardRepository(Whiteboard whiteboard,
                                WhiteboardSocket whiteboardSocket,
                                WhiteboardExecutor whiteboardExecutor) {
        this.whiteboard = whiteboard;
        this.whiteboardSocket = whiteboardSocket;
        addWhiteboardCallback(this.whiteboard);
        whiteboardExecutor.execute(this.whiteboardSocket);
    }

    /**
     * Used to add a callback to be used for incoming draw messages.
     */
    private void addWhiteboardCallback(WhiteboardCallback whiteboardCallback) {
        this.whiteboardSocket.addWhiteboardCallback(whiteboardCallback);
    }

    /**
     * Used to get an instance of a whiteboard.
     */
    public Whiteboard getWhiteboard() {
        return whiteboard;
    }

    /**
     * Used to send a draw message on a separated thread with an
     * executor service.
     */
    public void sendDrawMessage(String message) {
        Runnable sendDrawMessage = () ->
                whiteboardSocket.sendDrawMessage(message);
        executorService.execute(sendDrawMessage);
    }
}

# Använda datagram sockets

Rit program som använder peer-to-peer paradigmen och udp.

## Starta Applikationen

Två android smartphones behövs ```Android 1``` och ```Android 2```, kan vara emulatorer.

1. __Android 1, starta appen och ange:__

    - Host port: E.g. ```2001```
    - Target host: E.g. ```172.20.10.8```
    - Target port: E.g. ```2000```

2. __Android 2, starta appen och ange:__
    - Host port: E.g. ```2000```
    - Target host: E.g. ```172.20.10.9```
    - Target port: E.g. ```2001```

3. __Börja rita__

## Uppgift

Gör en app som implementerar ett enkelt distribuerat ritprogram (whiteboard). Allt som ritas ska sändas med UDP till ett annat identiskt program som ritar upp exakt samma bild. Det andra programmet ska alltså fungera på samma sätt.


## Exempel

Ett enkelt exempel kan köras enligt följande:

Använd filen [Draw.jar] 

Kör igång programmet med: ```java -jar Draw.jar <my port> <remote host> <remote port>```
Om man vill testa på samma maskin kan man exempelvis köra igång två program:

```java -jar Draw.jar 2000 localhost 2001```
```java -jar Draw.jar 2001 localhost 2000```

Men det är roligare om man testar två program på två olika datorer.

## Tips

Utgå gärna från detta enkla ritprogram (men det är inget krav). Bygg gärna ut programmet så att det fungerar med olika färger, olika tjocklek på penseln, olika figurer och så vidare (men det är inget krav).

När man ska sända bildpunkter så kan man först omvandla dem till en sträng på något smart format (exempelvis så kan man ha ett mellanslag mellan x och y) och sedan omvandla till en byte-array. När man går från en Point till en String kan man exempelvis göra så här:

```String message = Integer.toString(p.x) + " " + Integer.toString(p.y);```

När man ska ta emot bildpunkter så kommer de i en byte-array som man gör om till en sträng. När man går från en String till en Point så kan man exempelvis göra så här:

```
String[] xy = message.split(" ");
Point p = new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
```

Vill man även sända med färg och tjocklek så får man utöka formatet att även ha med dessa värden.

Om man testar mellan två datorer och det inte fungerar så kan det bero på att port-forwarding ska ställas in i routern eller att ens ISP blockar UDP på vissa portar.

Som överkurs kan man titta på paketet ```java.nio```, exempelvis på klassen ```DatagramChannel```.
package com.martelleur.fatagpaanslutningsinformation;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.martelleur.fatagpaanslutningsinformation.databinding.FragmentNetworkInfoBinding;

/**
 * A fragment used to display if the phone is connected to WiFi,
 * Cellular network, Unknown network, or not connected to a network.
 */
public class NetworkInfoFragment extends Fragment {
    private FragmentNetworkInfoBinding binding;
    private ConnectivityManager connectivityManager;
    private ConnectivityManager.NetworkCallback networkCallback;

    /**
     * Used to create an instance of this fragment.
     */
    public static NetworkInfoFragment newInstance() {
        return new NetworkInfoFragment();
    }

    /**
     * Used to bind layout using view binding and
     * register a ConnectivityManager.NetworkCallback
     * that listen for network changes.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNetworkInfoBinding.inflate(inflater, container, false);
        connectivityManager = (ConnectivityManager) requireContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        registerConnectivityManagerCallback();
        return binding.getRoot();
    }

    /**
     * Use a ConnectivityManager to register a
     * ConnectivityManager.NetworkCallback that listen for network changes.
     */
    private void registerConnectivityManagerCallback() {
        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                NetworkCapabilities networkCapabilities
                        = connectivityManager.getNetworkCapabilities(network);
                handleConnectionUp(networkCapabilities);
            }
            @Override
            public void onLost(Network network) {
                handleConnectionDown();
            }
        };
        connectivityManager.registerDefaultNetworkCallback(networkCallback);
    }

    /**
     * Used to update layout with network information
     * when the fragment view is created.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayoutFromNetworkStatus();
    }

    /**
     * Used to check network status with NetworkCapabilities
     * when the fragment view is created.
     */
    private void updateLayoutFromNetworkStatus() {
        NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
        if (capabilities != null && capabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)) {
            handleConnectionUp(capabilities);
        } else {
            handleConnectionDown();
        }
    }

    /**
     * Used to handle connection up.
     */
    private void handleConnectionUp(NetworkCapabilities capabilities) {
        String networkName = getNetworkName(capabilities);
        String status = getString(R.string.connected_to, networkName);

        // Update the status text view on the main thread.
        requireActivity().runOnUiThread(() ->
                binding.networkConnectionStatus.setText(status));
    }

    /**
     * Used to determine the network name based on network capabilities.
     */
    private String getNetworkName(NetworkCapabilities capabilities) {
        String networkName;
        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
            networkName = "Wi-Fi";
        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
            networkName = "cellular network";
        } else {
            networkName = "unknown network";
        }
        return  networkName;
    }

    /**
     * Used to handle connection down.
     */
    private void handleConnectionDown() {
        String status = getString(R.string.not_connected);

        // Update the status text view on the main thread.
        requireActivity().runOnUiThread(() ->
                binding.networkConnectionStatus.setText(status));
    }

    /**
     * Unregister ConnectivityManager.NetworkCallback.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        connectivityManager.unregisterNetworkCallback(networkCallback);
        binding = null;
    }
}

# Få tag på anslutningsinformation

Prototypen introducerar till hur man tar redan på om mobilen är ansluten till Internet.

## Prototyp

Gör en app som visar om mobilen är ansluten till Internet.

## Beskrivning implementation

Applikationen implementerar en fragment ```NetworkInfoFragment```
som visar om mobilen är ansluten till:

1. WiFi,

2. Cellular network,

3. Unkown network, eller 

4. Inte ansluten till något nätverk.
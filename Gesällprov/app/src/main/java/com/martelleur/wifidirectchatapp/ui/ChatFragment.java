package com.martelleur.wifidirectchatapp.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.martelleur.wifidirectchatapp.R;
import com.martelleur.wifidirectchatapp.databinding.FragmentChatBinding;
import com.martelleur.wifidirectchatapp.model.Chat;
import com.martelleur.wifidirectchatapp.model.Message;

import java.util.List;

/**
 * A fragment used to display and write chat messages.
 */
public class ChatFragment extends Fragment {
    private static final String ARG_CHAT_TITLE
            = "com.martelleur.anvandawifip2p.ui.ChatFragment.ARG_CHAT_TITLE";
    private FragmentChatBinding binding;
    private ChatViewModel chatViewModel;
    private MessagesAdapter messagesAdapter;
    private String chatTitle = "";

    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static ChatFragment newInstance(String chatTitle) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CHAT_TITLE, chatTitle);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Used to restore host and port stored in bundle object.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            chatTitle = getArguments().getString(ARG_CHAT_TITLE);
        }
    }

    /**
     * Used to get view model and bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentChatBinding.inflate(inflater, container, false);
        chatViewModel = getChatViewModel();
        chatViewModel.setChatTitle(chatTitle);
        return binding.getRoot();
    }

    /**
     * Used to get the view model instance.
     */
    private ChatViewModel getChatViewModel() {
        return new ViewModelProvider(this,
                ViewModelProvider.Factory.from(ChatViewModel.initializer))
                .get(ChatViewModel.class);
    }

    /**
     * When view is created setup.
     * 1) Setup chat title
     * 2) Setup recycler for displaying messages.
     * 3) Setup observer for messages livedata.
     * 4) Setup message send listener.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setChatTitle();
        setupMessagesRecycler();
        setupMessagesObserver();
        setupSendMessageListener();
    }

    /**
     * Used to set chat title from view model state.
     */
    private void setChatTitle() {
        String title = getString(R.string.chat_title, chatViewModel.getChatTitle());
        binding.chatTitle.setText(title);
    }

    /**
     * Used to configure and setup a recycler view that display
     * chat messages items with an adapter.
     */
    private void setupMessagesRecycler() {
        List<Message> messages = chatViewModel.getMessagesLiveData().getValue();
        messagesAdapter = new MessagesAdapter(
                R.layout.message_item, messages);
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(messagesAdapter);
        int size = messagesAdapter.getItemCount();
        recyclerView.smoothScrollToPosition(size);
    }

    /**
     * Update adapter and recycler when messages are updated.
     */
    private void setupMessagesObserver() {
        chatViewModel.getMessagesLiveData().observe(getViewLifecycleOwner(), messages -> {
            messagesAdapter.setGuestBooks(messages);
            scrollToLastMessage(messages.size());
        });
    }

    /**
     * Used to scroll down to last message.
     */
    private void scrollToLastMessage(int size) {
        try {
            binding.recycler.smoothScrollToPosition(size);
        } catch (NullPointerException ignore) {}
    }

    /**
     * Add a click listener for sending a new message.
     */
    private void setupSendMessageListener() {
        binding.sendMessage.setOnClickListener(view -> {
            String message = getUserMessage();
            if (message.isEmpty()) toastMessageStatus();
            else {
                try {
                    chatViewModel.writeMessage(message);
                    binding.newMessage.setText("");
                } catch (Chat.ChatException e) {
                    Toast.makeText(requireContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Used to get user entered message.
     */
    private String getUserMessage() {
        String message = "";
        EditText input = binding.newMsgContainer.getEditText();
        if (input != null) {
            message = input.getText().toString();
        }
        return message;
    }

    /**
     * Toast a message to the user describing if the status of
     * the message.
     */
    private void toastMessageStatus() {
        Toast.makeText(getContext(), getString(R.string.no_empty_message), Toast.LENGTH_LONG).show();
    }

    /**
     * Used to save state to bundle object.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_CHAT_TITLE, chatTitle);
    }

    /**
     * Destroy view and layout binding.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
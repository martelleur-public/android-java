package com.martelleur.wifidirectchatapp.ui;

import android.net.wifi.p2p.WifiP2pDevice;

/**
 * A callback interface used for communicating between
 * fragment {@link MainFragment} and adapter {@link WifiPeerAdapter}.
 */
public interface WifiPeerCallback {
    void onConnectToChannel(WifiP2pDevice peer);
    void onOpenChat();
    void onDisconnectFromChannel();
}

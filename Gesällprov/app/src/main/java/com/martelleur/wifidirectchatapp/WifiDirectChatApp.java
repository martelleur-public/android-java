package com.martelleur.wifidirectchatapp;

import android.app.Application;

import com.martelleur.wifidirectchatapp.model.Chat;
import com.martelleur.wifidirectchatapp.model.cttp.ChatProtocolController;
import com.martelleur.wifidirectchatapp.model.MainRepository;

/**
 * Extending application and initialize MainRepository with a chat.
 * This class is registered in the AndroidManifest.xml file, and an
 * instance of this class will be created when the application is started.
 */
public class WifiDirectChatApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ChatProtocolController chatProtocolController = new ChatProtocolController();
        Chat chat = new Chat(chatProtocolController);
        MainRepository.initialize(chat);
    }
}

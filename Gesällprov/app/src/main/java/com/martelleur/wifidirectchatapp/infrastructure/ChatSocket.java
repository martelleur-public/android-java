package com.martelleur.wifidirectchatapp.infrastructure;

/**
 * A chat socket interface implemented by
 * the ChatServer and the ChatSocket.
 */
public interface ChatSocket {
    void writeMessage(String message);
    void closeSocket();
}

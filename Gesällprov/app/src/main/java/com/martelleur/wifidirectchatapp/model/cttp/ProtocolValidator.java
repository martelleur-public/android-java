package com.martelleur.wifidirectchatapp.model.cttp;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

/**
 * A class used to validate elements in custom
 * protocol; Chat Text Transport Protocol (CTTP).
 * The protocol is defined in class CTTP.
 */
public class ProtocolValidator {
    /**
     * Used to validate required elements.
     */
    public void validateRequiredElements(Element[] incomingElements) throws ChatProtocolController.NotValidChatMessage {
        for (Element requiredElement: incomingElements) {
            if (requiredElement == null) {
                throw new ChatProtocolController.NotValidChatMessage("Required element is missing");
            }
        }
        List<String> tagNameValues = Arrays.stream(CTTP.Tag.values())
                .map(Enum::name)
                .collect(Collectors.toList());

        if (Arrays.stream(incomingElements)
                .map(element -> element.name)
                .allMatch(tagNameValues::contains)) {
            throw new ChatProtocolController.NotValidChatMessage("Required element is not correct");
        }
    }

    /**
     * Used to validate the root element.
     */
    public void validateRootElement(Element root) throws ChatProtocolController.NotValidChatMessage {
        Collection<Element> validChildren = new ArrayList<>(Arrays.asList(
                new Element(CTTP.Tag.HEADER.value),
                new Element(CTTP.Tag.BODY.value)));
        ElementValidator validator = parentElementValidator(CTTP.Tag.ROOT, validChildren);
        validator.validate(root);
    }

    /**
     * Used to get a element validator that validate parent element,
     * that is element that contain other element.
     */
    private ElementValidator parentElementValidator(CTTP.Tag tag,
                                                    Collection<Element> validChildren) {
        return element -> {
            if (!Objects.equals(element.name, tag.value)) {
                throw new ChatProtocolController.NotValidChatMessage("Element name should be "
                        + tag.value);
            }

            if (element.getText() != null) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + tag.value
                        + " should not contain any text");
            }

            if (!element.getChildren().containsAll(validChildren)) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + tag.value
                        + " do not contain correct type of child elements");
            }

            if (element.getChildren().size() != validChildren.size()) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + tag.value
                        + " do not contain correct number of child elements");
            }
        };
    }

    /**
     * Used to validate the root element.
     */
    public void validateHeaderElement(Element header) throws ChatProtocolController.NotValidChatMessage {
        Collection<Element> validChildren = new ArrayList<>(Collections.singletonList(
                new Element(CTTP.Tag.PROTOCOL.value)));
        ElementValidator validator = parentElementValidator(CTTP.Tag.HEADER, validChildren);
        validator.validate(header);
    }

    /**
     * Used to validate element "protocol".
     */
    public void validateProtocolElement(Element protocol) throws ChatProtocolController.NotValidChatMessage {
        Collection<Element> validChildren = new ArrayList<>(Arrays.asList(
                new Element(CTTP.Tag.SCHEME.value),
                new Element(CTTP.Tag.VERSION.value),
                new Element(CTTP.Tag.METHOD.value)));
        ElementValidator validator = parentElementValidator(
                CTTP.Tag.PROTOCOL,
                validChildren);
        validator.validate(protocol);
    }

    /**
     * Used to validate element "scheme".
     */
    public void validateSchemeElement(Element scheme) throws ChatProtocolController.NotValidChatMessage {
        List<String> validSchemeNames = new ArrayList<>(Arrays.asList(
                CTTP.SCHEME.toLowerCase(), CTTP.SCHEME.toUpperCase()));
        ElementValidator validator = leafElementValidator(
                CTTP.Tag.SCHEME,
                validSchemeNames,
                this::isValidTextContent);
        validator.validate(scheme);
    }

    /**
     * Used to get a element validator that validate leaf element,
     * that is element that contain text with specific values.
     */
    private ElementValidator leafElementValidator(CTTP.Tag tag,
                                                  List<String> validValues,
                                                  BiPredicate<String, List<String>> textPredicate) {
        return element -> {
            if (!Objects.equals(element.name, tag.value)) {
                throw new ChatProtocolController.NotValidChatMessage("Element name should be "
                        + tag.value);
            }

            if (element.getChildren().size() != 0) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + tag.value
                        + " should not contain any child elements.");
            }

            if (!textPredicate.test(element.getText(), validValues)) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + tag.value
                        + " does not contain correct value");
            }
        };
    }

    /**
     * Used as a predicate to validate text content in a leaf element.
     */
    private boolean isValidTextContent(String value, List<String> validValues) {
        return validValues.contains(value);
    }

    /**
     * Used to validate element "version".
     */
    public void validateVersionElement(Element version) throws ChatProtocolController.NotValidChatMessage {
        List<String> validVersionNumbers = new ArrayList<>(Arrays.asList(
                CTTP.MAJOR_VERSION, CTTP.MINOR_VERSION, CTTP.PATCH_VERSION));
        ElementValidator validator = leafElementValidator(
                CTTP.Tag.VERSION,
                validVersionNumbers,
                this::isValidTextContent);
        validator.validate(version);
    }

    /**
     * Used to validate element "method".
     */
    public void validateMethodElement(Element method) throws ChatProtocolController.NotValidChatMessage {
        List<String> validMethods = Arrays.stream(CTTP.Method.values())
                .map(Enum::name)
                .collect(Collectors.toList());
        ElementValidator validator = leafElementValidator(
                CTTP.Tag.METHOD,
                validMethods,
                this::isValidTextContent);
        validator.validate(method);
    }

    /**
     * Used to validate element "body".
     */
    public void validateBodyElement(Element body) throws ChatProtocolController.NotValidChatMessage {
        ElementValidator validator = bodyElementValidator();
        validator.validate(body);
    }

    /**
     * Used to get a element validator that validate the body element.
     */
    private ElementValidator bodyElementValidator() {
        return element -> {
            if (!Objects.equals(element.name, CTTP.Tag.BODY.value)) {
                throw new ChatProtocolController.NotValidChatMessage("Element name should be "
                        + CTTP.Tag.BODY.value);
            }

            if (element.getText().length() == 0) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + CTTP.Tag.BODY.value
                        + " should not be empty.");
            }

            if (element.getText().length() > CTTP.MAX_SIZE_BODY) {
                throw new ChatProtocolController.NotValidChatMessage("Element "
                        + CTTP.Tag.BODY.value
                        + " should be less than: " + CTTP.MAX_SIZE_BODY);
            }
        };
    }
}

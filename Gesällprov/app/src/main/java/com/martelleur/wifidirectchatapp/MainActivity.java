package com.martelleur.wifidirectchatapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.martelleur.wifidirectchatapp.service.ChatService;
import com.martelleur.wifidirectchatapp.ui.MainFragment;
import com.martelleur.wifidirectchatapp.ui.PermissionWarningDialogFragment;


/**
 * Launch activity for the application. The activity is
 * used to launch the main fragment and update app permissions.
 */
public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSIONS = 1; // Used to control permission mgmt.

    /**
     * Launch main fragment if all necessary permission is allowed
     * else show permission warning dialog.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boolean permissionsGranted = checkPermissions();
        if (permissionsGranted) startApplicationComponents();
        else ActivityCompat.requestPermissions(
                this,
                getRequiredPermissions(),
                REQUEST_PERMISSIONS);
    }

    /**
     * Used to check if all necessary permissions is allowed.
     */
    private boolean checkPermissions() {
        boolean permissionsGranted = true;
        for (String permission : getRequiredPermissions()) {
            if (ContextCompat.checkSelfPermission(this, permission) !=
                    PackageManager.PERMISSION_GRANTED) {
                permissionsGranted = false;
                break;
            }
        }
        return permissionsGranted;
    }

    /**
     * Used to get required permissions dependent on android version.
     */
    private String[] getRequiredPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return new String[]{
                    Manifest.permission.POST_NOTIFICATIONS,
                    Manifest.permission.NEARBY_WIFI_DEVICES,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            };
        } else {
            return new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
            };
        }
    }

    /**
     * Used to start application components
     */
    private void startApplicationComponents() {
        // startChatService();
        startMainFragment();
    }

    /**
     * Start foreground chat service if it is not already running.
     */
    private void startChatService() {
        // if (!isChatServiceRunning()) return; // TODO Bug
        Intent intent = new Intent(this, ChatService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
    }

    /**
     * Used to check if chat service is running.
     * TODO! Current implementation not reliable how to implement this check?
     */
    public boolean isChatServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ChatService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    /**
     * Used to start main fragment.
     */
    private void startMainFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow();
    }

    /**
     * Used to request all necessary permissions and
     * start application components if permissions are granted.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean permissionsGranted = true;

        if (requestCode == REQUEST_PERMISSIONS) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                    break;
                }
            }
        }
        if (!permissionsGranted) showWarningDialog();
        else startApplicationComponents();
    }

    /**
     * Used to show permission warning dialog if not all
     * necessary permission is allowed.
     */
    private void showWarningDialog() {
        PermissionWarningDialogFragment warningFragment =
                PermissionWarningDialogFragment.newInstance();
        warningFragment.show(getSupportFragmentManager(), "Warning Dialog");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy()");
    }
}
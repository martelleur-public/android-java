package com.martelleur.wifidirectchatapp.ui;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.wifidirectchatapp.R;
import com.martelleur.wifidirectchatapp.model.Message;

import java.util.List;

/**
 * An adapter used for displaying messages.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    final private int layout;
    private List<Message> messages;

    public MessagesAdapter(int layoutId,
                           List<Message> messages) {
        layout = layoutId;
        this.messages = messages;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setGuestBooks(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of items in the adapter.
     */
    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    /**
     * Used to set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Used to update message items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView messageTitle = holder.messageTitle;
        TextView messageContent = holder.messageContent;
        Message message = messages.get(listPosition);
        String title = message.type + " " + message.getFormattedLocalDate();
        messageTitle.setText(title);
        messageContent.setText(message.content);
    }

    /**
     * A view holder that describes item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView messageTitle;
        TextView messageContent;
        ViewHolder(View itemView) {
            super(itemView);
            messageTitle = itemView.findViewById(R.id.message_title);
            messageContent = itemView.findViewById(R.id.message_content);
        }
    }
}


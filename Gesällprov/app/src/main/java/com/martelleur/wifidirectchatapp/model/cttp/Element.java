package com.martelleur.wifidirectchatapp.model.cttp;


import android.util.Xml;

import androidx.annotation.NonNull;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Utility class used to build elements custom protocol CTTP.
 */
public class Element {
    final String name;
    private String text;
    private final ArrayList<Element> children = new ArrayList<>();

    public Element(String name){
        this.name = name;
    }

    /**
     * Used to set text on an element.
     */
    public Element setText(String text) {
        this.text = text;
        return this;
    }

    /**
     * Used to get text on an element.
     */
    public String getText() {
        return this.text;
    }

    /**
     * Used to set add children element to element.
     */
    public Element addChild(Element child) {
        this.children.add(child);
        return this;
    }

    /**
     * Used to get the elements children.
     */
    public ArrayList<Element> getChildren() {
        return children;
    }

    /**
     * Used to serialize the built element to an xml string.
     */
    public String toXml() throws IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        serializer.setOutput(writer);
        serializer.startDocument("UTF-8", true);
        write(serializer);
        serializer.endDocument();

        return writer.toString();
    }

    /**
     * Used to write the element, the children elements, and
     * elements text recursively.
     */
    private void write(XmlSerializer serializer) throws IOException {
        serializer.startTag("", name);
        if (text != null) {
            serializer.text(text);
        }
        for (Element child : children) {
            child.write(serializer);
        }
        serializer.endTag("", name);
    }

    /**
     * Used to present a string
     * representation of the element.
     */
    @NonNull
    @Override
    public String toString() {
        return toString("");
    }

    /**
     * Used to print xml elements recursively.
     */
    private String toString(String indent) {
        StringBuilder sb = new StringBuilder();
        sb.append(indent).append("<").append(name).append(">\n");
        if (text != null) {
            sb.append(indent).append("  ").append(text).append("\n");
        }
        for (Element child : children) {
            sb.append(child.toString(indent + "  "));
        }
        sb.append(indent).append("</").append(name).append(">\n");
        return sb.toString();
    }

    /**
     * Used to check for equality between elements,
     * used when validating elements.
     * The element are considered equal when the element have same
     * tag name and contain the sam type of children at the first level
     * not recursively. If the element contain no children only the tag
     * name is considered for equality.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Element element = (Element) obj;
        boolean sameName = name.equals(element.name);
        boolean sameChildrenFirstLevel = isSameChildrenFirstLevel(children, ((Element) obj).children);
        return sameName && sameChildrenFirstLevel;
    }

    /**
     * Used to check if children at first level is equal.
     */
    private boolean isSameChildrenFirstLevel(ArrayList<Element> thisChildren,
                                             ArrayList<Element> otherChildren) {
        boolean sameChildren = true;
        if (thisChildren.size() > 0 && otherChildren.size() > 0) {
            List<String> childrenName = thisChildren.stream()
                    .map(item -> item.name)
                    .collect(Collectors.toList());
            List<String> childrenNameObj = otherChildren.stream()
                    .map(item -> item.name)
                    .collect(Collectors.toList());
            sameChildren = new HashSet<>(childrenNameObj).containsAll(childrenName);
        }
        return sameChildren;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, children);
    }
}

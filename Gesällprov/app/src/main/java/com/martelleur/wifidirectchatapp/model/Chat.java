package com.martelleur.wifidirectchatapp.model;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martelleur.wifidirectchatapp.infrastructure.ChatHandlerCallback;
import com.martelleur.wifidirectchatapp.model.cttp.CTTP;
import com.martelleur.wifidirectchatapp.model.cttp.ChatProtocolController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A class used to represent a chat that hold message
 * instances as livedata.
 * This class is implementing ChatHandlerCallback
 * used in ChatSocket when incoming messages arrives
 * on the chat socket. The class is also using a
 * custom chat protocol for creating, parsing, and validating
 * incoming and outgoing messages.
 */
public class Chat implements ChatHandlerCallback {
    private final ChatProtocolController controller;
    private final MutableLiveData<List<Message>> messagesLiveData
            = new MutableLiveData<>(new ArrayList<>());

    public Chat(ChatProtocolController controller) {
        this.controller = controller;
    }

    /**
     * Used to get chat messages.
     */
    public LiveData<List<Message>> getMessagesLiveData() { return messagesLiveData; }

    /**
     * Used to create new messages.
     */
    public String createNewMsg(String message, CTTP.Method cttpMethod) throws ChatException {
        try {
            return controller.writeOutgoingCTTP(message, cttpMethod);
        } catch (ChatProtocolController.NotValidChatMessage e) {
            throw new ChatException(e.getMessage());
        }
    }

    /**
     * Used to handle incoming messages.
     */
    @Override
    public void onIncomingMsg(String content) {
        try {
            ChatProtocolController.Response parsedContent
                    = controller.parseIncomingCTTP(content);
            if (parsedContent.method.equals(CTTP.Method.STOP.value))
                handleStopMessage(controller.parseIncomingCTTP(content));
            else
                handleNewMessage(controller.parseIncomingCTTP(content));
        } catch (ChatProtocolController.NotValidChatMessage e) {
            Log.d("Chat", "Message from peer not valid: " + e.getMessage());
        }
    }

    /**
     * Used to handle a incoming stop message.
     */
    private void handleStopMessage(ChatProtocolController.Response parsedContent) {
        Message message = new Message(
                Message.Type.INCOMING,
                parsedContent.body,
                new Date());
        addMessage(message);
    }

    /**
     * Used to handle a new incoming message.
     */
    private void handleNewMessage(ChatProtocolController.Response parsedContent) {
        Message message = new Message(
                Message.Type.INCOMING,
                parsedContent.body,
                new Date());
        addMessage(message);
    }

    /**
     * Used to handle outgoing messages.
     */
    @Override
    public void onOutgoingMsg(String content) {
        Message message = new Message(Message.Type.OUTGOING, content, new Date());
        addMessage(message);
    }

    /**
     * Used for adding a new message to the list of messages.
     */
    private void addMessage(Message message) {
        List<Message> messages = messagesLiveData.getValue();
        Objects.requireNonNull(messages).add(message);
        messagesLiveData.postValue(messages);
    }

    /**
     * Used to handle error message from socket.
     */
    @Override
    public void onError(String message) {
        Log.d("Chat", "Error message: " + message);
    }

    public static class ChatException extends Exception {
        ChatException(String message) {
            super(message);
        }
    }
}

package com.martelleur.wifidirectchatapp.ui;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.martelleur.wifidirectchatapp.model.Chat;
import com.martelleur.wifidirectchatapp.model.MainRepository;
import com.martelleur.wifidirectchatapp.model.Message;

import java.util.List;

/**
 * A view model class used to store the state of the
 * chat fragment.
 */
public class ChatViewModel extends ViewModel {
    private static ChatViewModel chatViewModel = null;
    private final MainRepository mainRepository;
    private String chatTitle = "";

    private ChatViewModel(MainRepository mainRepository) {
        this.mainRepository = mainRepository;
    }

    /**
     * Used to get chat title
     */
    public String getChatTitle() {
        return chatTitle;
    }

    /**
     * Used to set chat title.
     */
    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    /**
     * Used to get observable chat messages.
     */
    public LiveData<List<Message>> getMessagesLiveData() {
        return mainRepository.getMessagesLiveData();
    }

    /**
     * Used to write message on chat.
     */
    public void writeMessage(String message) throws Chat.ChatException {
        mainRepository.writeMessage(message);
    }

    /**
     * Used to create an instance of this view model.
     * Ref: <a href="https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1">...</a>.
     */
    public static final ViewModelInitializer<ChatViewModel> initializer = new ViewModelInitializer<>(
            ChatViewModel.class,
            creationExtras -> {
                if (chatViewModel == null) {
                    MainRepository mainRepository = MainRepository.getInstance();
                    chatViewModel = new ChatViewModel(mainRepository);
                }
                return chatViewModel;
            }
    );
}

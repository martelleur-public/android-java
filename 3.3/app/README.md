# Spela in och visa film

Prototypen introducerar till hur man använder filmkameran för att spela in film samt hur man spelar upp filmen för användaren.

## Prototyp

Gör en app som möjliggör att:

- Användaren kan spela in film
- Användaren kan spela upp senast inspelade film

Man kan ta hjälp av den app som redan finns i Android (använda en Intent) eller göra en egen kustomiserad variant (svårare).
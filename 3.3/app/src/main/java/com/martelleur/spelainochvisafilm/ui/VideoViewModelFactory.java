package com.martelleur.spelainochvisafilm.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.spelainochvisafilm.infrastructure.VideoManager;
import com.martelleur.spelainochvisafilm.model.VideoRepository;

/**
 * A factory class used for creating an instance of VideoViewModel.
 */
public class VideoViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final VideoRepository repository;
    private final VideoManager videoManager = new VideoManager();

    public VideoViewModelFactory(Application application) {
        this.repository = new VideoRepository(application, videoManager);
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new VideoViewModel(repository);
    }
}

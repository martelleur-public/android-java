package com.martelleur.spelainochvisafilm.ui;

import android.net.Uri;

/**
 * Interface describing methods to be used when an user select a video.
 * The interface is implemented in {@link VideoFragment} and are used in
 * {@link VideoListAdapter}.
 */
public interface VideoItemCallback {
    void onSelect(Uri videoUri);
}

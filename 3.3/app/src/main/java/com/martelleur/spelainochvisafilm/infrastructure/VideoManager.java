package com.martelleur.spelainochvisafilm.infrastructure;

import android.app.Application;
import android.content.Context;
import android.net.Uri;

import androidx.core.content.FileProvider;

import com.martelleur.spelainochvisafilm.R;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to store and acquire persistent video files.
 */
public class VideoManager extends FileProvider {
    private Context context;
    private String videoDirName;
    public VideoManager() {
        super(R.xml.videos);
    }

    public void initialize(Application application,
                           String videoDir) {
        this.context = application;
        this.videoDirName = videoDir;
        createVideoDir();
    }

    /**
     * Used to create an directory used for videos.
     */
    private void createVideoDir() {
        File imageDir = new File(context.getFilesDir(), videoDirName);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
    }

    /**
     * Used to get all stored videos.
     */
    public List<Uri> getAllVideos() {
        String dirPath = context.getFilesDir() + "/" + videoDirName;
        return Stream.of(new File(dirPath).listFiles())
                .filter(file -> !file.isDirectory())
                .map(this::getUriForFile)
                .collect(Collectors.toList());
    }

    /**
     * Used to get a uri for a file.
     */
    private Uri getUriForFile(File file) {
        return getUriForFile(context,
                "com.martelleur.spelainochvisafilm.fileprovider",
                file);
    }

    /**
     * Used to get uri for a new file.
     */
    public Uri getUriForNewVideoFile(String fileName) {
        String filePath = context.getFilesDir() + "/"
                + videoDirName + "/" + fileName;
        File file = new File(filePath);
        return getUriForFile(context,
                "com.martelleur.spelainochvisafilm.fileprovider",
                file);
    }
}

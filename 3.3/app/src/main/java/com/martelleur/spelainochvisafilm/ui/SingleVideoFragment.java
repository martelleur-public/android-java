package com.martelleur.spelainochvisafilm.ui;

import android.app.Application;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.martelleur.spelainochvisafilm.databinding.SingleVideoFragmentBinding;

/**
 * Used to display a single recorded video.
 */
public class SingleVideoFragment extends Fragment {
    private static final String ARG_STRING_URI = "";
    private VideoViewModel videoViewModel;
    SingleVideoFragmentBinding binding;
    MediaController mediaController;


    public SingleVideoFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static SingleVideoFragment newInstance(String filePath) {
        SingleVideoFragment fragment = new SingleVideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_STRING_URI, filePath);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Bind layout using view binding
     * SingleImageFragmentBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        binding = SingleVideoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Create view model instance, add a recycler for added images.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        videoViewModel = getVideoViewModel();
        if (getArguments() != null) {
            String uriString = getArguments().getString(ARG_STRING_URI);
            configureVideoView(uriString);
        }
        setupOnVideoListener();
    }

    /**
     * Used to get an instance of video view model.
     */
    private VideoViewModel getVideoViewModel() {
        Application application = requireActivity().getApplication();
        VideoViewModelFactory factory = new VideoViewModelFactory(application);
        return new ViewModelProvider(requireActivity(), factory).get(VideoViewModel.class);
    }

    /**
     * Add media controller to the video view and start playing video
     * in a loop.
     */
    private void configureVideoView(String uriString) {
        binding.singleVideoView.setVideoURI(Uri.parse(uriString));
        mediaController = new MediaController(requireContext());
        mediaController.setAnchorView(binding.singleVideoView);
        binding.singleVideoView.setMediaController(mediaController);
        binding.singleVideoView.setOnPreparedListener(mp -> mp.setLooping(true));
        binding.singleVideoView.start();
    }


    /**
     * Add a click listener for returning and close this fragment.
     */
    private void setupOnVideoListener() {
        binding.singleVideoView.setOnClickListener(view -> {
            stopVideo();
            navigateBackToPreviousFragment();
        });
    }

    /**
     * Used to stop the video.
     */
    private void stopVideo() {
        binding.singleVideoView.stopPlayback();
        mediaController = null;
    }

    /**
     * Used to navigate back to previous fragment.
     */
    private void navigateBackToPreviousFragment() {
        FragmentManager fragmentManager =  getParentFragmentManager();
        fragmentManager.popBackStack(
                VideoFragment.transactionFromVideoFragmentToSingleVideoFragment,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopVideo();
        binding = null;
    }
}

package com.martelleur.spelainochvisafilm.ui;

import android.app.Application;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.spelainochvisafilm.R;
import com.martelleur.spelainochvisafilm.databinding.VideoFragmentBinding;

import java.util.List;

/**
 * Fragment used for record new videos and show all videos
 * recorded with this app.
 */
public class VideoFragment extends Fragment {
    private VideoFragmentBinding binding;
    private VideoViewModel videoViewModel;
    private VideoListAdapter videoListAdapter;

    static final String transactionFromVideoFragmentToSingleVideoFragment
            = "fromVideoFragmentToSingleVideoFragment";

    public VideoFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static VideoFragment newInstance() {
        return new VideoFragment();
    }

    /**
     * Bind layout using view binding instance
     * VideoFragmentBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = VideoFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Create view model instance, add a recycler for added videos.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Application application = requireActivity().getApplication();
        VideoViewModelFactory factory = new VideoViewModelFactory(application);
        videoViewModel = new ViewModelProvider(requireActivity(), factory).get(VideoViewModel.class);
        setupTakeVideoListener();
        setupVideoRecycler(videoViewModel.getVideosLiveData().getValue());
        setupVideosObserver();
    }


    /**
     * Add a listener that launch the activity launcher cameraLauncher.
     */
    private void setupTakeVideoListener() {
        binding.takeVideo.setOnClickListener(view ->
                videoLauncher.launch(videoViewModel.getUriForNewVideoFile()));
    }

    /**
     * ActivityResultLauncher used to launch video application and
     * update video live data if video was recorded successfully.
     */
    private final ActivityResultLauncher<Uri> videoLauncher  =
            registerForActivityResult(new ActivityResultContracts.CaptureVideo(), isSuccessful -> {
                if (isSuccessful != null && isSuccessful) {
                    videoViewModel.updateVideosLiveData();
                } else {
                    boolean isUpdated = videoViewModel.updateVideosLiveData();
                    if (!isUpdated) notifyVideoNotRecorded();
                }
            });

    /**
     * Used to notify user that video was not recorded.
     */
    private void notifyVideoNotRecorded() {
        int msg = R.string.video_not_recorded;
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Set up a recycler view with an adapter
     * that displays recorded videos with this application.
     */
    private void setupVideoRecycler(List<Uri> videos) {
        videoListAdapter = new VideoListAdapter(
                R.layout.video_item,
                videos,
                videoItemCallback);
        RecyclerView recyclerView = binding.videoRecycler;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(videoListAdapter);
    }

    /**
     * Callback used to view a video in full screen with SingleVideosFragment.
     */
    private final VideoItemCallback videoItemCallback = videoUri -> {
        FragmentManager fragmentManager = getParentFragmentManager();
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, SingleVideoFragment.newInstance(videoUri.toString()))
                .addToBackStack(transactionFromVideoFragmentToSingleVideoFragment)
                .commit();
    };

    /**
     * Add an observer that update the adapter and by so the recycler view
     * when a new video have been recorded and stored.
     */
    private void setupVideosObserver() {
        videoViewModel.getVideosLiveData().observe(requireActivity(), videos ->
                videoListAdapter.setVideoList(videos));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}


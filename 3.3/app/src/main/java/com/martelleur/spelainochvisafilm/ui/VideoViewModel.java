package com.martelleur.spelainochvisafilm.ui;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.spelainochvisafilm.model.VideoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * A view model used to communicate application functionality with
 * a repository and store state for videos livedata.
 */
public class VideoViewModel extends ViewModel {
    VideoRepository repository; // A repository used for to delegate persistent storage functionality.
    @NonNull
    private final MutableLiveData<List<Uri>> videosLiveData =
            new MutableLiveData<>(new ArrayList<>()); // Video live data used to observe change in stored videos.

    public VideoViewModel(VideoRepository repository) {
        this.repository = repository;
        videosLiveData.setValue(this.repository.getAllVideos());
    }

    /**
     * Used to get video live data.
     */
    @NonNull
    public MutableLiveData<List<Uri>> getVideosLiveData() {
        return videosLiveData;
    }

    /**
     * Update video live data if not the number of
     * files are the as before.
     */
    public boolean updateVideosLiveData() {
        if (this.repository.getAllVideos().size() ==
                videosLiveData.getValue().size()) {
            return false;
        }
        videosLiveData.setValue(this.repository.getAllVideos());
        return true;
    }

    /**
     * Used to get a random filename.
     */
    private String getRandomFileName() {
        int minCharacter = 97; // represent A
        int maxCharacter = 122; // represent Z
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(2);
        for(int i = 0; i < 2; i++) {
            int character = minCharacter + random.nextInt() * (maxCharacter - minCharacter + 1);
            buffer.append(character);
        }
        return buffer.toString();
    }

    /**
     * Used to get the uri for a new created video file.
     */
    public Uri getUriForNewVideoFile() {
        String fileIndex = ".mp4";
        String fileName = getRandomFileName() + fileIndex;
        return repository.getUriForNewVideoFile(fileName);
    }
}

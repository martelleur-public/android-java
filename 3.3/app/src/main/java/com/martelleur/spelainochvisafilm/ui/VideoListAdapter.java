package com.martelleur.spelainochvisafilm.ui;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.spelainochvisafilm.R;

import java.util.List;

/**
 * An adapter used to when displaying video items.
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    final private int videoItemLayout;
    private List<Uri> videos;
    private VideoItemCallback videoItemCallback;

    public VideoListAdapter(int layoutId,
                            List<Uri> videos,
                            VideoItemCallback videoItemCallback) {
        videoItemLayout = layoutId;
        this.videos = videos;
        this.videoItemCallback = videoItemCallback;
    }

    /**
     * Used to update the video list.
     */
    public void setVideoList(List<Uri> videos) {
        this.videos = videos;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of videos.
     */
    @Override
    public int getItemCount() {
        return videos == null ? 0 : videos.size();
    }

    /**
     * Used to set layout for video items.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(videoItemLayout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update video item views
     * at the given position of videos.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        VideoView videoView = holder.videoView;
        setupVideoOnPreparedListener(videoView, videos.get(listPosition));
        setupSelectImageListener(videoView, videos.get(listPosition));
    }

    /**
     * Used to display video after 0 mille sec when video is ready.
     */
    private void setupVideoOnPreparedListener(VideoView videoView, Uri videoUri) {
        videoView.setVideoURI(videoUri);
        videoView.setOnPreparedListener(mp ->
                mp.seekTo(0));
    }

    /**
     * Used to communicate with the caller that a user have clicked on the video.
     */
    private void setupSelectImageListener(VideoView videoView, Uri videoUri) {
        videoView.setOnClickListener(view ->
                videoItemCallback.onSelect(videoUri));
    }

    /**
     * A ViewHolder which describes a video items.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        VideoView videoView;
        ViewHolder(View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.video_view_item);
        }
    }
}

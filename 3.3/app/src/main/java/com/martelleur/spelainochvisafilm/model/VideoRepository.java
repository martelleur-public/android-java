package com.martelleur.spelainochvisafilm.model;

import android.app.Application;
import android.net.Uri;

import com.martelleur.spelainochvisafilm.infrastructure.VideoManager;

import java.io.File;
import java.util.List;

/**
 * A repository used to access persistent data
 * for storing and acquiring file videos.
 */
public class VideoRepository {
    private final VideoManager videoManager;

    public VideoRepository(Application application, VideoManager videoManager) {
        this.videoManager = videoManager;
        String imageDir = "videos";
        this.videoManager.initialize(application, imageDir);
    }

    /**
     * Used to get all videos.
     */
    public List<Uri> getAllVideos() {
        return  videoManager.getAllVideos();
    }

    /**
     * Used to get uri for a new file.
     */
    public Uri getUriForNewVideoFile(String fileName) {
        return videoManager.getUriForNewVideoFile(fileName);
    }

}

package com.martelleur.anvandawifip2p;

import android.app.Application;

import com.martelleur.anvandawifip2p.model.Chat;
import com.martelleur.anvandawifip2p.model.cttp.ChatProtocolController;
import com.martelleur.anvandawifip2p.model.MainRepository;

/**
 * Extending application and initialize MainRepository with a chat.
 * This class is registered in the AndroidManifest.xml file, and an
 * instance of this class will be created when the application is started.
 */
public class WifiP2pChatApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ChatProtocolController chatProtocolController = new ChatProtocolController();
        Chat chat = new Chat(chatProtocolController);
        MainRepository.initialize(chat);
    }
}

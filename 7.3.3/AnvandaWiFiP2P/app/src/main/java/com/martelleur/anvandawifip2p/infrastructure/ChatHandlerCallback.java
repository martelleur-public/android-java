package com.martelleur.anvandawifip2p.infrastructure;

/**
 * Interface defining methods to be used as callbacks
 * for network communication events.
 */
public interface ChatHandlerCallback {
    void onIncomingMsg(String content);
    void onOutgoingMsg(String content);
    void onError(String message);
}

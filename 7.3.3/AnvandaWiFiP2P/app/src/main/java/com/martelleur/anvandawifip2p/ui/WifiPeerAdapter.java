package com.martelleur.anvandawifip2p.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvandawifip2p.R;

import java.util.List;


/**
 * An adapter used for displaying WiFi peers devices
 * and delegate user events back to MainFragment using
 * WifiPeerCallback methods.
 */
public class WifiPeerAdapter extends RecyclerView.Adapter<WifiPeerAdapter.ViewHolder> {
    private final int layout;
    private List<WifiP2pDevice> peers;
    private final WifiPeerCallback wifiPeerCallback;
    private final Context context;
    private boolean activeConnection;

    public WifiPeerAdapter(int layoutId,
                           List<WifiP2pDevice> peers,
                           WifiPeerCallback wifiPeerCallback,
                           boolean activeConnection,
                           Context context) {
        this.layout = layoutId;
        this.peers = peers;
        this.wifiPeerCallback = wifiPeerCallback;
        this.activeConnection = activeConnection;
        this.context = context;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setPeers(List<WifiP2pDevice> peers) {
        this.peers = peers;
        notifyDataSetChanged();
    }

    /**
     * Used to update the active connection state
     * that is used as a decider for how to update the UI
     * for each WiFi peer item.
     */
    public void setActiveConnection(boolean activeConnection) {
        this.activeConnection = activeConnection;
    }

    /**
     * Used to get the number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return peers == null ? 0 : peers.size();
    }

    /**
     * Inflate layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update WiFi peer view items and add user
     * listener on each items to be used to communicate back
     * to the caller.
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int listPosition) {
        WifiP2pDevice peer = peers.get(listPosition);
        holder.peerName.setText(context.getString(R.string.peer_device_name, peer.deviceName));
        holder.peerMAC.setText(context.getString(R.string.peer_mac, peer.deviceAddress));

        String status;
        switch (peer.status) {
            case WifiP2pDevice.UNAVAILABLE:
                status = context.getString(R.string.connection_status, "Unavailable");
                break;
            case WifiP2pDevice.CONNECTED:
                holder.channelControlContainer.setVisibility(View.VISIBLE);
                holder.callChannel.setVisibility(View.GONE);
                status = context.getString(R.string.connection_status, "Connected");
                setupReturnToPeerListener(holder.returnToChannel);
                setupRemoveChannelListener(holder.removeChannel);
                break;
            case WifiP2pDevice.INVITED:
                status = context.getString(R.string.connection_status, "Invited");
                break;
            case WifiP2pDevice.FAILED:
                status = context.getString(R.string.connection_status, "failed");
                break;
            default: // Assume Available.
                if (activeConnection) holder.callChannel.setVisibility(View.GONE);
                else holder.callChannel.setVisibility(View.VISIBLE);
                holder.channelControlContainer.setVisibility(View.GONE);
                status = context.getString(R.string.connection_status, "Available");
                setupUpConnectToPeerListener(holder.callChannel, peer);
                break;
        }
        holder.peerStatus.setText(status);
    }

    /**
     * A listener that return to a connected
     * channel when a user select return to channel.
     */
    private void setupReturnToPeerListener(ImageView returnToChannel) {
        returnToChannel.setOnClickListener(view -> wifiPeerCallback.onOpenChat());
    }

    /**
     * A listener that show a dialog for removing a
     * channel or not dependent on what user select.
     */
    private void setupRemoveChannelListener(ImageView removeChannel) {
        removeChannel.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.delete_channel_dialog_title));
            builder.setMessage(context.getString(R.string.delete_channel_dialog_text));
            builder.setPositiveButton(
                    context.getString(R.string.delete_channel_dialog_yes),
                    (dialogInterface, i) -> wifiPeerCallback.onDisconnectFromChannel());
            builder.setNegativeButton(
                    context.getString(R.string.delete_channel_dialog_no),
                    (dialogInterface, i) -> Toast.makeText(
                            context,
                            context.getString(R.string.channel_not_deleted),
                            Toast.LENGTH_LONG).show());
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }

    /**
     * A listener that try to connect to peer when the user select
     * a peer item.
     */
    private void setupUpConnectToPeerListener(ImageView wifiCall, WifiP2pDevice peer) {
        wifiCall.setOnClickListener(view -> wifiPeerCallback.onConnectToChannel(peer));
    }

    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView peerName;
        TextView peerMAC;
        TextView peerStatus;
        ImageView callChannel;
        ImageView returnToChannel;
        ImageView removeChannel;
        LinearLayout channelControlContainer;
        ViewHolder(View itemView) {
            super(itemView);
            peerName = itemView.findViewById(R.id.name);
            peerMAC = itemView.findViewById(R.id.mac);
            peerStatus = itemView.findViewById(R.id.status);
            callChannel = itemView.findViewById(R.id.wifi_call_channel);
            returnToChannel = itemView.findViewById(R.id.return_to_wifi_channel);
            removeChannel = itemView.findViewById(R.id.remove_wifi_channel);
            channelControlContainer = itemView.findViewById(R.id.connected_channel_control_container);
        }
    }
}

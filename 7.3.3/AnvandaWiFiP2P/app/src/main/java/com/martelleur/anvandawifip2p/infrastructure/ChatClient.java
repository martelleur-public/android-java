package com.martelleur.anvandawifip2p.infrastructure;

import android.util.Log;

import com.martelleur.anvandawifip2p.model.cttp.CTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A Chat Socket implemented as a socket tcp client
 * used for communicating with peer device.
 * The peer device have previous been connected with
 * wifi direct where one device is the group owner
 * and by so working as the socket server.
 */
public class ChatClient implements Runnable, ChatSocket {
    private final InetAddress SERVER;
    private final int PORT;
    private final ChatHandlerCallback chatHandler;
    private final AtomicBoolean socketIsOpen = new AtomicBoolean(false);
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;


    public ChatClient(InetAddress SERVER,
                      int PORT,
                      ChatHandlerCallback chatHandler) {
        this.SERVER = SERVER;
        this.PORT = PORT;
        this.chatHandler = chatHandler;
    }

    @Override
    public void run() {
        this.socketIsOpen.set(true);
        connectToServer();
    }

    /**
     * Used to open socket on given host and port.
     */
    private void connectToServer() {
        try {
            socket = new Socket(SERVER, PORT);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socket.setKeepAlive(true);
            startListeningOnIncomingMessages();
        } catch (IOException e) {
            this.chatHandler.onError(e.getMessage());
            closeSocket();
        }
    }

    /**
     * Used to listen and handle incoming messages.
     */
    private void startListeningOnIncomingMessages() throws IOException {
        if (!socketIsOpen.get()) return;
        char[] msgChar = new char[CTTP.MAX_SIZE_BODY];
        int size;
        while (socketIsOpen.get() && (size = in.read(msgChar, 0, CTTP.MAX_SIZE_BODY)) != -1) {
            this.chatHandler.onIncomingMsg(new String(msgChar, 0, size));
        }
    }

    /**
     * Used to write outgoing messages.
     */
    @Override
    public void writeMessage(String message) {
        if (!socketIsOpen.get()) return;
        out.println(message);
    }

    /**
     * Used to close socket resources.
     */
    @Override
    public void closeSocket() {
        socketIsOpen.set(false);
        try {
            if (socket != null) {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            }
            if (in != null) in.close();
            if (out != null) out.close();
        } catch (IOException e) {
            Log.d("ChatSocket", "Error close socket: " + e.getMessage());
        }
    }
}

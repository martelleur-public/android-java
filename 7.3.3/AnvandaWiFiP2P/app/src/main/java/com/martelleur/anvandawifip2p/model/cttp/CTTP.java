package com.martelleur.anvandawifip2p.model.cttp;

/**
 * A class used to define custom protocol
 * Chat Text Transport Protocol (CTTP)
 * The class define required elements tag names, valid size,
 * and valid content in required elements.
 */
public class CTTP {
    /**
     * The max size of the body.
     */
    public final static int MAX_SIZE_BODY = 1024;

    /**
     * De scheme name for the protocol.
     */
    public final static String SCHEME = "CTTP";

    /**
     * Patch, Minor, and Major version of CTTP
     */
    public final static String PATCH_VERSION = "1.0.0";
    public final static String MINOR_VERSION = "1.0";
    public final static String MAJOR_VERSION = "1";

    /**
     * Required elements tag names.
     */
    public enum Tag {
        ROOT("message"),
        HEADER("header"),
        PROTOCOL("protocol"),
        SCHEME("scheme"),
        VERSION("version"),
        METHOD("method"),
        BODY("body");

        public final String value;

        Tag(String tagName) {
            this.value = tagName;
        }
    }

    /**
     * Valid methods that can be used in header field "method".
     */
    public enum Method {
        MESSAGE("MESSAGE"),
        STOP("STOP");

        public final String value;

        Method(String value) {
            this.value = value;
        }
    }
}
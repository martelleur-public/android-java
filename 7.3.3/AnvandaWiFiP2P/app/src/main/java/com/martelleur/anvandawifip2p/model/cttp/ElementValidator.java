package com.martelleur.anvandawifip2p.model.cttp;

/**
 * Interface defining operation for
 * validating an element in custom protocol;
 * Chat Text Transport Protocol (CTTP).
 * The interface is used in ProtocolValidator.
 */
public interface ElementValidator {
    void validate(Element element) throws ChatProtocolController.NotValidChatMessage;
}

package com.martelleur.anvandawifip2p.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.martelleur.anvandawifip2p.MainActivity;
import com.martelleur.anvandawifip2p.R;
import com.martelleur.anvandawifip2p.model.MainRepository;

import java.net.InetAddress;


/**
 * A foreground bounded service that is used for
 * starting and stopping a socket that listen on incoming
 * messages from the WiFi connected peer device.
 */
public class ChatService extends Service {
    private static final int NOTIFICATION_ID = 1;
    private static final String NOTIFICATION_CHANNEL_ID = "com.martelleur.anvandawifip2p.service.ChatService.NOTIFICATION_CHANNEL";
    private static final String NOTIFICATION_CHANNEL_NAME = "Chat Service Channel";
    private final IBinder chatServiceBinder = new ChatServiceBinder();
    private static final MainRepository mainRepository = MainRepository.getInstance();

    public ChatService() {}

    public class ChatServiceBinder extends Binder {
        public ChatService getService() {
            return ChatService.this;
        }
    }

    /**
     * On start command create a required notification
     * for this foreground service.
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createNotificationChannel();
        PendingIntent notificationIntent = getPendingNotificationIntent();
        Notification notification = getNotification(notificationIntent);
        startForeground(NOTIFICATION_ID, notification);
        return START_STICKY;
    }

    /**
     * Used to get an pending intent that is used to open
     * the main activity when a user click on the notification.
     */
    private PendingIntent getPendingNotificationIntent() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        return PendingIntent.getActivity(
                this,
                0,
                notificationIntent,
                PendingIntent.FLAG_IMMUTABLE);
    }

    /**
     * Used to build and get and notification to be
     * used for the foreground service.
     */
    private Notification getNotification(PendingIntent notificationIntent) {
        return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getString(R.string.chat_service_notification_title))
                .setContentText(getString(R.string.chat_service_notification_text))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(notificationIntent)
                .build();
    }

    /**
     * Used to create the notification channel.
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    /**
     * Used to start the chat.
     */
    public void startChat(boolean isServer, InetAddress serverAddress) {
        mainRepository.openChat(isServer, serverAddress);
    }

    /**
     * Used to close the chat.
     */
    public void closeChat() {
        mainRepository.closeChat();
    }

    /**
     * Used to write a close message to the peer device.
     */
    public boolean writeCloseMessageToPeer(String message) {
        return mainRepository.writeCloseMessage(message);
    }

    /**
     * Return binder object to be used to get
     * an instance of this service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return chatServiceBinder;
    }

    /**
     * Use to unbind this service.
     */
    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /**
     * Called when destroying this service.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("ChatService", "Chat service destroyed.");
    }
}

package com.martelleur.anvandawifip2p.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A class used to represent a chat message.
 */
public class Message {
    public final Type type;
    public final String content;
    private final Date date;

    public Message(Type type,
                   String content,
                   Date date) {
        this.type = type;
        this.content = content;
        this.date = date;
    }

    public enum Type {
        INCOMING("Incoming message"),
        OUTGOING("Outgoing message");

        private final String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * Used to get a local formatted date for the message date.
     */
    public String getFormattedLocalDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        return formatter.format(date);
    }
}

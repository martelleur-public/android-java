package com.martelleur.anvandawifip2p.ui;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvandawifip2p.R;
import com.martelleur.anvandawifip2p.databinding.FragmentMainBinding;
import com.martelleur.anvandawifip2p.service.ChatService;

import java.util.Collection;


/**
 * Fragment use for displaying wifi peers
 * which a user can try to connect to.
 */
@SuppressLint("MissingPermission")
public class MainFragment extends Fragment {
    private FragmentMainBinding binding;
    private MainViewModel mainViewModel;
    private ChatService chatService;
    private boolean isChatServiceBound = false;
    private WifiP2pManager wifiP2pManager;
    private WifiP2pManager.Channel wifiP2pChannel;
    private WifiPeerAdapter wifiPeerAdapter;
    private final String transactionToChatFragment = "fromMainFragmentToChatFragment";
    private final IntentFilter wifiP2pIntentFilter = new IntentFilter();
    private final Handler discoverPeerHandler = new Handler(Looper.getMainLooper());
    private final Runnable discoverPeerRunnable = new Runnable() {
        @Override
        public void run() {
            wifiP2pManager.discoverPeers(wifiP2pChannel, null);
            discoverPeerHandler.postDelayed(this, 10000);
        }
    };
    private final Handler returnNavigationHandler = new Handler(Looper.getMainLooper());
    private final Runnable returnNavigationRunnable = () -> {
        Log.d("MainFragment", "Navigation runner called");
        getParentFragmentManager().popBackStack(
                transactionToChatFragment,
                FragmentManager.POP_BACK_STACK_INCLUSIVE);
    };

    /**
     * Used to get an instance of this fragment.
     */
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    /**
     * Initiate WifiP2pManager and a WifiP2pManager.Channel that are used for
     * wifi P2P operations.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wifiP2pManager = (WifiP2pManager) requireContext().getSystemService(Context.WIFI_P2P_SERVICE);
        wifiP2pChannel = wifiP2pManager.initialize(requireActivity(), requireContext().getMainLooper(), null);
        setupWifiP2pIntentFilter();
    }

    /**
     * Bind to chat service on start.
     */
    @Override
    public void onStart() {
        super.onStart();
        Intent intent = new Intent(requireActivity(), ChatService.class);
        requireContext().bindService(intent, chatServiceConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Chat service callbacks for service binding.
     */
    private final ServiceConnection chatServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            ChatService.ChatServiceBinder binder = (ChatService.ChatServiceBinder) service;
            chatService = binder.getService();
            isChatServiceBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            isChatServiceBound = false;
        }
    };

    /**
     * Used to setup WiFi P2P broadcast intent filter.
     */
    private void setupWifiP2pIntentFilter() {
        wifiP2pIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        wifiP2pIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        wifiP2pIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        wifiP2pIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    /**
     * Used to bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater, container, false);
        mainViewModel = getViewModel();
        return binding.getRoot();
    }

    /**
     * Used to get the view model instance.
     */
    private MainViewModel getViewModel() {
        return new ViewModelProvider(
                this,
                ViewModelProvider.Factory.from(MainViewModel.initializer))
                .get(MainViewModel.class);
    }

    /**
     * When view is created setup
     * 1) Peer recycler displaying discovered peers.
     * 2) Setup discover wifi peers listener.
     * 3) Navigate to chat if wifi grouped is formed, e.g. by peer device.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupWifiP2pPeerRecycler();
        setupDiscoverWifiPeersHandler();
        if (mainViewModel.isWifiP2pGroupFormed()) {
            navigateToChatFragment(mainViewModel.getChatTitle());
        }
    }

    /**
     * Used to configure and setup a recycler view that display
     * discovered peers.
     */
    private void setupWifiP2pPeerRecycler() {
        wifiPeerAdapter = new WifiPeerAdapter(
                R.layout.peer_item,
                mainViewModel.getPeers(),
                wifiPeerCallback,
                mainViewModel.isWifiP2pGroupFormed(),
                requireContext());
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setAdapter(wifiPeerAdapter);
    }

    /**
     * Setup handler that execute discover peers every 10 seconds.
     * If peers is detected the system broadcasts the
     * WIFI_P2P_PEERS_CHANGED_ACTION intent.
     */
    private void setupDiscoverWifiPeersHandler() {
        discoverPeerHandler.post(discoverPeerRunnable);
    }

    /**
     * Register broadcast receiver for wifi P2P.
     */
    @Override
    public void onResume() {
        super.onResume();
        requireContext().registerReceiver(wifiP2PReceiver, wifiP2pIntentFilter);
    }

    /**
     * Broadcast receiver used to observe and act on system
     * event related to WiFi P2P.
     */
    private final BroadcastReceiver wifiP2PReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (wifiP2pManager == null) return;
            String action = intent.getAction();
            if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
                wifiP2pManager.requestPeers(
                        wifiP2pChannel,
                        wifiPeerListListener);
                updateLayoutOnWifiP2pStateChange(intent.getIntExtra(
                        WifiP2pManager.EXTRA_WIFI_STATE,
                        -1));
            } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
                wifiP2pManager.requestPeers(
                        wifiP2pChannel,
                        wifiPeerListListener);
            } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
                wifiP2pManager.requestConnectionInfo(
                        wifiP2pChannel,
                        wifiConnectionInfoListener);
            } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
                wifiP2pManager.requestPeers(
                        wifiP2pChannel,
                        wifiPeerListListener);
            }
        }
    };

    /**
     * Callback used to update peers if available peers have changed.
     */
    private final WifiP2pManager.PeerListListener wifiPeerListListener = wifiP2pDeviceList -> {
        Collection<WifiP2pDevice> discoverablePeers = wifiP2pDeviceList.getDeviceList();
        if (!discoverablePeers.equals(mainViewModel.getPeers())) {
            mainViewModel.updatePeers(discoverablePeers);
            wifiPeerAdapter.setPeers(mainViewModel.getPeers());
        }
        updateLayoutOnPeerChange();
    };

    /**
     * Used to update layout when peers are changed.
     */
    private void updateLayoutOnPeerChange() {
        if (mainViewModel.getPeers().size() == 0) {
            binding.loader.setVisibility(View.VISIBLE);
            binding.recycler.setVisibility(View.GONE);
            binding.wifiDirectNotEnabledContainer.setVisibility(View.GONE);
        } else {
            binding.recycler.setVisibility(View.VISIBLE);
            binding.wifiDirectNotEnabledContainer.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
        }
    }

    /**
     * Used to update layout dependent when wifi P2P
     * state are changed.
     */
    private void updateLayoutOnWifiP2pStateChange(int state) {
        if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED && mainViewModel.getPeers().size() == 0) {
            binding.loader.setVisibility(View.VISIBLE);
            binding.recycler.setVisibility(View.GONE);
            binding.wifiDirectNotEnabledContainer.setVisibility(View.GONE);
        } else if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
            binding.recycler.setVisibility(View.VISIBLE);
            binding.wifiDirectNotEnabledContainer.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
        } else {
            binding.wifiDirectNotEnabledContainer.setVisibility(View.VISIBLE);
            binding.recycler.setVisibility(View.GONE);
            binding.loader.setVisibility(View.GONE);
        }
    }

    /**
     * Callback used to get connection info when WiFiP2p
     * connection have changed.
     * If a wifi group is established then handle start the chat
     * else handle close the chat.
     */
    private final WifiP2pManager.ConnectionInfoListener wifiConnectionInfoListener = wifiP2pInfo -> {
        if (wifiP2pInfo.groupFormed && wifiP2pInfo.groupOwnerAddress != null)
            handleStartChat(wifiP2pInfo);
        else
            handleCloseChat();
    };

    /**
     * A handler used when wifi peer connection is formed.
     * The handler is used to update state, starting a chat,
     * and navigate to chat fragment.
     */
    private void handleStartChat(WifiP2pInfo wifiP2pInfo) {
        String chatTitle = wifiP2pInfo.groupOwnerAddress.toString();
        mainViewModel.setChatTitle(chatTitle);
        mainViewModel.setWifiP2pGroupFormed(true);
        wifiPeerAdapter.setActiveConnection(mainViewModel.isWifiP2pGroupFormed());
        wifiPeerAdapter.setPeers(mainViewModel.getPeers());
        startChat(wifiP2pInfo);
        navigateToChatFragment(mainViewModel.getChatTitle());
    }

    /**
     * Used to start a chat with a foreground service.
     */
    private void startChat(WifiP2pInfo wifiP2pInfo) {
        Intent intent = new Intent(requireContext(), ChatService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            requireContext().startForegroundService(intent);
        else
            requireContext().startService(intent);
        chatService.startChat(
                wifiP2pInfo.isGroupOwner,
                wifiP2pInfo.groupOwnerAddress);
    }

    /**
     * Used to navigate to chat fragment when a
     * connection is established.
     */
    private void navigateToChatFragment(String chatTitle) {
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, ChatFragment.newInstance(chatTitle))
                .addToBackStack(transactionToChatFragment)
                .commit();
    }

    /**
     * A handler used when closing the wifi peer connection.
     * The handler is used to close the chat connection, and
     * navigate back to this fragment if a wifi grouped have
     * previous been established.
     */
    private void handleCloseChat() {
        chatService.closeChat();
        if (mainViewModel.isWifiP2pGroupFormed()) {
            mainViewModel.setWifiP2pGroupFormed(false);
            wifiPeerAdapter.setActiveConnection(mainViewModel.isWifiP2pGroupFormed());
            wifiPeerAdapter.setPeers(mainViewModel.getPeers());
            Toast.makeText(requireContext(),
                    getString(R.string.channel_deleted_by_peer),
                    Toast.LENGTH_SHORT).show();
            returnNavigationHandler.postDelayed(returnNavigationRunnable, 5000);
        }
    }

    /**
     * Callback for when a user select a peer device from the
     * WiFi peer adapter list.
     */
    private final WifiPeerCallback wifiPeerCallback = new WifiPeerCallback() {
        @Override
        public void onOpenChat() { handleOnOpenChat(); }
        @Override
        public void onConnectToChannel(WifiP2pDevice peer) {
            handleOnConnectToChannel(peer);
        }
        @Override
        public void onDisconnectFromChannel() {
            handleOnDisconnectFromChannel();
        }
    };

    /**
     * Handler used to open a chat when a user select a chat.
     */
    private void handleOnOpenChat() {
        wifiP2pManager.requestConnectionInfo(
                wifiP2pChannel,
                wifiConnectionInfoListener);
    }

    /**
     * Handler used when a user select a device to connect to
     * the handle is calling wifiP2pManager connect with an
     * WifiP2pManager.ActionListener that toast the user
     * if connection is successful or not.
     */
    private void handleOnConnectToChannel(WifiP2pDevice peer) {
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = peer.deviceAddress;
        wifiP2pManager.connect(wifiP2pChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(requireContext(),
                        getString(
                        R.string.connect_channel_success,
                        peer.deviceName,
                        peer.deviceAddress),
                        Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(int reason) {
                String reasonText = "";
                if (reason == WifiP2pManager.P2P_UNSUPPORTED)
                    reasonText = "Wifi P2P not supported by peer";
                else if (reason == WifiP2pManager.BUSY)
                    reasonText = "Peer device busy";
                else if (reason == WifiP2pManager.ERROR)
                    reasonText = "Wifi P2P error";
                Toast.makeText(requireContext(),
                        getString(
                                R.string.connect_channel_fail,
                                reasonText),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Handler used when a user select a device to disconnect from.
     */
    private void handleOnDisconnectFromChannel() {
        wifiP2pManager.removeGroup(wifiP2pChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                boolean isMessageWritten = chatService.writeCloseMessageToPeer(
                        getString(R.string.channel_deleted_by_peer));
                if (!isMessageWritten) {
                    Toast.makeText(requireContext(),
                            getString(R.string.failed_send_close_msg),
                            Toast.LENGTH_LONG).show();
                }
                mainViewModel.setWifiP2pGroupFormed(false);
                wifiPeerAdapter.setActiveConnection(false);
                wifiPeerAdapter.setPeers(mainViewModel.getPeers());
                Toast.makeText(requireContext(),
                        getString(R.string.delete_channel_success),
                        Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(int reason) {
                CharSequence msg = getText(R.string.delete_channel_fail);
                Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }


    /**
     * Unregister receiver for wifi P2P.
     */
    @Override
    public void onPause() {
        super.onPause();
        requireContext().unregisterReceiver(wifiP2PReceiver);
    }

    /**
     * Unbind service on stop.
     */
    @Override
    public void onStop() {
        super.onStop();
        if (isChatServiceBound) {
            requireContext().unbindService(chatServiceConnection);
        }
        returnNavigationHandler.removeCallbacks(returnNavigationRunnable);
        discoverPeerHandler.removeCallbacks(discoverPeerRunnable);
    }

    /**
     * Destroy view layout binding and unregister discover peer handler.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
package com.martelleur.anvandawifip2p.model;

import androidx.lifecycle.LiveData;

import com.martelleur.anvandawifip2p.infrastructure.ChatClient;
import com.martelleur.anvandawifip2p.infrastructure.ChatServer;
import com.martelleur.anvandawifip2p.infrastructure.ChatSocket;
import com.martelleur.anvandawifip2p.model.cttp.CTTP;

import java.net.InetAddress;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A repository used to abstract the infrastructure layer.
 * The repository use a predefined port number for network
 * communication.
 */
public class MainRepository {
    private static MainRepository mainRepository;
    private ChatSocket chatSocket;
    private final Chat chat;
    private final ExecutorService executorService
            = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors());
    private final int PORT = 8988; // Static created port used.

    private MainRepository(Chat chat) {
        this.chat = chat;
    }

    /**
     * Use to initialize this repository.
     */
    public static void initialize(Chat chat) {
        if (mainRepository == null) {
            mainRepository = new MainRepository(chat);
        }
    }

    /**
     * Used to get a single instance of this repository.
     */
    public static MainRepository getInstance() {
        if (mainRepository == null) {
            throw new IllegalStateException("Main repo not initialized.");
        }
        return mainRepository;
    }

    /**
     * Used to start a chat socket server or client
     * dependent on if the device is group owner or not.
     */
    public void openChat(boolean isServer, InetAddress serverAddress) {
        if (chatSocket != null) return;
        if (isServer) openChatSocketServer(serverAddress);
        else openChatSocketClient(serverAddress);
    }

    /**
     * Used to start a chat server socket.
     */
    private void openChatSocketServer(InetAddress server) {
        chatSocket = new ChatServer(server, PORT, chat);
        executorService.execute((Runnable) chatSocket);
    }

    /**
     * Used to start a chat client socket.
     */
    private void openChatSocketClient(InetAddress server) {
        chatSocket = new ChatClient(server, PORT, chat);
        executorService.execute((Runnable) chatSocket);
    }

    /**
     * Used to get observable chat messages.
     */
    public LiveData<List<Message>> getMessagesLiveData() {
        return chat.getMessagesLiveData();
    }

    /**
     * Used to write message to chat.
     */
    public void writeMessage(String message) throws Chat.ChatException {
        if (chatSocket == null) return;
        String cttpMessage = chat.createNewMsg(message, CTTP.Method.MESSAGE);
        Runnable sendChatSocketMessage = () -> {
            chatSocket.writeMessage(cttpMessage);
            chat.onOutgoingMsg(message);
        };
        executorService.execute(sendChatSocketMessage);
    }

    /**
     * Used to write message to chat.
     */
    public boolean writeCloseMessage(String message) {
        if (chatSocket == null) return true;
        Callable<Boolean> sendChatSocketCloseMessage = () -> {
            String cttpCloseMessage = chat.createNewMsg(message, CTTP.Method.MESSAGE);
            chatSocket.writeMessage(cttpCloseMessage);
            return true;
        };
        try {
            return executorService.submit(sendChatSocketCloseMessage).get();
        } catch (ExecutionException | InterruptedException e) {
            return false;
        }
    }

    /**
     * Used to close chat socket.
     */
    public void closeChat() {
        if (chatSocket == null) return;
        Runnable closeChatSocket = () -> {
            chatSocket.closeSocket();
            chatSocket = null;
        };
        executorService.execute(closeChatSocket);
    }
}

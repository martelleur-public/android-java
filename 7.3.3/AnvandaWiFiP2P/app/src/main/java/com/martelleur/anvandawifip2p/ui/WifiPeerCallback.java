package com.martelleur.anvandawifip2p.ui;

import android.net.wifi.p2p.WifiP2pDevice;

/**
 * Interface describing callback for events related
 * to connecting to WiFi P2P device. The interface is
 * implemented in MainFragment and are used by
 * WifiPeerAdapter.
 */
public interface WifiPeerCallback {
    void onConnectToChannel(WifiP2pDevice peer);
    void onOpenChat();
    void onDisconnectFromChannel();
}

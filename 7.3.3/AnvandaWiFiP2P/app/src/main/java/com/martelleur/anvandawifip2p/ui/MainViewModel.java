package com.martelleur.anvandawifip2p.ui;

import android.net.wifi.p2p.WifiP2pDevice;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A view model class used to store the state of the
 * main fragment UI.
 */
public class MainViewModel extends ViewModel {
    private static MainViewModel mainViewModel = null;
    private final List<WifiP2pDevice> peers = new ArrayList<>();
    private boolean isWifiP2pGroupFormed = false;
    // private boolean chatPaused = false;
    private String chatTitle = "";

    /**
     * Used to get WiFi peer livedata.
     */
    public List<WifiP2pDevice> getPeers() {
        return peers;
    }

    /**
     * Used to update WiFi peer livedata.
     */
    public void updatePeers(Collection<WifiP2pDevice> discoverablePeers) {
        peers.clear();
        peers.addAll(discoverablePeers);
    }

    public boolean isWifiP2pGroupFormed() {
        return isWifiP2pGroupFormed;
    }

    public void setWifiP2pGroupFormed(boolean wifiP2pGroupFormed) {
        isWifiP2pGroupFormed = wifiP2pGroupFormed;
    }

    public String getChatTitle() {
        return chatTitle;
    }

    public void setChatTitle(String chatTitle) {
        this.chatTitle = chatTitle;
    }

    /*public boolean isChatPaused() {
        return chatPaused;
    }

    public void setChatPaused(boolean chatPaused) {
        this.chatPaused = chatPaused;
    }*/

    /**
     * Used to create an instance of this view model.
     * Ref: <a href="https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1">...</a>.
     */
    public static final ViewModelInitializer<MainViewModel> initializer = new ViewModelInitializer<>(
            MainViewModel.class,
            creationExtras -> {
                if (mainViewModel == null) {
                    mainViewModel = new MainViewModel();
                }
                return mainViewModel;
            }
    );
}

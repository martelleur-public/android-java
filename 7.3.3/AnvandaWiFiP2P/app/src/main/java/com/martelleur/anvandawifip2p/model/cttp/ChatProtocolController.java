package com.martelleur.anvandawifip2p.model.cttp;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;


/**
 * A controller class used to parse, build, and validate
 * custom protocol Chat Text Transport Protocol (CTTP)
 * incoming and outgoing messages using "org.xmlpull.v1".
 */
public class ChatProtocolController {

    private final ProtocolValidator protocolValidator = new ProtocolValidator();

    /**
     * Used to write an outgoing CTTP message.
     */
    public String writeOutgoingCTTP(String message, CTTP.Method cttpMethod) throws NotValidChatMessage {
        if (message.length() > CTTP.MAX_SIZE_BODY) {
            throw new NotValidChatMessage("Message can not be bigger then: " + CTTP.MAX_SIZE_BODY);
        }
        try {
            Element root = buildCTTP(message, cttpMethod);
            return root.toXml();
        } catch (IOException e) {
            throw new NotValidChatMessage(e.getMessage());
        }
    }

    /**
     * Used to build and validate a CTTP chat message.
     */
    private Element buildCTTP(String message, CTTP.Method cttpMethod) throws NotValidChatMessage {
        // Build elements
        Element scheme = new Element(CTTP.Tag.SCHEME.value)
                .setText(CTTP.SCHEME);
        Element version = new Element(CTTP.Tag.VERSION.value)
                .setText(CTTP.PATCH_VERSION);
        Element method = new Element(CTTP.Tag.METHOD.value)
                .setText(cttpMethod.value);
        Element protocol = new Element(CTTP.Tag.PROTOCOL.value)
                .addChild(scheme)
                .addChild(version)
                .addChild(method);
        Element header = new Element(CTTP.Tag.HEADER.value)
                .addChild(protocol);
        Element body = new Element(CTTP.Tag.BODY.value)
                .setText(message);
        Element root = new Element(CTTP.Tag.ROOT.value)
                .addChild(header)
                .addChild(body);

        // Validate correctness off elements.
        protocolValidator.validateRootElement(root);
        protocolValidator.validateHeaderElement(header);
        protocolValidator.validateProtocolElement(protocol);
        protocolValidator.validateSchemeElement(scheme);
        protocolValidator.validateVersionElement(version);
        protocolValidator.validateMethodElement(method);
        protocolValidator.validateBodyElement(body);

        // Return validated root element
        return root;
    }

    /**
     * Used to parse and validate an incoming CTTP message.
     * @param message - Incoming message to be parsed and validated.
     * @return - Text content of the body element if xml message is valid
     * else the method throws an NotValidChatMessage.
     * @throws NotValidChatMessage - Custom exception that indicate incorrect xml message.
     */
    public Response parseIncomingCTTP(String message) throws NotValidChatMessage {
        try {
            // Set required element as null;
            Element root = null;
            Element header = null;
            Element protocol = null;
            Element scheme = null;
            Element method = null;
            Element version = null;
            Element body = null;

            // Used to populate elements from the incoming message.
            String bodyText = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(message));
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();
                if (eventType == XmlPullParser.START_TAG) {
                    if (CTTP.Tag.ROOT.value.equals(tagName)) {
                        root = new Element(CTTP.Tag.ROOT.value);
                    } else if (CTTP.Tag.HEADER.value.equals(tagName)) {
                        header = new Element(CTTP.Tag.HEADER.value);
                    } else if (CTTP.Tag.PROTOCOL.value.equals(tagName)) {
                        protocol = new Element(CTTP.Tag.PROTOCOL.value);
                    } else if (CTTP.Tag.SCHEME.value.equals(tagName)) {
                        String schemeText = parser.nextText();
                        scheme = new Element(CTTP.Tag.SCHEME.value).setText(schemeText);
                    } else if (CTTP.Tag.VERSION.value.equals(tagName)) {
                        String versionText = parser.nextText();
                        version = new Element(CTTP.Tag.VERSION.value).setText(versionText);
                    } else if (CTTP.Tag.METHOD.value.equals(tagName)) {
                        String methodText = parser.nextText();
                        method = new Element(CTTP.Tag.METHOD.value).setText(methodText);
                    } else if (CTTP.Tag.BODY.value.equals(tagName)) {
                        bodyText = parser.nextText();
                        body = new Element(CTTP.Tag.BODY.value).setText(bodyText);
                    }
                }
                eventType = parser.next();
            }

            // Validate that all required elements have been set.
            protocolValidator.validateRequiredElements(new Element[]{
                    root, header, protocol, scheme, method, version, body});

            // Build Protocol of incoming elements.
            if (root != null) root.addChild(header).addChild(body);
            if (protocol != null) protocol.addChild(scheme)
                    .addChild(version)
                    .addChild(method);
            if (header != null) header.addChild(protocol);

            // Validate correctness off elements.
            protocolValidator.validateRootElement(root);
            protocolValidator.validateHeaderElement(header);
            protocolValidator.validateProtocolElement(protocol);
            protocolValidator.validateSchemeElement(scheme);
            protocolValidator.validateVersionElement(version);
            protocolValidator.validateMethodElement(method);
            protocolValidator.validateBodyElement(body);

            if (method != null) {
                return new Response(bodyText, method.getText());
            } else {
                throw new NotValidChatMessage("Invalid method used.");
            }
        } catch (XmlPullParserException | IOException e) {
            throw new NotValidChatMessage(e.getMessage());
        }
    }

    public static class Response {
        public final String method;
        public final String body;
        Response(String body, String method) {
            this.body = body;
            this.method = method;
        }
    }

    /**
     * Custom chat exception used to indicate error
     * when parsing and validating xml.
     */
    public static class NotValidChatMessage extends Exception {
        NotValidChatMessage(String message) {
            super(message);
        }
    }
}

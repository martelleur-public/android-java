package com.martelleur.anvandavibrationer;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.os.CombinedVibration;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.os.VibratorManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.anvandavibrationer.databinding.FragmentVibrateBinding;

/**
 * A fragment used to demonstrate device vibration.
 */
public class VibrateFragment extends Fragment {

    private FragmentVibrateBinding binding;

    public VibrateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
    **/
    public static VibrateFragment newInstance() {
        return new VibrateFragment();
    }

    /**
     * Used to bind layout using view binding.
     **/
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentVibrateBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created setup a vibrate click listener.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupVibrateListener();
    }

    /**
     * Used to setup a vibration listener dependent on api levels,
     * due new solutions and deprecation.
     */
    private void setupVibrateListener() {
        binding.vibrate.setOnClickListener(view -> {
            int durationMedium = 1000; // milli seconds.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                setVibrateFromApiLevel31(durationMedium);
            } else {
                setupVibrateBeforeApiLevel31(durationMedium);
            }
        });
    }

    /**
     * Used to setup a vibration listener from api 31 and forward.
     */
    @RequiresApi(api = Build.VERSION_CODES.S)
    private void setVibrateFromApiLevel31(int duration) {
        VibratorManager vibratorManager = (VibratorManager) requireActivity()
                .getSystemService(Context.VIBRATOR_MANAGER_SERVICE);

        VibrationEffect vibrationEffect = getVibrationsEffect(duration);
        CombinedVibration combinedVibration = getCombinedVibration(vibrationEffect);
        vibratorManager.vibrate(combinedVibration);
    }

    /**
     * Used to get vibrations effect lasting to be used
     * for a device vibrator. Min api level 26.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private VibrationEffect getVibrationsEffect(int duration) {
        return VibrationEffect.createOneShot(
                duration, VibrationEffect.DEFAULT_AMPLITUDE);

    }

    /**
     * Used to get a combined vibration for all device vibrators
     * running in parallel with a single vibration effect. Min api level 31.
     */
    @RequiresApi(api = Build.VERSION_CODES.S)
    private CombinedVibration getCombinedVibration(VibrationEffect vibrationEffect) {
        return CombinedVibration.createParallel(vibrationEffect);
    }

    /**
     * Used to setup a vibration listener before api level 31.
     */
    private void setupVibrateBeforeApiLevel31(int duration) {
        Vibrator vibrator = (Vibrator) requireActivity()
                .getSystemService(Context.VIBRATOR_SERVICE);

        if (vibrator.hasVibrator()) {
            return;
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setVibrateFromApiLevel26(vibrator, duration);
        } else {
            setVibrateBeforeApiLevel26(vibrator, duration);
        }
    }

    /**
     * Used to setup a vibration listener from api level 26.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setVibrateFromApiLevel26(Vibrator vibrator, int duration) {
        VibrationEffect vibrationEffect = getVibrationsEffect(duration);
        vibrator.vibrate(vibrationEffect);
    }

    /**
     * Used to setup a vibration listener before api level 26.
     */
    private void setVibrateBeforeApiLevel26(Vibrator vibrator, int duration) {
        vibrator.vibrate(duration);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

package com.martelleur.anvandavibrationer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

/**
 * A launch activity for the application used to launch the main fragment.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * When activity is created launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startMainFragment();
    }

    /**
     * Used to start main fragment.
     */
    private void startMainFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, VibrateFragment.newInstance())
                .commitNow();
    }
}
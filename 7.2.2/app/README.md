# Använda vibrationer

Prototypen introducerar till hur man använder vibrationer.

## Prototyp

Gör en app som använder vibrationer.

## Funktionalitet

Har man ingen Android-mobil att testa på så kan man lägga till en Toast i metoden som genererar vibrationen.

## Beskrivning implementation

Applikationen implementerar en fragment ```VibrateFragment``` som
sätter upp en lyssnare som en användare kan välja för att mobilen
ska vibrera.
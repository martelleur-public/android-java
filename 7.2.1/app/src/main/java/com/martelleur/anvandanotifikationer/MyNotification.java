package com.martelleur.anvandanotifikationer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.service.notification.StatusBarNotification;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


/**
 * A class used to construct and send an example notification.
 */
public class MyNotification {
    private final String channelID;
    private final int notificationID;
    private final Context context;

    /**
     * Used to construct the notification on a
     * defined channel and ID.
     */
    MyNotification(String channelID, int notificationID, Context context) {
        this.channelID = channelID;
        this.notificationID = notificationID;
        this.context = context;
    }

    /**
     * Used to send the notification. Post notifications permission
     * is checked from api level 33 and forward. If post notifications
     * is denied a custom exception will be throed indicating that the
     * notification have failed.
     * @throws SendNotificationException
     */
    public void send() throws SendNotificationException {
        setupNotificationChannel();
        PendingIntent mainActivity = getNotificationIntent();
        NotificationCompat.Builder builder = getNotificationBuilder(mainActivity);
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);

        // From api level 33 TIRAMISU post notifications must be allowed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            boolean isPostNotificationsAllowed = ActivityCompat.checkSelfPermission(
                    context, android.Manifest.permission.POST_NOTIFICATIONS) ==
                    PackageManager.PERMISSION_GRANTED;
            if (!isPostNotificationsAllowed) {
                String msg = "Notification fail due POST_NOTIFICATIONS is not allowed.";
                throw new SendNotificationException(msg);
            }
            manager.notify(notificationID, builder.build());

        // Before api level 33 TIRAMISU.
        } else {
            manager.notify(notificationID, builder.build());
        }
    }

    /**
     * A custom exception used to indicate that a notification have failed.
     */
    public static class SendNotificationException extends Exception {
        public SendNotificationException(String errorMessage) {
            super(errorMessage);
        }
    }

    /**
     * Used to set up a notification channel for API
     * level > 26 (=android.os.Build.VERSION_CODES.O).
     * Before API 26 channel was not required.
     */
    private void setupNotificationChannel() {
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        NotificationChannel channel;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(
                    channelID,
                    context.getText(R.string.notification_channel_name),
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel.enableVibration(true);
            manager.createNotificationChannel(channel);
        }
    }

    /**
     * Create a PendingIntent that will be used to start
     * main activity when notification is clicked.
     */
    private PendingIntent getNotificationIntent() {
        Intent mainActivityIntent = new Intent(context, MainActivity.class);
        return PendingIntent.getActivity(
                context,
                0,
                mainActivityIntent,
                PendingIntent.FLAG_IMMUTABLE
        );
    }

    /**
     * Construct a NotificationCompat.Builder used to build the
     * notification with a specific pending intent.
     **/
    private NotificationCompat.Builder getNotificationBuilder(PendingIntent intent) {
        String title = context.getString(R.string.notification_title);
        String name = context.getString(R.string.this_is_a_periodic_notification);
        context.getString(R.string.this_is_a_periodic_notification);
        return new NotificationCompat.Builder(
                context, channelID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(title)
                .setContentText(name)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(intent) // Intent will be activated when user click on the notification.
                .setAutoCancel(true); // Remove notification when user click on it.
    }

    /**
     * Used to remove all notifications when the app is open.
     */
    public static void removeAllNotifications(Context context) {
        NotificationManagerCompat manager = NotificationManagerCompat.from(context);
        manager.cancelAll();
    }

    /**
     * Used to remove all notifications when the app is open.
     */
    public static int getNumberOfHighPriorityNotifications(Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        StatusBarNotification[] notifications = notificationManager.getActiveNotifications();
        int highPriorityNotifications = 0;

        for (StatusBarNotification notification : notifications) {
            boolean isHighPriority = notification.getNotification().priority == NotificationCompat.PRIORITY_MAX;
            if (isHighPriority) highPriorityNotifications++;
        }
        return highPriorityNotifications;
    }
}

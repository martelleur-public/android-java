package com.martelleur.anvandanotifikationer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

/**
 * Main activity used to set necessary run time
 * permissions and launch the main fragment.
 */
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSIONS = 1; // Used to control permission mgmt.

    private static final String NOTIFICATION_WORKER = "com.martelleur.anvandanotifikationer.NOTIFICATION_WORKER";

    /**
     * When activity is created launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean permissionsGranted = checkPermissions();
        if (permissionsGranted) {
            setupNotificationWorker();
            startMainFragment();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    getPermissions(),
                    REQUEST_PERMISSIONS);
        }
    }

    /**
     * Used to check if all necessary permissions is allowed.
     */
    private boolean checkPermissions() {
        boolean permissionsGranted = true;
        for (String permission : getPermissions()) {
            if (ContextCompat.checkSelfPermission(this, permission) !=
                    PackageManager.PERMISSION_GRANTED) {
                permissionsGranted = false;
                break;
            }
        }
        return permissionsGranted;
    }

    /**
     * Used to get permission dependent on api level.
     */
    private String[] getPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return new String[] {Manifest.permission.POST_NOTIFICATIONS};
        } else {
            return new String[] {};
        }
    }

    /**
     * Used to setup a worker that post example notifications.
     */
    private void setupNotificationWorker() {
        PeriodicWorkRequest notificationWorker = new PeriodicWorkRequest
                .Builder(NotificationWorker.class, 1, TimeUnit.MINUTES)
                .build();

        WorkManager.getInstance(this).enqueueUniquePeriodicWork(
                NOTIFICATION_WORKER,
                ExistingPeriodicWorkPolicy.KEEP,
                notificationWorker);
    }

    /**
     * Used to start main fragment.
     */
    private void startMainFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, MainFragment.newInstance())
                .commitNow();
    }

    /**
     * Used to request all necessary permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean permissionsGranted = true;

        if (requestCode == REQUEST_PERMISSIONS) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                    break;
                }
            }
        }

        if (!permissionsGranted) {
            showWarningDialog();
        } else {
            startMainFragment();
            setupNotificationWorker();
        }
    }

    /**
     * Used to show permission warning dialog if not all
     * necessary permission is allowed.
     */
    private void showWarningDialog() {
        PermissionWarningDialogFragment warningFragment =
                PermissionWarningDialogFragment.newInstance();
        warningFragment.show(getSupportFragmentManager(), "Warning Dialog");
    }
}
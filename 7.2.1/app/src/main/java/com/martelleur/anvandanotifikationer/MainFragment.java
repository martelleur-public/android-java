package com.martelleur.anvandanotifikationer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.martelleur.anvandanotifikationer.databinding.FragmentMainBinding;


/**
 * A fragment used to show dialogs and toasts. The
 * fragment also listen on application notification that
 * are posted periodically by a worker using a dynamic
 * registered receiver.
 */
public class MainFragment extends Fragment {

    FragmentMainBinding binding;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     **/
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    /**
     * Used to bind layout using view binding.
     **/
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMainBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created setup
     * 1) Listener for open a Dialog.
     * 2) Listener for starting a toast.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayout();
        setupClearNotificationsListener();
        setupOpenDialogListener();
        setupStartToastListener();
    }

    /**
     * Used to update layout to show number of active notifications in
     * the context of the application.
     */
    private void updateLayout() {
        int nrOfNotifications = MyNotification.getNumberOfHighPriorityNotifications(requireActivity());
        if (nrOfNotifications > 0) {
            String clearNotificationLabel = getString(R.string.clear_notifications, nrOfNotifications);
            binding.clearNotifications.setVisibility(View.VISIBLE);
            binding.clearNotifications.setText(clearNotificationLabel);
        } else {
            binding.clearNotifications.setVisibility(View.GONE);
        }
    }

    /**
     * Used to setup listener for clearing all notifications in
     * the context of the application.
     */
    private void setupClearNotificationsListener() {
        binding.clearNotifications.setOnClickListener(view -> {
            MyNotification.removeAllNotifications(requireActivity());
            updateLayout();
        });
    }

    /**
     * Used to setup listener for displaying a example dialog.
     */
    private void setupOpenDialogListener() {
        binding.showDialog.setOnClickListener(view -> {
            MyDialogFragment myDialog =
                    MyDialogFragment.newInstance();
            myDialog.show(getChildFragmentManager(), "A Dialog");
        });
    }

    /**
     * Used to setup listener for displaying a example toast.
     */
    private void setupStartToastListener() {
        binding.showToast.setOnClickListener(view -> {
            int msg = R.string.this_is_a_toast;
            Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
        });
    }

    /**
     * Used to register broadcast receiver for notification.
     */
    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(
                NotificationWorker.ACTION_NOTIFICATION_POSTED);
        requireContext().registerReceiver(notificationReceiver, filter);
    }

    /**
     * A broadcast receiver used to listen and handle
     * broadcast from notification worker.
     */
    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(
                    NotificationWorker.KEY_NEW_NOTIFICATION);
            Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
            updateLayout();
        }
    };

    /**
     * Used to unregister broadcast receiver for notification.
     */
    @Override
    public void onPause() {
        super.onPause();
        requireContext().unregisterReceiver(notificationReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

package com.martelleur.anvandanotifikationer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;

/**
 * An dialog fragment used to display a example dialog.
 */
public class MyDialogFragment extends androidx.fragment.app.DialogFragment {

    public static MyDialogFragment newInstance() {
        return new MyDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        int title = R.string.dialog_title;
        int msg = R.string.this_is_a_dialog;
        int positiveButtonText = R.string.dialog_ok;

        builder.setTitle(title)
                .setMessage(msg)
                .setPositiveButton(positiveButtonText, (dialog, id) -> {
                    // User clicked the "OK" button
                });
        return builder.create();
    }
}

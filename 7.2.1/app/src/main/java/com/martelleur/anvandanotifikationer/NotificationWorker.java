package com.martelleur.anvandanotifikationer;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Random;

/**
 * A worker used to send periodic notifications.
 */
public class NotificationWorker extends Worker {
    private static final String NOTIFICATION_CHANNEL_ID = "com.martelleur.anvandanotifikationer.NOTIFICATION_CHANNEL";
    static final String KEY_NEW_NOTIFICATION = "com.martelleur.anvandanotifikationer.NEW_NOTIFICATION";
    static final String ACTION_NOTIFICATION_POSTED = "com.martelleur.anvandanotifikationer.NOTIFICATION_POSTED";

    /**
     * Default constructor for worker.
     */
    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    /**
     * Work method used to create an example notification.
     */
    @NonNull
    @Override
    public Result doWork() {
        // Create an example notification on a chanel whit a random ID.
        Random random = new Random();
        int notificationId = random.nextInt(1000);
        MyNotification myNotification = new MyNotification(
                NOTIFICATION_CHANNEL_ID, notificationId, getApplicationContext());

        // Handle successful and failed work.
        try {
            myNotification.send();
            publishNewNotification();
            return Result.success();
        } catch (MyNotification.SendNotificationException e) {
            return Result.failure();
        }
    }

    /**
     * Used to publish that a new notification have been
     * posted by sending a broadcast.
     */
    private void publishNewNotification() {
        Intent intent = new Intent(ACTION_NOTIFICATION_POSTED);
        String msg = getApplicationContext()
                .getString(R.string.new_notification_received);
        intent.putExtra(KEY_NEW_NOTIFICATION, msg);
        getApplicationContext().sendBroadcast(intent);
    }
}

# Använda notifikationer

Prototypen introducerar till hur man använder notifikationer, dialogfönster och små popupper (toasts).

## Prototyp

Gör en app som använder både notifikationer, dialogfönster och små popupper (toasts).

## Beskrivning implementation

Applikationen implemneterar:

1. __Två dialoger:__
    - ```MayDialogFragment``` som används som en hello world dialog
    - ```PermissionWarningDialogFragment``` som används om en användare inte tillåtit appen dem tillstånd som appen behöver för att fungera optimalt.

2. __En hello world toast__

3. __Periodiska notificationer:__
    - En Worker klass ```NotificationWorker``` som publiserar notifikationer periodiskt.
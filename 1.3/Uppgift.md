# 1.3 Paketera, signera och sälja

Uppgiften introducerar till hur man paketerar, signerar och säljer sitt program i Google Play».

## Uppgift
Skapa en apk-fil från uppgiften 1.2 Koppla till händelsehantering och och signera den med ett självsignerat certifikat. Redovisa varje steg detaljerat i en textfil.


## Tips

Apk-filer är en variant på jar-filer som är en samling sammanlagda filer som har komprimerats. Ofta skapar utvecklingsverktyget apk-filer automatiskt.

Testa gärna programmet på en riktig mobil. Ett lätt sätt att föra över den signerade apk-filen är att eposta den på datorn till sig själv och sen läsa ebrevet på mobilen och installera den direkt från epostprogrammet.

Se:

- __[Google Developers: Publish your app](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://developer.android.com/studio/publish/index.html&session=879819)__

- __[Google Developers: Sign your app](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://developer.android.com/studio/publish/app-signing.html&session=879819)__

Man kan lägga ut appar till försäljning på __[Google Play](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://play.google.com/&session=879819)__ med några få enkla steg men man måste som utvecklare betala en engångssumma på $25. Man kan även använda reklam via AdMob i sin app för att få betalt för de klick som användarna gör på reklamen som visas i appen.

Numera krävs att man använder _Android App Bundles_, se vidare __[Google Developers: About Android App Bundles](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://developer.android.com/guide/app-bundle&session=879819)__.
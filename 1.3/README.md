# Paketera och signera en android app lokalt

Källan [https://developer.android.com/studio/publish/preparing#publishing-intro](https://developer.android.com/studio/publish/preparing#publishing-intro)
rekommenderar 5 steg för att förbereda en app för release:

1. __Förbered ```End User License Agreement (EULA)```, skapa kryptografiska nycklar, samt skapa en ikon för appen__

2. __Skapa release specifika konfigurationer__

3. __Bygga och signera en releasen__

4. __Förebered tjänster som appen använder__

5. __Testa den signerade releasen__

<br/>

## Steg 1: EULA, kryptografiska nycklar, samt ikon
***

- __EULA:__ I detta fall så behös ingen ```End User License Agreement (EULA)``` eftersom applikationerna endast ska testas internt.

- __Skapa ikon:__ För att skapa en ikon så kan web tjänten [icon.kitchen](icon.kitchen) användas dör att snabbt skapa ikoner för bland annat Android applikationer. 

- __Skapa kryptografiska nycklar:__ För att skapa nycklar för att signera applikationen följ [https://developer.android.com/studio/publish/app-signing#generate-key](https://developer.android.com/studio/publish/app-signing#generate-key). Mer specifikt så kan följande steg genomföras:

    1. I _Android Studio_ menyn ```Build -> Generate signed bundle or APK``` så valdes alternativet för att bygga och signera en APK som kan installeras på en Android mobil.
    2. Valde att skapa en ny ```Key store path```
    3. En ```Key store path``` sattes.
    3. En ```key store password``` sattes.
    4. En ```key store alias``` sattes.
    5. En ```key password``` sattes.
    6. Giltighet 1 år valdes.
    7. Certifikat med förnamn och erfternamn _Joel Martelleur_ sattes.

<br/>

## Steg 2: Skapa release specifika konfigurationer
***

I ```build.gradles``` konfigurera ```debuggable``` som ```false```
för att bygga en ```release```.

```bash
...
buildTypes {
        release {
            debuggable false
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
...
```

- I filen ```build.gradles``` markera också minimum system API (minSdk) och target system API (targetSdk) som appen är designad för. Samt markera en intern version ```versionCode``` samt en extern version ```versionName```.   

```bash
...
defaultConfig {
        applicationId "com.example.laggautkomponentersamtkopplatillhandelsehantering"
        minSdk 24
        targetSdk 33
        versionCode 1
        versionName "hello_world_1"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
...
```

<br/>

## Steg 3: Bygga och signera releasen 
***

- __Signera releasen:__ För att signera releasen följ [https://developer.android.com/studio/publish/app-signing#sign_release](https://developer.android.com/studio/publish/app-signing#sign_release). Mer specifikt så kan följande steg genomföras:

    1. I _Android Studio_ menyn ```Build -> Generate signed bundle or APK``` välj alternativet för att bygga och signera en APK som kan installeras på en Android mobil.
    2. Välj den tidigare skapta ```key store``` i [Steg 1](#steg-1-eula-kryptografiska-nycklar-samt-ikon).    
    3. Välj katalog för den signerade releasen.
    4. Välj release typ ```release``` som bygg typp.

<br/>

## Steg 4: Förebered tjänster som appen använder
***

Om appen inte är beroende av några externa tjänster hoppa över detta steg.

<br/>

## Steg 5: Testa den signerade releasen
***

Testa sedan den signerade applikationen på en emulator genom att droppa den byggda APK filen från arbetsstationen till en emulator där den sedan kan testas manuellt.
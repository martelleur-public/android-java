package com.martelleur.anvndadatabaskopplingar.infrastructure;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * @DEPRECATED
 * @TODO Issue can not connect directly to mysql db from android app.
 * Used to connect directly to mysql database service for acquiring and creating
 * guestbooks.
 */
public class GuestBookMysql implements GuestBookStore {
    private final String TAG = "GuestBookMysql";
    private static final String DB_PROPERTY_FILE = "db.properties";
    private final Connection connection;
    private final String guestbookTable;
    private final Context context;

    public GuestBookMysql(Context context) {
        Log.d(TAG, "GuestBookMysql(Context context)");
        this.context = context;
        Properties properties = loadDBProperties();
        String url = properties.getProperty("db.url");
        String user = properties.getProperty("db.user");
        String pw = properties.getProperty("db.password");
        guestbookTable = properties.getProperty("db.guestbook_table");
        connection = getConnection(url, user, pw);
        if (connection == null) {
            Log.d(TAG, "Could not connect to mysql server.");
        }
    }

    /**
     * Used to load db properties from db property file.
     */
    private Properties loadDBProperties() {
        Properties properties;
        try {
            properties = new Properties();
            AssetManager assetManager =
                    Objects.requireNonNull(context).getAssets();
            InputStream inputStream = assetManager.open(DB_PROPERTY_FILE);
            properties.load(inputStream);
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return properties;
    }

    /**
     * Used to get the connection to the database.
     */
    private Connection getConnection(String url, String user, String pw) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, pw);
        } catch (SQLException e) {
            Log.d(TAG, Arrays.toString(e.getStackTrace()));
            Log.d(TAG, e.getMessage());
        }
        return connection;
    }

    /**
     * Used to insert a guest book to the database.
     */
    @Override
    public GuestBook addGuestBook(GuestBook guestBook) {
        String query = getQueryInsertGuestBook();
        insertGuestBookToDb(query, guestBook);
        return guestBook;
    }

    /**
     * Used to get the query string for inserting
     * a guest book to the database.
     */
    private String getQueryInsertGuestBook() {
        return "INSERT INTO "  +
                guestbookTable +
                " (name, mail, webpage, comment) VALUES (?, ?, ?, ?)";
    }

    /**
     * Used to insert a guest book to the database with
     * prepared statements.
     */
    private void insertGuestBookToDb(String query, GuestBook guestBook) {
        PreparedStatement pStmt = null;
        try {
            pStmt = connection.prepareStatement(query);
            pStmt.setString(1, guestBook.getName());
            pStmt.setString(2,guestBook.getMail());
            pStmt.setString(3,guestBook.getWebpage());
            pStmt.setString(4, guestBook.getComment());

            // Execute the prepared statement.
            pStmt.executeUpdate();
            closePreparedStatement(pStmt);

        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        } finally {
            closePreparedStatement(pStmt);
        }
    }

    /**
     * Used to close prepared statement.
     */
    private void closePreparedStatement(PreparedStatement pStmt) {
        // Close the prepared statement
        try {
            if (pStmt != null) {
                pStmt.close();
            }
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * Used to get all the guestbooks.
     */
    @Override
    public List<GuestBook> getAllGuestBooks() {
        ResultSet result = getAllGuestBooksItemsFromDB();
        return convertResultsSetToListOfGuestBook(result);
    }


    /**
     * Used to get the result set from querying all guestbooks.
     */
    private ResultSet getAllGuestBooksItemsFromDB() {
        ResultSet result = null;
        try {
            String queryAllGuestBooks = "SELECT * FROM " + guestbookTable;
            Statement stmt = connection.createStatement();
            result = stmt.executeQuery(queryAllGuestBooks);
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
        return result;
    }

    /**
     * Used to convert result set to guestbook instances.
     */
    private List<GuestBook> convertResultsSetToListOfGuestBook(ResultSet result) {
        List<GuestBook> guestBooks = new ArrayList<>();
        try {
            while (result.next()) {
                GuestBook guestBook = new GuestBook(
                        result.getString("name"),
                        result.getString("mail"),
                        result.getString("webpage"),
                        result.getString("comment"));
                guestBooks.add(guestBook);
            }
        } catch (SQLException |
                 GuestBook.FieldContainHTMLException |
                 GuestBook.EmptyGuestBookFieldException |
                 GuestBook.SizeNotAllowedException e) {
            Log.d(TAG, e.getMessage());
        }
        return guestBooks;
    }

    /**
     * Used to close the DB connection.
     */
    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            Log.d(TAG, e.getMessage());
        }
    }
}

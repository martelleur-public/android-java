# Använda en SQLite databas

Prototypen introducerar till databaser med SQLite».

## Prototyp

Gör en app som försöker hitta större och större primtal. När ett nytt primtal är hittat så ska följande lagras i en SQLite databas:

- Primtalet
- Datum och tid då primtalet hittades

Samtliga funna primtal ska lagras. Användaren ska ha möjlighet att se alla funna primtal och den tid då de hittades. När appen startas så ska den fortsätta att leta där den var när appen stängdes.

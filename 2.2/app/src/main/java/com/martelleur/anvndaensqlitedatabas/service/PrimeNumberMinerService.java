package com.martelleur.anvndaensqlitedatabas.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;

import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberExecutor;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberMiner;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberRepository;

/**
 * A bound service that is used for start and stop mining of
 * prime numbers. The service is running separated process
 * defined in the manifest file.
 */
public class PrimeNumberMinerService extends Service {
    final Messenger messenger = new Messenger(new IncomingHandler(Looper.getMainLooper()));
    public static final String msgKey = "PrimeNumberMinerService";
    public static final String STOP = "STOP";
    public static final String START = "START";
    static private final PrimeNumberRepository repository = PrimeNumberRepository.getInstance();
    static private final PrimeNumberExecutor primeNumberExecutor = new PrimeNumberExecutor();
    static private final PrimeNumberMiner miner = new PrimeNumberMiner(repository);

    public PrimeNumberMinerService() {}

    static class IncomingHandler extends Handler {

        public IncomingHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            String dataString = data.getString(msgKey);
            if (dataString.equals(STOP)) {
                stopMinePrimeNumbers();
            } else if (dataString.equals(START)) {
                startMinePrimeNumbers();
            }
        }
    }

    static private void startMinePrimeNumbers() {
        primeNumberExecutor.execute(miner);
    }

    static public void stopMinePrimeNumbers() {
        primeNumberExecutor.stopExecute();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopMinePrimeNumbers();
        return super.onUnbind(intent);
    }
}

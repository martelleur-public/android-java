package com.martelleur.anvndaensqlitedatabas;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import androidx.appcompat.app.AppCompatActivity;

import com.martelleur.anvndaensqlitedatabas.databinding.ActivityMainBinding;
import com.martelleur.anvndaensqlitedatabas.service.PrimeNumberMinerService;

/**
 * Main activity of this application used
 * to display two fragments on for searching for mined
 * prime numbers and the other displaying mined prime numbers.
 * The activity is also using a bound service that are mining
 * prime numbers in a separated process.
 */
public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    Messenger primeNumberService = null;
    boolean isBound;

    /**
     * A service connection used to connect a service
     * for mining prime numbers.
     */
    final private ServiceConnection primeNumber = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            primeNumberService = new Messenger(service);
            isBound = true;
            sendMessageToService(PrimeNumberMinerService.START);
        }
        public void onServiceDisconnected(ComponentName className) {
            primeNumberService = null;
            isBound = false;
        }
    };

    /**
     * Used for sending messages to prime number service
     * running on a separated process.
     */
    private void sendMessageToService(String msgType) {
        if (!isBound) return;
        Message msg = Message.obtain();
        Bundle bundle = new Bundle();
        bundle.putString(PrimeNumberMinerService.msgKey, msgType);
        msg.setData(bundle);
        try {
            primeNumberService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to bound layout using view binding.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    /**
     * Used to start a bound service running on a separated process
     * for mining prime numbers
     */
    @Override
    protected void onStart() {
        super.onStart();
        Intent primeNumberServiceIntent = new Intent(getApplicationContext(),
                PrimeNumberMinerService.class);
        bindService(primeNumberServiceIntent, primeNumber, Context.BIND_AUTO_CREATE);
    }

    /**
     * Used to stop mining prime numbers and
     * unbind prime number service.
     */
    @Override
    public void onStop() {
        super.onStop();
        sendMessageToService(PrimeNumberMinerService.STOP);
        unbindService(primeNumber);
    }

    /**
     * Used to unbind to the service.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        isBound = false;
    }
}
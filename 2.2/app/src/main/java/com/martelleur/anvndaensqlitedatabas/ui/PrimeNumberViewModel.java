package com.martelleur.anvndaensqlitedatabas.ui;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvndaensqlitedatabas.model.LoadPrimeNumberCallback;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberRepository;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

import java.util.ArrayList;
import java.util.List;

/**
 * A view model extending androidx.lifecycle.ViewModel
 * that holds livedata for prime numbers.
 */
public class PrimeNumberViewModel extends ViewModel {
    private final PrimeNumberRepository repository;
    private final MutableLiveData<List<PrimeNumber>> primeNumbersLiveData =
            new MutableLiveData<>(new ArrayList<>());

    private final MutableLiveData<Boolean> isLoadingPrimeNumbers =
            new MutableLiveData<>(false);

    public PrimeNumberViewModel(PrimeNumberRepository repository) {
        this.repository = repository;
    }

    // LoadPrimeNumberCallback is used as a callback in the
    // repository to manage loading a large number of prime numbers
    // from persistent storage in a more user friendly manner.
    private final LoadPrimeNumberCallback loadPrimeNumberCallback = new LoadPrimeNumberCallback()  {
        @Override
        public void onStart(List<PrimeNumber> primeNumbers) {
            isLoadingPrimeNumbers.postValue(true);
            primeNumbersLiveData.postValue(new ArrayList<>());
        }

        @Override
        public void onComplete(List<PrimeNumber> primeNumbers) {
            isLoadingPrimeNumbers.postValue(false);
            primeNumbersLiveData.postValue(primeNumbers);
        }

        @Override
        public void onError(List<PrimeNumber> primeNumbers) {
            isLoadingPrimeNumbers.postValue(false);
            primeNumbersLiveData.postValue(primeNumbers);
        }
    };

    public void loadPrimeNumbers(boolean reverseOrder) {
        repository.loadPrimeNumbers(reverseOrder, loadPrimeNumberCallback);
    }

    public void loadPrimeNumbersInInterval(long minValue, long maxValue, boolean reverseOrder) {
        repository.loadPrimeNumbersInInterval(minValue,
                maxValue, reverseOrder, loadPrimeNumberCallback);
    }

    public void loadPrimeNumbersAfter(long minValue, boolean reverseOrder) {
        repository.loadPrimeNumbersAfter(minValue,
                reverseOrder, loadPrimeNumberCallback);
    }

    public void loadPrimeNumbersBefore(long maxValue, boolean reverseOrder) {
        repository.loadPrimeNumbersBefore(maxValue,
                reverseOrder, loadPrimeNumberCallback);
    }

    public MutableLiveData<List<PrimeNumber>> getPrimeNumbersLiveData() {
         return primeNumbersLiveData;
    }

    public void setPrimeNumbersLiveData(List<PrimeNumber> primeNumbers) {
        primeNumbersLiveData.setValue(primeNumbers);
    }

    public MutableLiveData<Boolean> isLoadingPrimeNumbers() {
        return isLoadingPrimeNumbers;
    }
}

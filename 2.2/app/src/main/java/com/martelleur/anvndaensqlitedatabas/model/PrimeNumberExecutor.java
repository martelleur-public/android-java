package com.martelleur.anvndaensqlitedatabas.model;

import java.util.concurrent.Executor;

/**
 * A Executor class used for start and stop mining in a separated thread.
 */
public class PrimeNumberExecutor implements Executor {
    private PrimeNumberMiner miner;

    /**
     * Used to start mining prime numbers in a separated thread.
     */
    @Override
    public void execute(Runnable primeNumberMiner) {
        if (!(primeNumberMiner instanceof PrimeNumberMiner)) {
            throw  new IllegalArgumentException("Runner should be an instance of PrimeNumberRunner.");
        }
        this.miner = (PrimeNumberMiner) primeNumberMiner;
        new Thread(this.miner).start();
    }

    /**
     * Used to stop mining prime numbers.
     */
    public void stopExecute() {
        miner.stop();
    }
}



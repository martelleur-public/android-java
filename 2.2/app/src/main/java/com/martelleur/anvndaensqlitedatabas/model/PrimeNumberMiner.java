package com.martelleur.anvndaensqlitedatabas.model;

import android.util.Log;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A Runnable class used for mining prime numbers.
 */
public class PrimeNumberMiner implements Runnable {
    private final String TAG = "PrimeNumberMiner";
    private long currentNumberToCheck; // Used to check if the current number is a prime number.
    private final AtomicBoolean isMining = new AtomicBoolean(false);
    private final PrimeNumberRepository repository;

    public PrimeNumberMiner (PrimeNumberRepository repository) {
        this.repository = repository;
        this.currentNumberToCheck = repository.getStartValueForMining();
    }

    /**
     * Used to start mining prime numbers.
     */
    @Override
    public void run() {
        startMining();
    }

    /**
     * Method used for mining/obtaining prime numbers by
     * testing if an number is a prime number.
     */
    private void startMining() {
        isMining.set(true);
        while (isMining.get()) {
            Log.d(TAG, "Checking value: " + currentNumberToCheck);
            try { // Only valid prime number can be created.
                Date date = new Date();
                PrimeNumber primeNumber = new PrimeNumber(currentNumberToCheck, date);
                repository.storePrimeNumber(primeNumber);
            } catch (NotAPrimeNumberException ignored) {}
            repository.storeLastCheckedNumber(currentNumberToCheck);
            currentNumberToCheck++;
        }
    }

    /**
     * Used to stop mining prime numbers.
     */
    public void stop() {
        isMining.set(false);
    }
}

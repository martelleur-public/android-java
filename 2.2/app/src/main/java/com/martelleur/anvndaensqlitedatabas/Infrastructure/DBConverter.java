package com.martelleur.anvndaensqlitedatabas.Infrastructure;

import androidx.room.TypeConverter;

import java.util.Date;

/**
 * A class used for defining how java types and sqlite types
 * should be mapped.
 */
public class DBConverter {
    @TypeConverter
    public static Long fromDate(Date date) {
        return date == null ? null : date.getTime();
    }

    @TypeConverter
    public static Date toDate(Long dateLong) {
        return dateLong == null ? null : new Date(dateLong);
    }
}

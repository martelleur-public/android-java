package com.martelleur.anvndaensqlitedatabas.model;

/**
 * Exception thrown if creating prime number instance that is not a prime number.
 */
public class NotAPrimeNumberException extends IllegalArgumentException {
    public NotAPrimeNumberException(String errorMessage) {
        super(errorMessage);
    }
}

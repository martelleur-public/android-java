package com.martelleur.anvndaensqlitedatabas.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvndaensqlitedatabas.R;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

import java.util.List;

/**
 * An adapter used for displaying mined prime numbers.
 */
public class PrimeNumberListAdapter extends RecyclerView.Adapter<PrimeNumberListAdapter.ViewHolder>{
    final private int primeNumberItemLayout;
    private List<PrimeNumber> primeNumberList;
    private final Context context;

    public PrimeNumberListAdapter(int layoutId,
                                  List<PrimeNumber> primeNumbers,
                                  Context context) {
        primeNumberItemLayout = layoutId;
        this.primeNumberList = primeNumbers;
        this.context = context;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setPrimeNumberList(List<PrimeNumber> primeNumbers) {
        primeNumberList = primeNumbers;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of items in the adapter.
     */
    @Override
    public int getItemCount() {
        return primeNumberList == null ? 0 : primeNumberList.size();
    }

    /**
     * Used to inflate the layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(primeNumberItemLayout,
                        parent, false);
        return new ViewHolder(view);
    }

    /**
     * Used to update prime number items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView primeId = holder.primeId;
        TextView primeValue = holder.primeValue;
        TextView primeMinedDate = holder.primeMinedDate;
        PrimeNumber primeNumber = primeNumberList.get(listPosition);
        context.getString(R.string.prime_number_item_id, primeNumberList.get(listPosition).getId());
        primeId.setText(context.getString(R.string.prime_number_item_id, primeNumber.getId()));
        primeValue.setText(context.getString(R.string.prime_number_item_value, primeNumber.getValue()));
        primeMinedDate.setText(context.getString(R.string.prime_number_item_date, primeNumber.getFormattedDate()));
    }

    /**
     * A view holder that describes item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView primeId;
        TextView primeValue;
        TextView primeMinedDate;
        ViewHolder(View itemView) {
            super(itemView);
            primeId = itemView.findViewById(R.id.prime_id);
            primeValue = itemView.findViewById(R.id.prime_value);
            primeMinedDate = itemView.findViewById(R.id.prime_mined_date);
        }
    }
}

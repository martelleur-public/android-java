package com.martelleur.anvndaensqlitedatabas.model;

import java.util.List;

public interface LoadPrimeNumberCallback {
    void onStart(List<PrimeNumber> primeNumbers);
    void onComplete(List<PrimeNumber> primeNumbers);
    void onError(List<PrimeNumber> primeNumbers);
}

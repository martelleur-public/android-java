package com.martelleur.anvndaensqlitedatabas;

import android.app.Application;

import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberRepository;

/**
 * Extending application and initialize PrimeNumberRepository.
 * This class is registered in the AndroidManifest.xml file, and an
 * instance of this class will be created when the application is started.
 * Ref: <a href="https://developer.android.com/guide/topics/manifest/application-element#nm">...</a>.
 */
public class PrimeNumberApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        PrimeNumberRepository.initialize(this);
    }
}

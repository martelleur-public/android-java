package com.martelleur.anvndaensqlitedatabas.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvndaensqlitedatabas.model.PrimeNumberRepository;

/**
 * Class used to build an instance of type [PrimeNumberViewModel].
 */
public class PrimeNumberViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final PrimeNumberRepository repository = PrimeNumberRepository.getInstance();

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new PrimeNumberViewModel(repository);
    }
}

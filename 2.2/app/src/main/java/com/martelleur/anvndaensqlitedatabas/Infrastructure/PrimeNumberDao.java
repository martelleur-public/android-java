package com.martelleur.anvndaensqlitedatabas.Infrastructure;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

import java.util.List;

/**
 * Class used to access prime number
 * data from sqlite database.
 */
@Dao
public interface PrimeNumberDao {

    @Insert
    void insertPrimeNumber(PrimeNumber primeNumber);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPrimeNumbers(List<PrimeNumber> primeNumber);

    @Query("SELECT * FROM primeNumbers")
    List<PrimeNumber> getPrimeNumbers();

    @Query("SELECT * FROM primeNumbers " +
            "ORDER BY primeValue DESC")
    List<PrimeNumber> getPrimeNumbersReverseOrder();

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue <= (:maxValue) " +
            "AND primeValue >= (:minValue)")
    List<PrimeNumber> getPrimeNumbersInInterval(long minValue, long maxValue);

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue <= (:maxValue) " +
            "AND primeValue >= (:minValue)" +
            "ORDER BY primeValue DESC")
    List<PrimeNumber> getPrimeNumbersInIntervalReverseOrder(long minValue, long maxValue);

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue >= (:minValue)")
    List<PrimeNumber> getPrimeNumbersAfter(long minValue);

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue >= (:minValue)" +
            "ORDER BY primeValue DESC")
    List<PrimeNumber> getPrimeNumbersAfterReverseOrder(long minValue);

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue <= (:maxValue)")
    List<PrimeNumber> getPrimeNumbersBefore(long maxValue);

    @Query("SELECT * FROM primeNumbers " +
            "WHERE primeValue <= (:maxValue)" +
            "ORDER BY primeValue DESC")
    List<PrimeNumber> getPrimeNumbersBeforeReverseOrder(long maxValue);

    @Query("SELECT * FROM primeNumbers " +
            "ORDER BY primeValue DESC LIMIT 1")
    List<PrimeNumber> getLastMinedPrimeValue();
}

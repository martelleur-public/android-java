package com.martelleur.anvndaensqlitedatabas.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A Class representing a valid prime number with a date and an ID.
 * The class is also used as a table for the database.
 */
@Entity(tableName = "primeNumbers")
public class PrimeNumber {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "primeId")
    private long id;

    @ColumnInfo(name = "primeValue")
    private final long value;

    @ColumnInfo(name = "primeDate")
    private final Date date;

    public PrimeNumber(long value, Date date) throws NotAPrimeNumberException {
        if (!isPrimeNr(value)) {
            throw new NotAPrimeNumberException("Value must be a prime number");
        }
        this.id = id;
        this.value = value;
        this.date = date;
    }

    /**
     * Used to get the id of this prime number.
     */
    public long getId() {
        return id;
    }

    /**
     * Used to set the id of this prime number.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Used to get the value of this prime number.
     */
    public long getValue() {
        return value;
    }

    /**
     * Used to get the date when this prime number where mined.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Used to get a formatted date when this prime number where mined.
     */
    public String getFormattedDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd-HH:mm:ss", Locale.getDefault());
        return formatter.format(date);
    }

    /**
     * Test if an number is a prime number.
     * @param value - the value of the number to be tested.
     * @return - true if the number is a prime number, otherwise it return false.
     */
    private boolean isPrimeNr (long value) {
        // Prime number can not be 0, 1, or negative.
        if (value <= 1) {
            return false;
        }

        // Prime number can not be a product of two smaller numbers.
        double halfTheValue = value / 2d;
        for (long i = 2; i < halfTheValue; i++) {
            if (value % i == 0) {
              return false;
            }
        }
        return true;
    }
}

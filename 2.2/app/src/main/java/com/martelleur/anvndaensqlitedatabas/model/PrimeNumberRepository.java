package com.martelleur.anvndaensqlitedatabas.model;

import android.app.Application;

import com.martelleur.anvndaensqlitedatabas.Infrastructure.FileManager;
import com.martelleur.anvndaensqlitedatabas.Infrastructure.PrimeNumberDB;
import com.martelleur.anvndaensqlitedatabas.Infrastructure.PrimeNumberDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * A repository class used for accessing and writing
 * data concerning mined prime number to persistent
 * data.
 */
public class PrimeNumberRepository {
    private static PrimeNumberRepository INSTANCE;
    private final ExecutorService executorService =
            Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private final PrimeNumberDao primeNumberDao;
    private final FileManager fileManager = new FileManager();
    private final String appStorageFile = "app_storage.env";
    private final String LAST_CHECKED_NUMBER_KEY = "LAST_CHECKED_NUMBER";

    private PrimeNumberRepository(Application application) {
        PrimeNumberDB db = PrimeNumberDB.getDatabase(application);
        primeNumberDao = db.primeNumberDao();
        fileManager.initialize(application, appStorageFile);
    }

    /**
     * Used to store last checked prime number in file.
     */
    public void storeLastCheckedNumber(long lastCheckedNumber) {
        fileManager.writeToFile(LAST_CHECKED_NUMBER_KEY +
                "=" + lastCheckedNumber);
    }

    /**
     * Used to store prime number in database.
     */
    public void storePrimeNumber(PrimeNumber primeNumber) {
        primeNumberDao.insertPrimeNumber(primeNumber);
    }

    /**
     * Get start value for mining.
     */
    public long getStartValueForMining() {
        return getLastCheckedNumber() + 1;
    }

    /**
     * Get Last checked prime number, either from file or if file not exist
     * or error when reading value from file get last mined prime number
     * from the database.
     */
    private long getLastCheckedNumber() {
        long lastCheckedValue = 0;
        HashMap<String, String> content = fileManager.getKeyValueFileContent(appStorageFile);
        if (content.containsKey(LAST_CHECKED_NUMBER_KEY)) {
            lastCheckedValue = getLastCheckedNumberFromFile(content);
        } else {
            lastCheckedValue = getLastMinedPrimeValue();
        }
        return lastCheckedValue;
    }

    /**
     * Get last checked number from file.
     */
    private long getLastCheckedNumberFromFile(HashMap<String, String> fileContent) {
        long lastCheckedValue = 0;
        try {
            String startValueString = Objects.requireNonNull(fileContent.get(LAST_CHECKED_NUMBER_KEY));
            lastCheckedValue = Integer.parseInt(startValueString);
        } catch (NullPointerException | NumberFormatException ignore) {
            lastCheckedValue = getLastMinedPrimeValue();
        }
        return lastCheckedValue;
    }

    /**
     * Get last mined prime number from the database.
     */
    private long getLastMinedPrimeValue() {
        long lastMinedPrimeNumber = 0;
        Callable<List<PrimeNumber>> callable = primeNumberDao::getLastMinedPrimeValue;
        List<PrimeNumber> lastPrime = getValueFromPrimeNumberCallable(callable);
        if (lastPrime.size() == 1) {
            lastMinedPrimeNumber = lastPrime.get(0).getValue();
        }
        return lastMinedPrimeNumber;
    }

    /**
     * Get value from prime number callable.
     */
    private List<PrimeNumber> getValueFromPrimeNumberCallable(Callable<List<PrimeNumber>> callable) {
        try {
            Future<List<PrimeNumber>> future =
                    executorService.submit(callable);
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * Load prime number from database.
     */
    public void loadPrimeNumbers(boolean reverseOrder,
                                 LoadPrimeNumberCallback callback) {
        Callable<List<PrimeNumber>> callable = reverseOrder ?
                primeNumberDao::getPrimeNumbersReverseOrder :
                primeNumberDao::getPrimeNumbers;
        Future<List<PrimeNumber>> primeNumbers = executorService.submit(callable);
        handlePrimeNumberLoading(primeNumbers, callback);
    }

    /**
     * Handle loading prime number with callbacks for start,
     * finished and error state.
     */
    private void handlePrimeNumberLoading(Future<List<PrimeNumber>> primeNumbers,
                                          LoadPrimeNumberCallback callback) {
        callback.onStart(new ArrayList<>());
        Runnable loadPrimeNumbers = () -> {
            try {
                callback.onComplete(primeNumbers.get());
            } catch (ExecutionException | InterruptedException e) {
                callback.onError(new ArrayList<>());
            }
        };
        executorService.execute(loadPrimeNumbers);
    }

    /**
     * Load prime number from database in specific interval defined
     * by min and max values.
     */
    public void loadPrimeNumbersInInterval(long minValue,
                                           long maxValue,
                                           boolean reverseOrder,
                                           LoadPrimeNumberCallback callback) {
        Callable<List<PrimeNumber>> callable = reverseOrder ?
                () -> primeNumberDao.getPrimeNumbersInIntervalReverseOrder(minValue, maxValue) :
                () -> primeNumberDao.getPrimeNumbersInInterval(minValue, maxValue);
        Future<List<PrimeNumber>> primeNumbers = executorService.submit(callable);
        handlePrimeNumberLoading(primeNumbers, callback);
    }

    /**
     * Load prime number from database in specific interval defined
     * by a min value.
     */
    public void loadPrimeNumbersAfter(long minValue,
                                      boolean reverseOrder,
                                      LoadPrimeNumberCallback callback) {
        Callable<List<PrimeNumber>> callable = reverseOrder ?
                () -> primeNumberDao.getPrimeNumbersAfterReverseOrder(minValue) :
                () -> primeNumberDao.getPrimeNumbersAfter(minValue);
        Future<List<PrimeNumber>> primeNumbers = executorService.submit(callable);
        handlePrimeNumberLoading(primeNumbers, callback);
    }

    /**
     * Load prime number from database in specific interval defined
     * by a max value.
     */
    public void loadPrimeNumbersBefore(long maxValue,
                                       boolean reverseOrder,
                                       LoadPrimeNumberCallback callback) {
        Callable<List<PrimeNumber>> callable = reverseOrder ?
                () -> primeNumberDao.getPrimeNumbersBeforeReverseOrder(maxValue) :
                () -> primeNumberDao.getPrimeNumbersBefore(maxValue);
        Future<List<PrimeNumber>> primeNumbers = executorService.submit(callable);
        handlePrimeNumberLoading(primeNumbers, callback);
    }

    /**
     * Use to initialize this repository.
     */
    public static void initialize(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new PrimeNumberRepository(application);
        }
    }

    /**
     * Used to get a single instance of this repository.
     * @return a PrimeNumberRepository.
     */
    public static PrimeNumberRepository getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("Prime number rpo not initialized.");
        }
        return INSTANCE;
    }
}

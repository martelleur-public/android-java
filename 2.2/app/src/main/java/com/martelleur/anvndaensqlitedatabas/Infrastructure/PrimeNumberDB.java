package com.martelleur.anvndaensqlitedatabas.Infrastructure;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

/**
 * A class describing the sqlite database tables
 * used for this application.
 */
@Database(
        entities = {PrimeNumber.class},
        version = 1
)
@TypeConverters({DBConverter.class})
public abstract class PrimeNumberDB extends RoomDatabase {

    public abstract PrimeNumberDao primeNumberDao();
    private static PrimeNumberDB INSTANCE;

    public static PrimeNumberDB getDatabase(final Context context) {
        synchronized (PrimeNumberDB.class) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        context.getApplicationContext(),
                        PrimeNumberDB.class,
                        "prime_number_database").build();
            }
        }
        return INSTANCE;
    }
}

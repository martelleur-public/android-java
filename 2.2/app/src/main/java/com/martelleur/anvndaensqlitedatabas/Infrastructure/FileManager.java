package com.martelleur.anvndaensqlitedatabas.Infrastructure;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.martelleur.anvndaensqlitedatabas.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Used to write and read persistent data to and from files.
 * The class is used for storing data concerning latest checked
 * prime number so the application can start mining prime
 * numbers from the last tested number.
 */
public class FileManager extends FileProvider {
    private Context context;
    private String fileName;

    String TAG = "FileManager";

    public FileManager() {
        super(R.xml.files);
    }

    public void initialize(Application application,
                           String fileName) {
        this.context = application;
        this.fileName = fileName;
    }

    /**
     * Used to get key=value items from file and save it in a map.
     */
    public HashMap<String,String> getKeyValueFileContent(String fileName) {
        Uri uri = getUriForFile(new File(context.getFilesDir(), fileName));
        HashMap<String, String> appValues = new HashMap<>();
        try {
            InputStream inputStream =
                    context.getContentResolver().openInputStream(uri);
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(
                            inputStream));
            appValues = getConvertedKeyValueItems(reader);
            reader.close();
            inputStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return appValues;
    }


    /**
     * Used to convert key value items to a map.
     */
    private HashMap<String, String> getConvertedKeyValueItems(BufferedReader reader) throws IOException {
        String currentLine;
        HashMap<String, String> appValues = new HashMap<>();
        while ((currentLine = reader.readLine()) != null) {
            String[] keyValuePair = currentLine.split("=", 2);
            if (keyValuePair.length > 1) {
                String key = keyValuePair[0];
                String value = keyValuePair[1];
                appValues.putIfAbsent(key,value);
            }
        }
        return appValues;
    }

    /**
     * Used to write text a to a file.
     */
    public void writeToFile(String textContent) {
        try {
            Uri uri = getUriForFile(new File(context.getFilesDir(), fileName));

            ParcelFileDescriptor pfd =
                    context.getContentResolver().
                            openFileDescriptor(uri, "w");

            FileOutputStream fileOutputStream =
                    new FileOutputStream(
                            pfd.getFileDescriptor());

            fileOutputStream.write(textContent.getBytes());

            fileOutputStream.close();
            pfd.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Used to get the uri for a file.
     */
    private Uri getUriForFile(File file) {
        return getUriForFile(context,
                "com.martelleur.anvndaensqlitedatabas.fileprovider",
                file);
    }
}

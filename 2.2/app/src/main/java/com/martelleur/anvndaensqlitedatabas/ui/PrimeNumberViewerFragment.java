package com.martelleur.anvndaensqlitedatabas.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvndaensqlitedatabas.R;
import com.martelleur.anvndaensqlitedatabas.databinding.FragmentPrimeNumberViewerBinding;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

import java.util.List;

/**
 * A fragment for displaying prime numbers, specifically;
 * 1) The value of the prime number.
 * 2) Date when the prime number where found.
 * 3) Prime number ID.
 */
public class PrimeNumberViewerFragment extends Fragment {
    private FragmentPrimeNumberViewerBinding binding;
    private PrimeNumberListAdapter primeNrListAdapter;
    private PrimeNumberViewModel viewModel;

    /**
     * Bind layout using view binding instance
     * FragmentPrimeNumberViewerBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentPrimeNumberViewerBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance, add a recycler, and add a
     * observer that listen on prime number livedata.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PrimeNumberViewModelFactory factory = new PrimeNumberViewModelFactory();
        viewModel = new ViewModelProvider(requireActivity(), factory).get(PrimeNumberViewModel.class);
        setUpPrimeNumberRecycler(viewModel.getPrimeNumbersLiveData().getValue());
        setUpPrimeNumberObserver();
    }

    /**
     * Setup a recycler to view prime numbers.
     */
    private void setUpPrimeNumberRecycler(List<PrimeNumber> primeNumbers) {
        primeNrListAdapter = new PrimeNumberListAdapter(
                R.layout.prime_number_item,
                primeNumbers,
                requireContext());
        RecyclerView recyclerView = binding.primeNumberRecycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(primeNrListAdapter);
    }

    /**
     * Set up a observer that listen on prime number livedata.
     */
    public void setUpPrimeNumberObserver() {
        viewModel.getPrimeNumbersLiveData().observe(requireActivity(),
                primeNumbers -> primeNrListAdapter.setPrimeNumberList(primeNumbers));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
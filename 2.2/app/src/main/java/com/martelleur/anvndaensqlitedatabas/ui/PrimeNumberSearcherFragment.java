package com.martelleur.anvndaensqlitedatabas.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.martelleur.anvndaensqlitedatabas.R;
import com.martelleur.anvndaensqlitedatabas.databinding.FragmentPrimeNumberSearcherBinding;
import com.martelleur.anvndaensqlitedatabas.model.PrimeNumber;

import java.util.Collections;
import java.util.List;


/**
 * A fragment for displaying a search area where a user can choose
 * to view prime numbers in an interval. Specifically;
 * 1) Prime number between a min and max value, and
 * 2) Prime number in reverse order.
 */
public class PrimeNumberSearcherFragment extends Fragment {
    private FragmentPrimeNumberSearcherBinding binding;
    private PrimeNumberViewModel viewModel;
    private boolean primeNumberReverseArrangementState = false;

    /**
     * Bind layout using view binding instance
     * FragmentPrimeNumberSearcherBinding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        binding = FragmentPrimeNumberSearcherBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created get view model instance and setup listeners and observer.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = getPrimeNumberViewModel();
        setupOnLoadingPrimeNumberObserver();
        setupOnPrimeNumberReverseOrderListener();
        setupOnPrimeNrListener();
        displayNoLoading();
    }

    /**
     * Used to get an instance of prime number view model.
     */
     private PrimeNumberViewModel getPrimeNumberViewModel() {
         PrimeNumberViewModelFactory factory = new PrimeNumberViewModelFactory();
         return new ViewModelProvider(requireActivity(), factory).get(PrimeNumberViewModel.class);
     }

    /**
     * Observe if user have selected to load prime numbers.
     */
    private void setupOnLoadingPrimeNumberObserver() {
        viewModel.isLoadingPrimeNumbers().observe(
                getViewLifecycleOwner(),
                isLoadingPrimeNumbers -> {
                    if (isLoadingPrimeNumbers) displayLoading();
                    else displayNoLoading();
                });
    }

    /**
     * Used to display loading prime numbers.
     */
    private void displayLoading() {
        binding.viewPrimeNr.setVisibility(View.GONE);
        binding.loader.setVisibility(View.VISIBLE);
    }

    /**
     * Used to display prime numbers.
     */
    private void displayNoLoading() {
        binding.viewPrimeNr.setVisibility(View.VISIBLE);
        binding.loader.setVisibility(View.GONE);
    }

    /**
     * Listen on user check to view prime numbers in reverse order.
     */
    private void setupOnPrimeNumberReverseOrderListener() {
        binding.primeNumberReverseOrderCheckbox.setOnClickListener(view -> {
            updatePrimeNumberReverseArrangementState(view);
            reversePrimeNumbersLiveDataArrangement();
        });
    }

    /**
     * Used to update instance variable primeNumberReverseArrangementState
     * dependent on if checkbox is checked or not. This is used for
     * deciding if the prime numbers should loaded in reverse order or not.
     */
    private void updatePrimeNumberReverseArrangementState(View view) {
        if (view instanceof CheckBox) {
            primeNumberReverseArrangementState = ((CheckBox) view).isChecked();
        }
    }

    /**
     * Used to reverse prime numbers livedata.
     */
    private void reversePrimeNumbersLiveDataArrangement() {
        List<PrimeNumber> primeNumbers = viewModel.getPrimeNumbersLiveData().getValue();
        if (primeNumbers != null && primeNumbers.size() > 1) {
            Collections.reverse(primeNumbers);
            viewModel.setPrimeNumbersLiveData(primeNumbers);
        }
    }

    /**
     * Listen on user select to view prime numbers.
     */
    private void setupOnPrimeNrListener() {
        binding.viewPrimeNr.setOnClickListener(view ->
                loadPrimeNumbersFromUserInput());
    }

    /**
     * Load all mined prime numbers, or prime numbers in a
     * interval decided from user input.
     */
    private void loadPrimeNumbersFromUserInput() {
        String minInput = String.valueOf(binding.minValue.getText());
        String maxInput = String.valueOf(binding.maxValue.getText());
        String emptyInput = "";
        boolean minInputExist = !minInput.equals(emptyInput);
        boolean maxInputExist = !maxInput.equals(emptyInput);
        try {
            if (maxInputExist && minInputExist) {
                viewModel.loadPrimeNumbersInInterval(Long.parseLong(minInput),
                        Long.parseLong(maxInput), primeNumberReverseArrangementState);
            } else if (!maxInputExist && minInputExist) {
                viewModel.loadPrimeNumbersAfter(Long.parseLong(minInput),
                        primeNumberReverseArrangementState);
            } else if (maxInputExist && !minInputExist) {
                viewModel.loadPrimeNumbersBefore(Long.parseLong(maxInput),
                        primeNumberReverseArrangementState);
            } else {
                viewModel.loadPrimeNumbers(primeNumberReverseArrangementState);
            }
        } catch (NumberFormatException e) {
            handleParseInputError();
        }
    }

    /**
     * Handle input parse error,
     */
    private void handleParseInputError () {
        String msg = getString(R.string.input_error, Long.MAX_VALUE);
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
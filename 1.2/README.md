# Uppgit 1.1 samt 1.2 

_Lägga ut komponeter_ samt _Koppla till händelsehantering_

Följer stegen beskrivna i [Google Developers: Build Your First Android App in Java](https://developer.android.com/codelabs/build-your-first-android-app#0)

## Steg 1-4

- Steg 1: Ok
- Steg 2 installera Android Studion: Ok
- Steg 3 skapa projekt: Ok

## Steg 4; Utforks layout editor

- ok

## Steg 5; Lägga till färger

- ok

## Steg 6; Lägga till button

- ok

## Steg 7; Uppdatera utseendet

- ok

## Steg 8; Gör programmet interaktivt

- ok

## Steg 9; Lägg till ett andra Fragment

- ok

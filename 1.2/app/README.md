# Lägga ut komponenter samt Koppla till händelsehantering

## Lägga ut komponenter
Prototyp som följer [Build Your First Android App in Java](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://developer.android.com/codelabs/build-your-first-android-app&session=282501)» för att börja bygga en första Android app.

## Koppla till händelsehantering
Prototypen introducerar till hur man kopplar ihop användarens interaktion med komponenter i ett grafiskt användargränssnitt med en viss funktion.

Fortsätt stegen under Google Developers: [Build Your First Android App in Java](https://people.dsv.su.se/~pierre/i/i.cgi?href=https://developer.android.com/codelabs/build-your-first-android-app&session=282501)» för att bygga klart en första Android app.

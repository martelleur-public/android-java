package com.example.laggautkomponentersamtkopplatillhandelsehantering;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.laggautkomponentersamtkopplatillhandelsehantering.databinding.FragmentSecondBinding;

import java.util.Random;

/**
 * A Second hello world fragment used to display
 * a random number in the interval provided by the argument
 * to the fragment.
 */
public class SecondFragment extends Fragment {
    private FragmentSecondBinding binding;
    private MainViewModel viewModel;
    private static final String ARG_CURRENT_COUNT = "second_fragment_current_count";
    private int currentCount = 0;

    public SecondFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameter currentCount.
     */
    public static SecondFragment newInstance(int currentCount) {
        SecondFragment fragment = new SecondFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CURRENT_COUNT, currentCount);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Used to restore host and port stored in bundle object.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentCount = getArguments().getInt(ARG_CURRENT_COUNT);
        }
    }

    /**
     * Bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        binding = FragmentSecondBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created;
     * 1) Update view from fragment argument "current count".
     * 2) Add navigate back listener.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateViewFromArgs();
        setupNavigateBackToFirstFragment();
    }

    /**
     * Used to update view from argument.
     */
    private void updateViewFromArgs() {
        String countText = getString(R.string.random_heading, currentCount);
        binding.textviewSecond.setText(countText);
        String randomNumber = String.valueOf(getRandomNumber(currentCount));
        binding.randomText.setText(randomNumber);
    }

    /**
     * Used to generate a random number between
     * 0 and a given max value + 1.
     */
    private Integer getRandomNumber(Integer maxValue) {
        Random random = new java.util.Random();
        int randomNumber = 0;
        if (maxValue > 0) {
            randomNumber = random.nextInt(maxValue + 1);
        }
        return randomNumber;
    }

    /**
     * Used for navigating back to first fragment.
     */
    private void setupNavigateBackToFirstFragment() {
        binding.buttonSecond.setOnClickListener(view -> {
            viewModel.setCount(0); // Reset count.
            FragmentManager fragmentManager = getParentFragmentManager();
            fragmentManager.popBackStack(
                    FirstFragment.navigateFromFirstFragmentToSecondFragment,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        });
    }

    /**
     * Used to save current count value to bundle object.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_CURRENT_COUNT, currentCount);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
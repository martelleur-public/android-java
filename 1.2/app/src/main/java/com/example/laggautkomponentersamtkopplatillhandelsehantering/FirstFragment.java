package com.example.laggautkomponentersamtkopplatillhandelsehantering;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.laggautkomponentersamtkopplatillhandelsehantering.databinding.FragmentFirstBinding;

/**
 * A fragment used as hello world fragment that provide
 * interaction and navigation examples.
 */
public class FirstFragment extends Fragment {
    private FragmentFirstBinding binding;
    private MainViewModel viewModel;
    final static String navigateFromFirstFragmentToSecondFragment = "firstFragmentToSecondFragment";

    public FirstFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static FirstFragment newInstance() {
        return new FirstFragment();
    }

    /**
     * Bind layout using view binding and ad a view model
     * for holding the state of this fragment
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewModel = new ViewModelProvider(requireActivity()).get(MainViewModel.class);
        binding = FragmentFirstBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * On view created add
     * 1) Random listener used to navigate to second fragment.
     * 2) A toast listener used to show a hello world toast.
     * 3) A count listener used to update current count.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updatedViewFromViewModel();
        addOnRandomListener();
        addOnToastListener();
        addOnCountListener();
    }

    /**
     * Used to update view from view model.
     */
    private void updatedViewFromViewModel() {
        String currentCount = String.valueOf(viewModel.getCount());
        binding.showCountTextview.setText(currentCount);
    }

    /**
     * Used to navigate to second fragment with a
     * current count as argument.
     */
    private void addOnRandomListener() {
        binding.randomButton.setOnClickListener(view -> {
            int enter = R.anim.slide_in;
            int exit = R.anim.fade_out;
            int popEnter = R.anim.fade_in;
            int popExit = R.anim.slide_out;
            int currentCount = Integer.parseInt(binding.showCountTextview.getText().toString());
            FragmentManager fragmentManager = getParentFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(enter, exit, popEnter, popExit)
                    .add(R.id.container, SecondFragment.newInstance(currentCount))
                    .addToBackStack(navigateFromFirstFragmentToSecondFragment)
                    .commit();
        });
    }

    /**
     * Used to display a hello world toast.
     */
    private void addOnToastListener() {
        binding.toastButton.setOnClickListener(view -> {
            Toast myToast = Toast.makeText(
                    requireContext(),
                    getString(R.string.toast_hello_world),
                    Toast.LENGTH_SHORT);
            myToast.show();
        });
    }

    /**
     * Used to add a count listener.
     */
    private void addOnCountListener() {
        binding.countButton.setOnClickListener(view -> {
            Integer count = viewModel.getCount();
            viewModel.setCount(++count);
            String currentCount = String.valueOf(viewModel.getCount());
            binding.showCountTextview.setText(currentCount);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
package com.example.laggautkomponentersamtkopplatillhandelsehantering;

import androidx.lifecycle.ViewModel;

/**
 * A view model holding the UI state of the application.
 */
public class MainViewModel extends ViewModel {
    private Integer count = 0;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) throws IllegalArgumentException {
        if (count < 0) {
            throw new IllegalArgumentException("Count must be larger or equal to zero.");
        }
        this.count = count;
    }
}

package com.example.laggautkomponentersamtkopplatillhandelsehantering;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Launch activity for the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Used to launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, FirstFragment.newInstance())
                    .commitNow();
        }
    }
}
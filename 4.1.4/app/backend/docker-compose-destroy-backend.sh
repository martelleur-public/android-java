#/bin/bash

# Close docker deployment, and remove 
# bridge network on "10.0.100.0/24 and external volume."
docker compose -f docker-compose.yml down
docker volume rm guestbook_volume
docker network rm guestbook_net

# Delete env folder.
ENV_DIR=env
if [ -d ${ENV_DIR} ]; then
    rm -Rf ${ENV_DIR}
fi
#!/bin/bash

SUBNET="${1:-10.0.100.0/24}"

# Copy and overwrite default insecure environment variables to env folder.
ENV_DIR=env
ENV_DEFAULT=env_example
if [ ! -d ${ENV_DIR} ]; then
    cp -r ${ENV_DEFAULT} ${ENV_DIR}
else
    rm -Rf ${ENV_DIR} 
    cp -r ${ENV_DEFAULT} ${ENV_DIR}
fi

# Start up docker deployment with a bridge network on "10.0.100.0/24"
docker volume create guestbook_volume
docker network create -d bridge --subnet $SUBNET guestbook_net
docker compose -f docker-compose.yml up -d
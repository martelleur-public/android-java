SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `chatapplication`
--
DROP DATABASE IF EXISTS `guestbook`;
CREATE DATABASE IF NOT EXISTS `guestbook` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `guestbook`;

-- --------------------------------------------------------

--
-- Table structure for table `guestbook`
--

DROP TABLE IF EXISTS `guestbook_table`;
CREATE TABLE `guestbook_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `mail` text NOT NULL,
  `webpage` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

ALTER TABLE guestbook_table AUTO_INCREMENT=100;

--
-- Insert dummy values
--

INSERT INTO `guestbook_table` (`name`, `mail`, `webpage`, `comment`) VALUES
('Pit', 'pit@mail', 'www.pit.com', 'I let my racket do the talking.'),
('Nadal', 'Nadal@mail', 'www.Nadal.com', 'I always work with a goal, and the goal is to improve as a player and a person.'),
('Djockovich', 'Djockovich@mail', 'www.Djockovich.com', 'I think luck falls on not just the brave but also the ones who believe they belong there.'),
('Federer', 'Federer@mail', 'www.Federer.com', 'Im a very positive thinker, and I think that is what helps me the most in difficult moments.'),
('Pit', 'pit@mail', 'www.pit.com', 'You kind of live and die by the serve.'),
('Nadal', 'Nadal@mail', 'www.Nadal.com', 'If you do not lose, you cannot enjoy the victories.'),
('Djockovich', 'Djockovich@mail', 'www.Djockovich.com', 'I stopped thinking too much about what could happen and relied on my physical and mental strength to play the right shots at the right time.'),
('Federer', 'Federer@mail', 'www.Federer.com', 'When you do something best in life, you do not really want to give that up - and for me it is tennis.'),
('Pit', 'pit@mail', 'www.pit.com', 'The difference of great players is at a certain point in a match they raise their level of play and maintain it.'),
('Nadal', 'Nadal@mail', 'www.Nadal.com', 'I learned during all my career to enjoy suffering.'),
('Djockovich', 'Djockovich@mail', 'www.Djockovich.com', 'Tennis is a mental game.'),
('Federer', 'Federer@mail', 'www.Federer.com', 'You always want to win.')

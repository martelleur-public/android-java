from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Text

from . import database as db

"""
A ORM class used for interacting with the 
table guestbook_table in the GuestBook DB.
Ref: https://docs.sqlalchemy.org/en/20/dialects/mysql.html#module-sqlalchemy.dialects.mysql.pymysql
"""
class GuestBookModel(db.Base):
    __tablename__ = "guestbook_table"
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    name = Column(Text)
    mail = Column(Text)
    webpage = Column(Text)
    comment = Column(Text)

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from decouple import config

"""
Module used to connect to mysql db. The module use
sqlalchemy as ORM library and PyMySQL as a database driver.
"""

DB_HOST = config('DB_HOST', cast=str)
DB_PORT = config('DB_PORT', cast=str)
DB_NAME = config('DB_NAME', cast=str)
DB_USER = config('DB_USER', cast=str)
DB_PW = config('DB_PW', cast=str)

# Ref: - https://docs.sqlalchemy.org/en/20/dialects/mysql.html#module-sqlalchemy.dialects.mysql.pymysql
#      - https://pymysql.readthedocs.io/en/latest/user/installation.html
DATABASE_URL = (
    f"mysql+pymysql://{DB_USER}:{DB_PW}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    f"?charset=utf8mb4"
)

engine = create_engine(DATABASE_URL)

# Used to create a database session.
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# A declarative base instance used to create database ORM models:
Base = declarative_base()

# Used to connect to the database.
def get_db():
    # Create all Base tables.
    Base.metadata.create_all(bind=engine) 
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

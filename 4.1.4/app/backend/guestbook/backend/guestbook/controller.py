from typing import List, Union
from sqlalchemy.orm import Session

from .schema import GuestBookSchema
from .model import GuestBookModel

"""
Module used to implement application service 
operations for handling guestbooks requests.
"""

def get_all_guest_books(db: Session) -> Union[GuestBookModel, None]:
    try: 
        return db.query(GuestBookModel).all()
    except Exception as e:
        return None

def create_guestbook(
        db: Session,
        schema: GuestBookSchema) -> Union[List[GuestBookModel], None]:
    
    guestbook_model = GuestBookModel(name=schema.name,
                                     mail=schema.mail,
                                     webpage=schema.webpage,
                                     comment=schema.comment)
    try:
        db.add(guestbook_model)
        db.commit()
        db.refresh(guestbook_model)
        return guestbook_model
    except Exception as e:
        return None

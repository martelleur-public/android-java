from typing import List

from fastapi import Depends, APIRouter, HTTPException, status
from sqlalchemy.orm import Session

from .schema import GuestBookSchema
from . import controller as guestbook_controller
from . import database as db

"""
A router used to handle guestbook apis.
"""

router = APIRouter()

# Used to get all guestbooks.
@router.get("/", response_model=List[GuestBookSchema], status_code=200)
def get_all_guestbooks(db: Session = Depends(db.get_db)):
    guestbooks = guestbook_controller.get_all_guest_books(db)
    if guestbooks == None:
        raise HTTPException(status_code=400, detail="Could not get guestbooks")
    else:
        return guestbooks

# Used to create a new guestbook.
@router.post("/", response_model=GuestBookSchema, status_code=201)
def create_guestbook(guestbook: GuestBookSchema, db: Session = Depends(db.get_db)):
    guestbook =  guestbook_controller.create_guestbook(db, guestbook)
    if guestbook == None:
        raise HTTPException(status_code=400, detail="Could not create guestbook")
    else:
        return guestbook

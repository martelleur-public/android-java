import re
from pydantic import BaseModel, Field, validator

"""
# Class used to validate incomming data.
"""
class GuestBookSchema(BaseModel):
    name: str = Field(..., min_length=1, max_length=100)
    mail: str = Field(..., min_length=1, max_length=320)
    webpage: str = Field(..., min_length=1, max_length=2048)
    comment: str = Field(..., min_length=1, max_length=2048)

    @validator('name', 'mail', 'webpage', 'comment')
    def validate_no_html(cls, value):
        html_regex = r'<("[^"]*"|\'[^\']*\'|[^\'">])*>'
        if re.search(html_regex, value):
            raise ValueError('HTML tags are not allowed')
        return value

    class Config:
        orm_mode = True

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from guestbook.router import router as guestbook_router

"""
A simple http server used as backend for task 4.1.4; "A guestbook app".
"""

app = FastAPI()

# Used to configure CORS.
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)

@app.on_event("startup")
async def startup():
    print(f"Start server... ")

@app.on_event("shutdown")
async def shutdown():
    print(f"Close server...")

## Add router guestbook. 
app.include_router(guestbook_router, prefix="/guestbook", tags=["guestbook"])


# Backend for task 4.1.4 Guestbook App

__A simple backend implemented with:__ 
- Python
- FastAPI, uvicorn, PyMySQL, pydantic, etc see ```guestbook/backend/requirements.txt```
- MySQL
- docker
- docker-compose. 

## Start backend with insecure default environment variables.

- ```./docker-compose-start-questbook-backend.sh```

## API backend 

- API Doc: ```http://localhost:8000/docs```
- Get all guest books: ```http GET 127.0.0.1:8000/guestbook/```
- Create new guestbook: ```http POST http://127.0.0.1:8000/guestbook/ name="testuser" mail="test@gmail.com" webpage="www.test.com" comment="test api create new guestbook"```

## For backend development

- __Create virtual env:__
    - Change directory to ```guestbook/backend```
    - Create virtual env: ```python3 -m venv env```
    - Enter virtual env: ```source env/bin/activate```
    - Install wheel enter virtual env: ```python -m pip install wheel```
    - Leave virtual env: ```deactivate```

- __Install dependencies:__

    - Install from requirements file: ```python -m pip install -r requirements.txt```
    - To create requirement from installed packages ```python -m pip freeze > requirements.txt```

- __Setup environment:__

    - Create env files ```guestbook/backend/.env```

- __Start server set to reload:__

    - ```uvicorn main:app --reload```


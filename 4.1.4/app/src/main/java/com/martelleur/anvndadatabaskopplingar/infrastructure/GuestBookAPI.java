package com.martelleur.anvndadatabaskopplingar.infrastructure;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Used to connect to http backend api
 * for acquiring and creating guestbooks.
 */
public class GuestBookAPI implements GuestBookStore {
    private final String TAG = "GuestBookAPI";
    private static final String API_PROPERTY_FILE = "api.properties";
    private final String BASE_URL;
    private final String GUESTBOOK_PATH;
    private final Context context;
    private enum HTTPMethod {
        GET("GET"), POST("POST");
        private final String method;
        HTTPMethod(String method) { this.method = method; }
        public String getMethod() {
            return method;
        }
    }
    public GuestBookAPI(Context context) {
        this.context = context;
        Properties properties = loadAPIProperties();
        BASE_URL = properties.getProperty("api.url");
        GUESTBOOK_PATH = properties.getProperty("api.guestbook_path");
    }

    /**
     * Used to load api properties from api property file.
     */
    private Properties loadAPIProperties() {
        Properties properties;
        try {
            properties = new Properties();
            AssetManager assetManager =
                    Objects.requireNonNull(context).getAssets();
            InputStream inputStream = assetManager.open(API_PROPERTY_FILE);
            properties.load(inputStream);

        } catch (IOException e) {
            throw new RuntimeException("Could not load property: " + API_PROPERTY_FILE);
        }
        return properties;
    }

    /**
     * Used to request "get all guestbooks".
     */
    @Override
    public List<GuestBook> getAllGuestBooks() {
        BufferedReader responseBuffer = null;
        List<GuestBook> guestBooks = new ArrayList<>();
        HttpURLConnection httpConnection = null;
        try {
            httpConnection = getConnection(HTTPMethod.GET.getMethod());
            responseBuffer = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            checkExpectedStatusCode(HttpURLConnection.HTTP_OK, httpConnection.getResponseCode());
            guestBooks = getGuestBooksFromResponseBuffer(responseBuffer);
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        } finally {
            closeGetAllGuestBooks(httpConnection, responseBuffer);
        }
        return guestBooks;
    }

    /**
     * Used to get an open http connection to the guestbook backend api.
     */
    private HttpURLConnection getConnection(String httpMethod) throws IOException {
        URL url = new URL(BASE_URL + "/" + GUESTBOOK_PATH);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(httpMethod);
        connection.setRequestProperty("Content-Type", "application/json");
        return connection;
    }

    /**
     * Used to check if status code from backend api
     * is expected aka successful.
     */
    private void checkExpectedStatusCode(int expected, int actual) throws IOException {
        if (actual != expected) {
            throw new IOException("Response code from server: " + actual);
        }
    }

    /**
     * Used to get a list of guestbooks from the request
     * "get all guestbooks".
     */
    private List<GuestBook> getGuestBooksFromResponseBuffer(BufferedReader responseBuffer) throws IOException {
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = responseBuffer.readLine()) != null) {
            response.append(line);
        }
        return Arrays.asList(new Gson().fromJson(response.toString(), GuestBook[].class));
    }

    /**
     * Used to close connection and streams after
     * request "get all guestbooks".
     */
    private void closeGetAllGuestBooks(HttpURLConnection httpURLConnection,
                                       BufferedReader bufferedReader) {
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }

    /**
     * Used to request "adding a new guestbook".
     */
    @Override
    public GuestBook addGuestBook(GuestBook guestBook) {
        HttpURLConnection httpConnection = null;
        BufferedWriter requestBuffer = null;
        BufferedReader responseBuffer = null;
        GuestBook newGuestBook = null;
        try {
            httpConnection = getConnection(HTTPMethod.POST.getMethod());
            requestBuffer = sendAddGuestBookRequest(httpConnection, guestBook);
            checkExpectedStatusCode(HttpURLConnection.HTTP_CREATED, httpConnection.getResponseCode());
            responseBuffer = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            newGuestBook = getGuestBookFromResponseBuffer(responseBuffer);
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        } finally {
            closeAddGuestBook(httpConnection, requestBuffer, responseBuffer);
        }
        return newGuestBook;
    }

    /**
     * Used to send the request for "adding a new guestbook".
     */
    private BufferedWriter sendAddGuestBookRequest(HttpURLConnection httpURLConnection,
                                                   GuestBook guestBook) throws IOException {
        String guestBookJSON = new Gson().toJson(guestBook);
        OutputStream out = new BufferedOutputStream(
                httpURLConnection.getOutputStream());
        BufferedWriter requestBuffer = new BufferedWriter(new OutputStreamWriter(
                out, StandardCharsets.UTF_8));
        requestBuffer.write(guestBookJSON); // Used to write the content of guestBookJSON.
        requestBuffer.flush(); // Used to push the written content to the underlying output stream.
        return requestBuffer;
    }

    /**
     * Used to get a guestbook instance from the request
     * "adding a new guestbook".
     */
    private GuestBook getGuestBookFromResponseBuffer(BufferedReader responseBuffer) throws IOException {
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = responseBuffer.readLine()) != null) {
            response.append(line);
        }
        return new Gson().fromJson(response.toString(), GuestBook.class);
    }

    /**
     * Used to close connection and streams after
     * request "adding a new guestbook".
     */
    private void closeAddGuestBook(HttpURLConnection httpURLConnection,
                                   BufferedWriter bufferedWriter,
                                   BufferedReader bufferedReader) {
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        if (bufferedWriter != null) {
            try {
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bufferedReader != null) {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
            }
        }
    }

    @Override
    public void close() {
        // Not implemented.
    }
}

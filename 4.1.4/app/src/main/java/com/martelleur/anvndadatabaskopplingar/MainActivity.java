package com.martelleur.anvndadatabaskopplingar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.martelleur.anvndadatabaskopplingar.ui.GuestBookFragment;


/**
 * Launch activity for the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Used to launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, GuestBookFragment.newInstance())
                    .commitNow();
        }
    }
}

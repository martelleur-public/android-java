package com.martelleur.anvndadatabaskopplingar.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.martelleur.anvndadatabaskopplingar.R;
import com.martelleur.anvndadatabaskopplingar.databinding.FragmentAddGuestBookBinding;
import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.util.Objects;

/**
 * A fragment used to add a new guestbook.
 */
public class AddGuestBookFragment extends Fragment {
    private FragmentAddGuestBookBinding binding;
    GuestBookViewModel guestBookViewModel;

    public AddGuestBookFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static AddGuestBookFragment newInstance() {
        return new AddGuestBookFragment();
    }

    /**
     * Bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAddGuestBookBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance with factory class and add listeners
     * for creating a new guestbook and returning to previous fragment.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GuestBookViewModelFactory factory = new GuestBookViewModelFactory(getContext());
        guestBookViewModel = new ViewModelProvider(requireActivity(), factory).get(GuestBookViewModel.class);
        setupAddNewGuestBookListener();
    }

    /**
     * Add a click listener for creating a new guestbook and
     * for returning and closing this fragment.
     */
    private void setupAddNewGuestBookListener() {
        binding.addGuestBook.setOnClickListener(view -> {
            if (addNewGuestBook()) {
                navigateBackToPreviousFragment();
            }
        });
    }

    /**
     * Used to add a new guestbook from user input.
     */
    private boolean addNewGuestBook() {
        boolean isCreated = false;
        try {
            String name = Objects.requireNonNull(binding.nameInput.getText()).toString();
            String mail = Objects.requireNonNull(binding.mailInput.getText()).toString();
            String webpage = Objects.requireNonNull(binding.webpageInput.getText()).toString();
            String comment = Objects.requireNonNull(binding.commentInput.getText()).toString();
            GuestBook newGuestBook = guestBookViewModel.addGuestBook(
                    name, mail, webpage, comment);
            String successMsg = createSuccessMsg(newGuestBook);
            toastGuestBookCreatedStatus(successMsg);
            isCreated = true;
        } catch (GuestBook.FieldContainHTMLException |
                 GuestBook.EmptyGuestBookFieldException |
                 GuestBook.SizeNotAllowedException e) {
            toastGuestBookCreatedStatus(e.getMessage());
        } catch (NullPointerException e) {
            String failureMsg = getResources().getString(R.string.no_empty_fields_msg);
            toastGuestBookCreatedStatus(failureMsg);
        }
        return isCreated;
    }

    /**
     * Used to create a success message when a new guestbook
     * have been created.
     */
    private String createSuccessMsg(GuestBook newGuestBook) {
        String authorNewGuestBook = getResources().getString(R.string.author_new_guestbook_msg);
        return  authorNewGuestBook + "\n" +
                newGuestBook.getName() + "\n" +
                newGuestBook.getMail() + "\n" +
                newGuestBook.getWebpage() + "\n";
    }

    /**
     * Toast a message to the user describing if the guestbook
     * was created or not, and why it was not created.
     */
    private void toastGuestBookCreatedStatus(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Used to navigate back to previous fragment.
     */
    private void navigateBackToPreviousFragment() {
        FragmentManager fragmentManager =  getParentFragmentManager();
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .remove(this)
                .commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
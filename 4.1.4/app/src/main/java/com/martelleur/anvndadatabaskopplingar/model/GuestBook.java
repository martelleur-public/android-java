package com.martelleur.anvndadatabaskopplingar.model;

import com.martelleur.anvndadatabaskopplingar.constant.GuestBookInputField;
import com.martelleur.anvndadatabaskopplingar.constant.GuestBookInputFieldMaxSize;

/**
 * Model class used to describe a valid GuestBook with attributes
 * name, mail, webpage, and comments.
 * A guestbook instance can not contain attributes with html code,
 * nor empty or to big values.
 */
public class GuestBook {
    private final String name;
    private final String mail;
    private final String webpage;
    private final String comment;

    public GuestBook(String name,
                     String mail,
                     String webpage,
                     String comment)
            throws EmptyGuestBookFieldException,
            FieldContainHTMLException,
            SizeNotAllowedException {
        validateInputs(name, mail, webpage, comment);
        this.mail = mail;
        this.name = name;
        this.webpage = webpage;
        this.comment = comment;
    }

    /**
     * Used to validate the values of the attributes.
     * A guestbook instance can not contain attributes with html code,
     * nor empty or to big values.
     */
    private void validateInputs(String name,
                                String mail,
                                String webpage,
                                String comment)
            throws
            SizeNotAllowedException,
            EmptyGuestBookFieldException,
            FieldContainHTMLException {
        validateInput(name, GuestBookInputField.NAME.getLabel(),
                GuestBookInputFieldMaxSize.NAME.getSize());
        validateInput(mail, GuestBookInputField.MAIL.getLabel(),
                GuestBookInputFieldMaxSize.MAIL.getSize());
        validateInput(webpage, GuestBookInputField.WEBPAGE.getLabel(),
                GuestBookInputFieldMaxSize.WEBPAGE.getSize());
        validateInput(comment, GuestBookInputField.COMMENT.getLabel(),
                GuestBookInputFieldMaxSize.COMMENT.getSize());
    }

    /**
     * Getters for the attributes.
     */
    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getWebpage() {
        return webpage;
    }

    public String getComment() {
        return comment;
    }

    /**
     * Used to validate the values of a specific attributes.
     */
    private void validateInput(String input,
                               String fieldName,
                               int maxSize)
            throws FieldContainHTMLException,
            EmptyGuestBookFieldException,
            SizeNotAllowedException {
        validateNoHTML(input, fieldName);
        validateNoEmptyField(input, fieldName);
        validateSizeField(input, fieldName, maxSize);
    }

    /**
     * Used to validate that attribute do not contain HTML code.
     */
    private void validateNoHTML(String input,
                                String fieldName) throws FieldContainHTMLException {
        if (containsHtml(input)) {
            throw new FieldContainHTMLException(fieldName);
        }
    }

    /**
     * Used to validate that attribute do not contain HTML code.
     */
    private static boolean containsHtml(String input) {
        String htmlRegex = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";

        // check if the input matches the HTML regex
        return input.matches(".*" + htmlRegex + ".*");
    }

    /**
     * Custom exception used to indicate that attribute contain HTML code.
     */
    public static class FieldContainHTMLException extends Exception {
        public FieldContainHTMLException(String fieldName) {
            super("Guest book field \"" + fieldName + "\" can not contain HTML code");
        }
    }

    /**
     * Used to validate that attribute is not empty.
     */
    private void validateNoEmptyField(String input,
                                      String fieldName) throws EmptyGuestBookFieldException {
        if (input.equals("")) {
            throw new EmptyGuestBookFieldException(fieldName);
        }
    }

    /**
     * Custom exception used to indicate that attribute is empty.
     */
    public static class EmptyGuestBookFieldException extends Exception {
        public EmptyGuestBookFieldException(String fieldName) {
            super("Guest book field \"" + fieldName + "\" can not be empty");
        }
    }

    /**
     * Used to validate that attribute is not empty.
     */
    private void validateSizeField(String input,
                                   String fieldName,
                                   int maxSize) throws SizeNotAllowedException {
        if (input.length() > maxSize) {
            throw new SizeNotAllowedException(fieldName, maxSize);
        }
    }

    /**
     * Custom exception used to indicate that attribute is empty.
     */
    public static class SizeNotAllowedException extends Exception {
        public SizeNotAllowedException(String fieldName, int maxSize) {
            super("Guest book field \"" + fieldName + "\"" +
                    " can not contain more then " +
                    maxSize + " characters.");
        }
    }
}

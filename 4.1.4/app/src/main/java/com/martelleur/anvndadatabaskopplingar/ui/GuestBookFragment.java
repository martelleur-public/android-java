package com.martelleur.anvndadatabaskopplingar.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.anvndadatabaskopplingar.R;
import com.martelleur.anvndadatabaskopplingar.databinding.FragmentGuestBookBinding;
import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A fragment used to display guestbooks. The fragment use
 * the adapter GuestBookListAdapter to display the guestbooks items.
 */
public class GuestBookFragment extends Fragment {
    FragmentGuestBookBinding binding;
    GuestBookViewModel guestBookViewModel;
    private GuestBookListAdapter guestBookListAdapter;

    public GuestBookFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static GuestBookFragment newInstance() {
        return new GuestBookFragment();
    }


    /**
     * Bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentGuestBookBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance with factory class, add listeners,
     * and a recycler view used for displaying guestbooks.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GuestBookViewModelFactory factory = new GuestBookViewModelFactory(getContext());
        guestBookViewModel = new ViewModelProvider(requireActivity(), factory).get(GuestBookViewModel.class);
        setupGuestBookRecycler();
        setupNavigateToAddNewGuestBookListener();
        setupGuestBookObserver();
        setUpSearchGuestBookListener();
    }

    /**
     * Used to configure and setup a recycler view that display
     * guestbooks items with an adapter.
     */
    private void setupGuestBookRecycler() {
        List<GuestBook> guestBooks = guestBookViewModel.getAllGuestBooks().getValue();
        guestBookListAdapter = new GuestBookListAdapter(
                R.layout.guest_book_item, guestBooks);
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(guestBookListAdapter);
    }

    /**
     * Used to add click listener to
     * navigate to fragment for adding new guestbook.
     */
    private void setupNavigateToAddNewGuestBookListener() {
        binding.createNewQuestBook.setOnClickListener(
                view -> navigateToAddNewGuestBookListener());
    }

    /**
     * Used to navigate to fragment for adding new guestbook.
     */
    private void navigateToAddNewGuestBookListener() {
        FragmentManager fragmentManager = getParentFragmentManager();
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, AddGuestBookFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Update adapter and recycler when guestbooks are updated.
     */
    private void setupGuestBookObserver() {
        guestBookViewModel.getAllGuestBooks().observe(requireActivity(), guestBooks ->
            guestBookListAdapter.setGuestBooks(guestBooks)
        );
    }

    /**
     * Used to filter guestbooks by name.
     */
    private void setUpSearchGuestBookListener() {
        binding.searchGuestbook.addTextChangedListener(new TextWatcher() {
            private List<GuestBook> filteredGuestBooks = new ArrayList<>();
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int after) {
                List<GuestBook> guestBooks = guestBookViewModel.getAllGuestBooks().getValue();
                if (guestBooks == null) { return; }
                filteredGuestBooks = guestBooks.stream()
                        .filter(guestBook -> nameStartingWith(s.toString(), guestBook.getName()))
                        .collect(Collectors.toList());
            }
            private boolean nameStartingWith(String startValue, String guestBookName) {
                return guestBookName.toLowerCase()
                        .startsWith(startValue.toLowerCase());
            }
            @Override
            public void afterTextChanged(Editable editable) {
                List<GuestBook> guestBooks = guestBookViewModel.getAllGuestBooks().getValue();
                if (guestBooks == null) { return; }
                if (editable.toString().equals("")) {
                    guestBookListAdapter.setGuestBooks(guestBooks);
                } else {
                    guestBookListAdapter.setGuestBooks(filteredGuestBooks);
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not implemented
            }
        });
    }

    /**
     * Used to close open connection for guestbook store
     * and layout binding.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        guestBookViewModel.closeGuestBookStore();
        binding = null;
    }
}
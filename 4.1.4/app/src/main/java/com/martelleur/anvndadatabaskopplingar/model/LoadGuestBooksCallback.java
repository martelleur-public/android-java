package com.martelleur.anvndadatabaskopplingar.model;

import java.util.List;

/**
 * Used to describe callback interface to use when creating a guestbook.
 */
public interface LoadGuestBooksCallback {
    void onComplete(List<GuestBook> primeNumbers);
    void onError(String errMessage);
}

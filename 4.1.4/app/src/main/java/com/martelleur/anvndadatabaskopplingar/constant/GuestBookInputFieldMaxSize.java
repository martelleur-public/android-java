package com.martelleur.anvndadatabaskopplingar.constant;

/**
 * Used to validate size of user inputs.
 */
public enum GuestBookInputFieldMaxSize {
    NAME(100),
    MAIL(320),
    WEBPAGE(2048),
    COMMENT(2048);
    private final int size;

    GuestBookInputFieldMaxSize(int size) {
        this.size = size;
    }
    public int getSize() {
        return size;
    }
}

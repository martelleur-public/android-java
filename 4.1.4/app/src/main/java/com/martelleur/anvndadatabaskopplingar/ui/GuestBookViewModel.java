package com.martelleur.anvndadatabaskopplingar.ui;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvndadatabaskopplingar.model.GuestBook;
import com.martelleur.anvndadatabaskopplingar.model.GuestBookRepository;
import com.martelleur.anvndadatabaskopplingar.model.LoadGuestBooksCallback;

import java.util.Collections;
import java.util.List;

/**
 * View model class used to keep current
 * state of the ui. The class is also used to
 * communicate app functionality with the guest book repository
 * for persistent storage.
 */
public class GuestBookViewModel extends ViewModel {
    private final GuestBookRepository repository;
    private final MutableLiveData<List<GuestBook>> guestBooksLiveData =
            new MutableLiveData<>(Collections.emptyList());

    GuestBookViewModel(GuestBookRepository repository) {
        this.repository = repository;
        this.guestBooksLiveData.setValue(repository.getAllGuestBooks());
    }

    /**
     * Used to create and validate a new GuestBook.
     */
    public GuestBook addGuestBook(String name,
                             String mail,
                             String webpage,
                             String comment)
            throws GuestBook.EmptyGuestBookFieldException,
            GuestBook.FieldContainHTMLException,
            GuestBook.SizeNotAllowedException {
        GuestBook guestBook = new GuestBook(
                name, mail, webpage, comment);
        GuestBook newGuestBook = repository.addGuestBook(guestBook, callback);
        return newGuestBook;
    }

    /**
     * Callback used to update guestbooks livedata.
     */
    private final LoadGuestBooksCallback callback = new LoadGuestBooksCallback() {
        // Callback used to update guestbooks livedata.
        @Override
        public void onComplete(List<GuestBook> guestBooks) {
            guestBooksLiveData.postValue(guestBooks);
        }
        // Callback used for handling loadings error.
        @Override
        public void onError(String errMsg) {
        }
    };

    /**
     * Used to get all the guestbooks.
     */
    public MutableLiveData<List<GuestBook>> getAllGuestBooks() {
        this.guestBooksLiveData.setValue(repository.getAllGuestBooks());
        return this.guestBooksLiveData;
    }

    /**
     * Used to close open connection for guestbook store.
     */
    public void closeGuestBookStore() {
        repository.closeGuestBookStore();
    }
}

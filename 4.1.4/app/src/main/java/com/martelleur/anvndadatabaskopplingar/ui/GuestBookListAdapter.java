package com.martelleur.anvndadatabaskopplingar.ui;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvndadatabaskopplingar.R;
import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.util.List;

/**
 * An adapter used when displaying guest books.
 */
public class GuestBookListAdapter  extends RecyclerView.Adapter<GuestBookListAdapter.ViewHolder> {

    final private int layout;
    private List<GuestBook> guestBooks;

    public GuestBookListAdapter(int layoutId,
                            List<GuestBook> guestBooks) {
        layout = layoutId;
        this.guestBooks = guestBooks;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setGuestBooks(List<GuestBook> guestBooks) {
        this.guestBooks = guestBooks;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return guestBooks == null ? 0 : guestBooks.size();
    }

    /**
     * Inflate layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update guest books items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView guestBookName = holder.guestBookName;
        TextView guestBookMail = holder.guestBookMail;
        TextView guestBookWebpage = holder.guestBookWebpage;
        TextView guestBookComment = holder.guestBookComment;
        ImageView displayGuestBook = holder.displayGuestBook;
        ImageView hideGuestBook = holder.hideGuestBook;
        LinearLayout body = holder.body;
        GuestBook guestBook = guestBooks.get(listPosition);
        guestBookName.setText(guestBook.getName());
        guestBookMail.setText(guestBook.getMail());
        guestBookWebpage.setText(guestBook.getWebpage());
        guestBookComment.setText(guestBook.getComment());
        setupDisplayFullGuestBookListener(hideGuestBook, displayGuestBook, body);
        setupDisplayGuestBookTitleListener(hideGuestBook, displayGuestBook, body);
    }

    /**
     * Used to display full guestbook item.
     */
    private void setupDisplayFullGuestBookListener(ImageView hide,
                                                   ImageView display,
                                                   LinearLayout body) {
        hide.setOnClickListener(view -> {
            view.setVisibility(GONE);
            display.setVisibility(VISIBLE);
            body.setVisibility(VISIBLE);
        });
    }

    /**
     * Used to display only title of a guestbook item.
     */
    private void setupDisplayGuestBookTitleListener(ImageView hide,
                                                    ImageView display,
                                                    LinearLayout body) {
        display.setOnClickListener(view -> {
            view.setVisibility(GONE);
            body.setVisibility(GONE);
            hide.setVisibility(VISIBLE);
        });
    }

    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView guestBookName;
        TextView guestBookMail;
        TextView guestBookWebpage;
        TextView guestBookComment;
        ImageView displayGuestBook;
        ImageView hideGuestBook;
        LinearLayout body;
        ViewHolder(View itemView) {
            super(itemView);
            guestBookName = itemView.findViewById(R.id.guest_book_name);
            guestBookMail = itemView.findViewById(R.id.guest_book_mail);
            guestBookWebpage = itemView.findViewById(R.id.guest_book_webpage);
            guestBookComment = itemView.findViewById(R.id.guest_book_comment);
            displayGuestBook = itemView.findViewById(R.id.view_guest_book);
            hideGuestBook = itemView.findViewById(R.id.hide_guest_book);
            body = itemView.findViewById(R.id.guest_book_body);
        }
    }
}

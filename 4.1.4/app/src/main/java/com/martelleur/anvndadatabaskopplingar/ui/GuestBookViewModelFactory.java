package com.martelleur.anvndadatabaskopplingar.ui;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvndadatabaskopplingar.infrastructure.GuestBookAPI;
import com.martelleur.anvndadatabaskopplingar.infrastructure.GuestBookStore;
import com.martelleur.anvndadatabaskopplingar.model.GuestBookRepository;

/**
 * Used to create a GuestBookViewModel.
 */
public class GuestBookViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final GuestBookRepository repository;

    public GuestBookViewModelFactory(Context context) {
        GuestBookStore apiStore = new GuestBookAPI(context);
        this.repository = new GuestBookRepository(apiStore);
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new GuestBookViewModel(repository);
    }
}

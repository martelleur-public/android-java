package com.martelleur.anvndadatabaskopplingar.constant;

/**
 * Used for labels for UI input field.
 */
public enum GuestBookInputField  {
    NAME("name"),
    MAIL("mail"),
    WEBPAGE("webpage"),
    COMMENT("comment");
    private final String label;

    GuestBookInputField(String label) {
        this.label = label;
    }
    public String getLabel() {
        return label;
    }
}

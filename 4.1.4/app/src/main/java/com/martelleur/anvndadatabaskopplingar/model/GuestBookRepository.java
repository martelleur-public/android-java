package com.martelleur.anvndadatabaskopplingar.model;

import com.martelleur.anvndadatabaskopplingar.infrastructure.GuestBookStore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Repository class used to to acquire and update
 * persistent GuestBooks.
 */
public class GuestBookRepository {
    GuestBookStore store;
    private final ExecutorService executorService
            = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    public GuestBookRepository(GuestBookStore store) {
        this.store = store;
    }

    /**
     * Used to add a new guestbook.
     */
    public GuestBook addGuestBook(GuestBook guestBook, LoadGuestBooksCallback callback) {
        GuestBook newGuestBook = null;
        try {
            Callable<GuestBook> addGuestCallable = () -> store.addGuestBook(guestBook);
            Future<GuestBook> futureAddGuestBook = executorService.submit(addGuestCallable);
            newGuestBook = futureAddGuestBook.get();
            List<GuestBook> guestBooks = getAllGuestBooks();
            callback.onComplete(guestBooks);
        } catch (Exception e) {
            callback.onError(e.getMessage());
        }
        return newGuestBook;
    }

    /**
     * Used to acquire all guestbooks.
     */
    public List<GuestBook> getAllGuestBooks() {
        try {
            Callable<List<GuestBook>> getAllGuestBooksCallable =
                    store::getAllGuestBooks;
            Future<List<GuestBook>> futureGetAllGuestBooks =
                    executorService.submit(getAllGuestBooksCallable);
            return futureGetAllGuestBooks.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    /**
     * Used to close open connection for guestbook store.
     */
    public void closeGuestBookStore() {
        store.close();
    }
}

package com.martelleur.anvndadatabaskopplingar.infrastructure;

import com.martelleur.anvndadatabaskopplingar.model.GuestBook;

import java.util.List;

/**
 * Used to describe an interface for a guest book store.
 * Implemented by {@link GuestBookAPI}.
 */
public interface GuestBookStore {
    List<GuestBook> getAllGuestBooks();
    GuestBook addGuestBook(GuestBook guestBook);
    void close();
}

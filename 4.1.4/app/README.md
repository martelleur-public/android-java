# Använda databaskopplingar

Denna uppgift belyser hur man kopplar sig till en databasserver.

## Implementation 

- __Frontend:__ [Android app](.)
- __Backend:__ [Docker Compose with Python FastAPI and Mysql as DB](./backend/)
    - Verktygskrav: Docker and Docker compose.

## Körning av applikationen

1. __Backend api konfiguration__:
    - Ändra egenskap ```api.url``` in file ```4.1.4/app/src/main/assets/api.properties```
    till systemets IPv4 adress där Backend kommer att startas från.

2. __Använding a klar text http:__
    - Uppdatera ```4.1.4/app/src/main/res/xml/network_security_config.xml``` med den IPv4
    adress där Frontend kommer köras ifrån.

3. __Starta Backend med följande kommandon:__ 
    - Ändra katalog till: ```cd 4.1.4/app/backend/```    
    - Kör Backend startskript med standard värde för sub-nätverk: ```./docker-compose-start-questbook-backend.sh```
    - Kör Backend startskript med specificerat värde för sub-nätverk: ```./docker-compose-start-questbook-backend.sh 10.0.200.0/24```

4. __Starta Android frontend__

5. __Stoppa Backend med följande kommando:__ 
    - ```./docker-compose-destroy-backend.sh```

## Prototyp
Gör en app som implementerar en gästbok där användaren ska kunna fylla i följande information:

- Namn
- Epost
- Hemsida
- Kommentar

Användarens inlägg ska sparas i en databasserver (antingen den MySQL som kör på atlas.dsv.su.se eller i en egen) och alla inlägg som användarna har gjort ska kunna visas i appen.

All text ska kontrolleras så att den inte innehåller HTML-kod och om den gör det så ska texten bytas ut mot exempelvis ordet censur. På så sätt kan inläggen i gästboken även visas på en webbsida om så önskas.

## Funktionalitet

För att kunna lösa denna protyp måste måste man använda en databasserver. Om man vill så kan man (frivilligt) ansluta till ett (egengjort) HTTP-serverprogram (via HTTP-post) och låta det sköta kommunikationen med databasen. Detta kan vara ett stabilare sätt att prata med en databas över de något osäkra mobilanslutningarna.
# Ta och visa kort

Prototypen introducerar till hur man använder kameran för att ta kort samt hur man visar bilden för användaren.

## Prototyp

Gör en app som möjliggör att:

- Användaren kan ta ett kort
- Användaren kan se senast tagna kort

Man kan ta hjälp av den app som redan finns i Android (använda en Intent) eller göra en egen kustomiserad variant (svårare).
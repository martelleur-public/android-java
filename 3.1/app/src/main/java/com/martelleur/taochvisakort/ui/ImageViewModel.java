package com.martelleur.taochvisakort.ui;

import android.graphics.Bitmap;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.taochvisakort.model.ImageRepository;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * View model class used to store images livedata
 * and to communicate app functionality with the repository.
 */
public class ImageViewModel extends ViewModel {
    ImageRepository repository;
    private final MutableLiveData<List<File>> imagesLiveData =
            new MutableLiveData<>(new ArrayList<>());

    public ImageViewModel(ImageRepository repository) {
        this.repository = repository;
        imagesLiveData.setValue(this.repository.getAllImages());
    }

    /**
     * Used to get image livedata.
     */
    public MutableLiveData<List<File>> getImagesLiveData() {
        return imagesLiveData;
    }

    /**
     * Used to set image livedata.
     */
    public void setImagesLiveData() {
        imagesLiveData.setValue(this.repository.getAllImages());
    }

    /**
     * Used to store an image to persistent storage.
     */
    public void storeImage(Bitmap bitmap) {
        String fileIndex = ".jpeg";
        String fileName = getRandomFileName() + fileIndex;
        repository.storeImage(fileName, bitmap);
    }

    /**
     * Used to get a random file name for the image
     * between character 'A' and 'Z'.
     */
    private String getRandomFileName() {
        int minCharacter = 97; // represent character 'A'.
        int maxCharacter = 122; // represent character 'Z'.
        int length = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(length);
        for(int i = 0; i < length; i++) {
            int character = minCharacter + random.nextInt() * (maxCharacter - minCharacter + 1);
            buffer.append(character);
        }
        return buffer.toString();
    }
}

package com.martelleur.taochvisakort.infrastructure;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.core.content.FileProvider;

import com.martelleur.taochvisakort.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Used to store and get image files from persistent data.
 */
public class FileManager extends FileProvider  {
    private Context context;
    private String imageDirName;
    public FileManager() {
        super(R.xml.images);
    }

    public void initialize(Application application,
                           String imageDir) {
        this.context = application;
        this.imageDirName = imageDir;
        createImageDir();
    }

    /**
     * Used to create an directory used for images.
     */
    private void createImageDir() {
        File imageDir = new File(context.getFilesDir(), imageDirName);
        if (!imageDir.exists()) {
            imageDir.mkdirs();
        }
    }

    /**
     * Used to store images to app internal storage.
     */
    public void saveImageToInternalStorage (String fileName, Bitmap bitmap) {
        String TAG = "FileManager";
        try {
            String internalDirPath = context.getFilesDir() + "/" + imageDirName;
            File imageFile = new File(internalDirPath, fileName);
            FileOutputStream outStream = new FileOutputStream(imageFile);

            // Create a compressed bitmap of the image and save the file.
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Used to get all images.
     */
    public List<File> getAllImages() {
        String dirPath = context.getFilesDir() + "/" + imageDirName;
        return Stream.of(new File(dirPath).listFiles())
                .filter(file -> !file.isDirectory())
                .collect(Collectors.toList());
    }

    /**
     * Used to get an uri for a file.
     */
    public Uri getUriForFile(File file) {
        return getUriForFile(context,
                "com.martelleur.taochvisakort.fileprovider",
                file);
    }
}

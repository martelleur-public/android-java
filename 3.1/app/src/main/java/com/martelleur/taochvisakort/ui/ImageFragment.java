package com.martelleur.taochvisakort.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.taochvisakort.R;
import com.martelleur.taochvisakort.databinding.ImageFragmentBinding;

import java.io.File;
import java.util.List;

/**
 * Fragment used for take new pictures and show all pictures
 * taken with this app.
 */
public class ImageFragment extends Fragment {
    private ImageFragmentBinding binding;
    private ImageViewModel imageViewModel;
    private ImageListAdapter imageListAdapter;
    static final String transactionFromImageFragmentToSingleImageFragment
            = "fromImageFragmentToSingleImageFragment";

    public ImageFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static ImageFragment newInstance() {
        return new ImageFragment();
    }

    /**
     * Bind layout using view binding instance
     * ImageFragmentBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = ImageFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance, add a recycler for added images.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageViewModelFactory factory = new ImageViewModelFactory();
        imageViewModel = new ViewModelProvider(requireActivity(), factory).get(ImageViewModel.class);
        setupTakePictureListener();
        setUpImageRecycler(imageViewModel.getImagesLiveData().getValue());
        setupImagesObserver();
    }


    /**
     * Add a listener that launch the activity launcher cameraLauncher.
     */
    private void setupTakePictureListener() {
        binding.takePicture.setOnClickListener(view -> {
            cameraLauncher.launch(getVoid());
        });
    }

    /**
     * ActivityResultLauncher used to launch camera application and handle result,
     * a bitmap representation of the taken image.
     */
    private final ActivityResultLauncher<Void> cameraLauncher  =
            registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), result -> {
                if (result != null) handleBitmapImageResult(result);
            });

    /**
     * A handler used to store image result in livedata
     * and persistent data.
     */
    private void handleBitmapImageResult(Bitmap result) {
        imageViewModel.storeImage(result);
        imageViewModel.setImagesLiveData();
    }

    private Void getVoid() {
        return null;
    }

    /**
     * Set up a recycler view with an adapter
     * that displays taken pictures with this application.
     */
    private void setUpImageRecycler(List<File> images) {
        imageListAdapter = new ImageListAdapter(R.layout.image_item,
                images, imageItemCallback, requireActivity());
        RecyclerView recyclerView = binding.imageRecycler;
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(imageListAdapter);
    }

    /**
     * Callback used to view an image in full screen with SingleImageFragment.
     */
    private final ImageItemCallback imageItemCallback = file -> {
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager
                .beginTransaction()
                .add(R.id.container, SingleImageFragment.newInstance(file.getPath()))
                .addToBackStack(transactionFromImageFragmentToSingleImageFragment)
                .commit();
    };

    /**
     * Add an observer that update the adapter and by so the recycler view
     * when a new photo have been taken and stored.
     */
    private void setupImagesObserver() {
        imageViewModel.getImagesLiveData().observe(requireActivity(), images ->
                imageListAdapter.setImageList(images));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

package com.martelleur.taochvisakort.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Build;
import android.util.Log;

import java.io.File;

/**
 * Utility class used to calculate a scaled down bitmap for
 * a image. The scaled down bitmap image can then for example
 * be used to set an appropriate sized bitmap in an ImageView.
 */
public class ImageUtil {
    /**
     * Get scaled down bitmap image calculated with help
     * from window width and height.
     */
    public static Bitmap getScaledDownBitmap(String imagePath, Activity activity) {
        // Get the dimensions of the stored image and window image
        int[] windowDimensions = getWindowDimensions(activity);
        int windowWidth = windowDimensions[0];
        int windowHeight = windowDimensions[1];
        float[] dimensionStoredImage = getDimensionOfStoredImage(imagePath);
        float storedImageWidth = dimensionStoredImage[0];
        float storedImageHeight = dimensionStoredImage[1];

        // Get scaled down value between stored image dimensions
        // and destination dimensions.
        int scaleDownValue = getScaleDownValue(storedImageHeight,
                storedImageWidth, windowWidth, windowHeight);

        // Get scale downed bitmap from calculate scale down value.
        return getScaledDownBitmap(scaleDownValue, imagePath);
    }

    /**
     * Get window dimensions for width and height.
     */
    private static int[] getWindowDimensions(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            activity.getWindowManager().getCurrentWindowMetrics().getBounds();
        }
        return new int[]{size.x, size.y};
    }

    /**
     * Get the dimensions of the stored bitmap image.
     */
    private static float[] getDimensionOfStoredImage(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        float srcWidth = options.outWidth; // Width stored bitmap image.
        float srcHeight = options.outHeight; // Width stored bitmap image.
        return new float[]{srcWidth, srcHeight};
    }

    /**
     * Calculate how much to scale down the bitmap of the image by
     * comparing source dimensions with destination dimensions.
     * The calculated value will then be used to scale down the bitmap
     * of the image.
     */
    private static int getScaleDownValue(float srcHeight,
                                     float srcWidth,
                                     int destWidth,
                                     int destHeight) {
        int scaleDownValue = 1;
        if (srcHeight > destHeight || srcWidth > destWidth) {
            float heightScale = srcHeight / destHeight; // Get height scale.
            float widthScale = srcWidth / destWidth; // Get width scale.
            scaleDownValue = getMaximumScaleDownValue(heightScale,
                    widthScale);
        }
        String TAG = "ImageUtil";
        Log.d(TAG, "scaleDownValue: " + scaleDownValue);
        return scaleDownValue;
    }

    /**
     * Get maximum scale down value selected between scale
     * value for height and scale value for width.
     */
    private static int getMaximumScaleDownValue(float heightScale, float widthScale) {
        float scaleDownValueFloat;
        if (heightScale > widthScale) scaleDownValueFloat = heightScale;
        else scaleDownValueFloat = widthScale;
        return Math.round(scaleDownValueFloat);
    }


    /**
     * Get scaled down bitmap from a scale down value.
     */
    private static Bitmap getScaledDownBitmap(int scaleDownValue,
                                              String path) {
        // Scale down the image
        BitmapFactory.Options options = new BitmapFactory.Options();
        options = new BitmapFactory.Options();
        options.inSampleSize = scaleDownValue; // set scale down value.

        // Create scale downed bitmap
        return BitmapFactory.decodeFile(path, options);
    }
}

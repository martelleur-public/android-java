package com.martelleur.taochvisakort.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.martelleur.taochvisakort.R;
import com.martelleur.taochvisakort.databinding.SingleImageFragmentBinding;
import com.martelleur.taochvisakort.util.ImageUtil;

/**
 * Fragment used for displaying a selected picture on full screen.
 */
public class SingleImageFragment extends Fragment {
    private static final String ARG_FILE_PATH = "";
    SingleImageFragmentBinding binding;

    public SingleImageFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static SingleImageFragment newInstance(String filePath) {
        SingleImageFragment fragment = new SingleImageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_FILE_PATH, filePath);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Bind layout using view binding
     * SingleImageFragmentBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = SingleImageFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Get view model instance, add a recycler for added images.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            String imagePath = getArguments().getString(ARG_FILE_PATH);
            updateViewWithImage(imagePath);
        }
        setupOnImageListener();
    }

    /**
     * Update this fragment with a scaled image.
     */
    private void updateViewWithImage(String imagePath) {
        Bitmap bitmap = ImageUtil.getScaledDownBitmap(
                imagePath, requireActivity());
        binding.imageView.setImageBitmap(bitmap);
    }

    /**
     * Add a click listener for returning to close this fragment.
     */
    private void setupOnImageListener() {
        binding.imageView.setOnClickListener(view -> {
            FragmentManager fragmentManager = getParentFragmentManager();
            fragmentManager.popBackStack(
                    ImageFragment.transactionFromImageFragmentToSingleImageFragment,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

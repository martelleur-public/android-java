package com.martelleur.taochvisakort.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.taochvisakort.model.ImageRepository;

/**
 * A factory class used for creating an instance of ImageViewModel.
 */
public class ImageViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final ImageRepository repository = ImageRepository.getInstance();

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new ImageViewModel(repository);
    }
}

package com.martelleur.taochvisakort;

import android.app.Application;

import com.martelleur.taochvisakort.model.ImageRepository;

public class ImageApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ImageRepository.initialize(this);
    }
}

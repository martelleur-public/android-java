package com.martelleur.taochvisakort.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * An dialog fragment used to display for the user what
 * permission is necessary for the app to function.
 */
public class PermissionWarningDialogFragment extends DialogFragment {

    public static PermissionWarningDialogFragment newInstance() {
        return new PermissionWarningDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Permission warning")
                .setMessage("The app will not be functional when all permissions are NOT granted.")
                .setPositiveButton("OK", (dialog, id) -> {
                    // User clicked the "OK" button
                });
        return builder.create();
    }
}

package com.martelleur.taochvisakort.model;

import android.app.Application;
import android.graphics.Bitmap;

import com.martelleur.taochvisakort.infrastructure.FileManager;

import java.io.File;
import java.util.List;

/**
 * A repository used to access persistent data
 * for storing and acquiring file images.
 */
public class ImageRepository {

    static ImageRepository INSTANCE;
    private final FileManager fileManager = new FileManager();

    private ImageRepository(Application application) {
        String imageDir = "images";
        fileManager.initialize(application, imageDir);
    }

    /**
     * Used to store a file image.
     */
    public void storeImage(String fileName, Bitmap bitmap) {
        fileManager.saveImageToInternalStorage(fileName, bitmap);
    }

    /**
     * Used to acquire all file images.
     */
    public List<File> getAllImages() {
        return  fileManager.getAllImages();
    }

    /**
     * Used to initialize this repository.
     */
    public static void initialize(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ImageRepository(application);
        }
    }

    /**
     * Used to get a single instance of this repository.
     */
    public static ImageRepository getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("Repository not initialized.");
        }
        return INSTANCE;
    }
}

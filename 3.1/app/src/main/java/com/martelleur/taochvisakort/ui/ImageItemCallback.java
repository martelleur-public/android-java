package com.martelleur.taochvisakort.ui;

import java.io.File;

/**
 * Interface describing methods to be used when an user select a image.
 * The interface is implemented in {@link ImageFragment} and are used in
 * {@link ImageListAdapter}.
 */
public interface ImageItemCallback {
    void onSelect(File file);
}

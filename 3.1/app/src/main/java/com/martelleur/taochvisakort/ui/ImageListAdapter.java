package com.martelleur.taochvisakort.ui;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.taochvisakort.R;
import com.martelleur.taochvisakort.util.ImageUtil;

import java.io.File;
import java.util.List;

/**
 * An adapter used to display images.
 */
public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    final private int imageItemLayout;
    private List<File> images;
    private final ImageItemCallback imageItemCallback;
    private final Activity activity;

    public ImageListAdapter(int layoutId,
                            List<File> images,
                            ImageItemCallback imageItemCallback,
                            Activity activity) {
        imageItemLayout = layoutId;
        this.images = images;
        this.imageItemCallback = imageItemCallback;
        this.activity = activity;
    }

    /**
     * Used to update images.
     */
    public void setImageList(List<File> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of images.
     */
    @Override
    public int getItemCount() {
        return images == null ? 0 : images.size();
    }

    /**
     * Used to set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(imageItemLayout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Used to update image items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        ImageView imageView = holder.imageView;
        updateImageScale(images.get(listPosition), imageView);
        setupSelectImageListener(imageView, images.get(listPosition));
    }

    /**
     * Used to update scale for an image.
     */
    private void updateImageScale(File image, ImageView imageView) {
        Bitmap bitmap = ImageUtil.getScaledDownBitmap(image.getPath(), activity);
        imageView.setImageBitmap(bitmap);
    }

    /**
     * Used to set a listener for when a user select an image.
     * The listener is the calling 'imageItemCallback.onSelect(file)'
     * that is implemented by the caller.
     */
    private void setupSelectImageListener(ImageView imageView, File file) {
        imageView.setOnClickListener(view -> {
            imageItemCallback.onSelect(file);
        });
    }

    /**
     * A view holder that describes item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view_item);
        }
    }
}

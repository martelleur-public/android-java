# Spela upp YouTube-filmer

Prototypen introducerar till hur man spelar upp YouTube-filmer.

## Prototyp

Gör en app där användaren kan välja att klicka på en av flera webblänkar (eller knappar) (till YouTube-filmer) och som vid ett klick spelar upp respektive YouTube-film.

## Funktionalitet

Använd en Intent.
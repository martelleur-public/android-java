package com.martelleur.spelauppyoutube_filmer.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.spelauppyoutube_filmer.R;
import com.martelleur.spelauppyoutube_filmer.databinding.FragmentYoutubeBinding;
import com.martelleur.spelauppyoutube_filmer.model.YoutubeVideo;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Fragment used for displaying links to youtube videos.
 */
public class YoutubeFragment extends Fragment {
    private FragmentYoutubeBinding binding;
    private YoutubeListAdapter youtubeListAdapter;

    /* Static selected youtube videos for testing th application */
    private final ArrayList<YoutubeVideo> youtubeItems = new ArrayList<>(Arrays.asList(
            new YoutubeVideo("ZO5lV0gh5i4", "Bird video 1"),
            new YoutubeVideo("owiwCIhc0I0", "Bird video 2"),
            new YoutubeVideo("a1wp1RnC7kk", "Bird video 3"),
            new YoutubeVideo("ZO5lV0gh5i4", "Bird video 4"),
            new YoutubeVideo("owiwCIhc0I0", "Bird video 5"),
            new YoutubeVideo("a1wp1RnC7kk", "Bird video 6"),
            new YoutubeVideo("ZO5lV0gh5i4", "Bird video 7"),
            new YoutubeVideo("owiwCIhc0I0", "Bird video 8"),
            new YoutubeVideo("a1wp1RnC7kk", "Bird video 9"),
            new YoutubeVideo("ZO5lV0gh5i4", "Bird video 10"),
            new YoutubeVideo("owiwCIhc0I0", "Bird video 11"),
            new YoutubeVideo("a1wp1RnC7kk", "Bird video 12"),
            new YoutubeVideo("ZO5lV0gh5i4", "Bird video 13"),
            new YoutubeVideo("owiwCIhc0I0", "Bird video 14"),
            new YoutubeVideo("a1wp1RnC7kk", "Bird video 15")
    ));

    public YoutubeFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static YoutubeFragment newInstance() {
        return new YoutubeFragment();
    }

    /**
     * Bind layout using view binding instance FragmentYoutubeBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        binding = FragmentYoutubeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Used to setup a recycler for displaying youtube videos
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupYoutubeRecycler();
    }

    /**
     * Set up a recycler view with an adapter
     * that displays youtube items.
     */
    private void setupYoutubeRecycler() {
        youtubeListAdapter = new YoutubeListAdapter(R.layout.youtube_item,
                youtubeItems,
                youtubeItemCallback);
        RecyclerView recyclerView = binding.recycler;
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        }
        recyclerView.setAdapter(youtubeListAdapter);
    }

    /**
     * Callback used to open a youtube link and update play button names.
     */
    private final YoutubeItemCallback youtubeItemCallback = new YoutubeItemCallback() {
        @Override
        public void onSelect(String url) {
            openYoutubeLink(url);
        }

        @Override
        public void updatePlayButtonTextContent(Button playButton, String videoName) {
            playButton.setText(getString(R.string.play_youtube_video, videoName));
        }
    };

    /**
     * Used to open youtube link with an implicit intent.
     */
    private void openYoutubeLink(String youtubeLink) {
        Intent urlIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(youtubeLink));
        startActivity(urlIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

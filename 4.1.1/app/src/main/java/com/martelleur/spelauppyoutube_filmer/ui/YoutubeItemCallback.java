package com.martelleur.spelauppyoutube_filmer.ui;

import android.widget.Button;

/**
 * A callback interface used for communicating between
 * fragment {@link YoutubeFragment} and adapter {@link YoutubeListAdapter}.
 */
public interface YoutubeItemCallback {
    void onSelect(String url);
    void updatePlayButtonTextContent(Button playButton, String videoName);
}

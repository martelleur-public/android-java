package com.martelleur.spelauppyoutube_filmer;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.martelleur.spelauppyoutube_filmer.ui.YoutubeFragment;


/**
 * Launch activity for the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Used to launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, YoutubeFragment.newInstance())
                    .commitNow();
        }
    }
}
package com.martelleur.spelauppyoutube_filmer.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.spelauppyoutube_filmer.R;
import com.martelleur.spelauppyoutube_filmer.model.YoutubeVideo;

import java.util.List;

/**
 * An adapter used to when displaying youtube items.
 */
public class YoutubeListAdapter extends RecyclerView.Adapter<YoutubeListAdapter.ViewHolder> {
    final private int layout;
    private final List<YoutubeVideo> youtubeItems;
    private final YoutubeItemCallback youtubeItemCallback;

    public YoutubeListAdapter(int layoutId,
                              List<YoutubeVideo> youtubeItems,
                              YoutubeItemCallback youtubeItemCallback) {
        layout = layoutId;
        this.youtubeItems = youtubeItems;
        this.youtubeItemCallback = youtubeItemCallback;
    }

    /**
     * Used to get the number of youtube items.
     */
    @Override
    public int getItemCount() {
        return youtubeItems == null ? 0 : youtubeItems.size();
    }

    /**
     * Used to set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update youtube item views
     * at the given position.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        WebView youtubeWebView = holder.youtubeWebView;
        Button playButton = holder.playButton;
        YoutubeVideo youtubeItem = youtubeItems.get(listPosition);
        setupWebView(youtubeWebView, youtubeItem.getVideoThumbnailUrl());
        setupPlayButtonTextContent(playButton, youtubeItem.getName());
        setupPlayButtonListener(playButton, youtubeItem.getVideoUrl());
    }

    /**
     * Used to set up web view with provided link.
     */
    private void setupWebView(WebView webView, String link) {
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        webView.loadUrl(link);
    }

    /**
     * Update text content for play button by communicate back to caller.
     */
    private void setupPlayButtonTextContent(Button playButton, String videoName) {
        youtubeItemCallback.updatePlayButtonTextContent(playButton, videoName);
    }

    /**
     * Add listeners to play button that communicate back
     * to the caller that a youtube item have been selected.
     */
    private void setupPlayButtonListener(Button playButton, String url) {
        playButton.setOnClickListener(view -> {
            youtubeItemCallback.onSelect(url);
        });
    }

    /**
     * A ViewHolder which hold layout for a youtube item.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        WebView youtubeWebView;
        Button playButton;
        ViewHolder(View itemView) {
            super(itemView);
            youtubeWebView = itemView.findViewById(R.id.youtube_video_thumbnail);
            playButton = itemView.findViewById(R.id.play_video);
        }
    }
}

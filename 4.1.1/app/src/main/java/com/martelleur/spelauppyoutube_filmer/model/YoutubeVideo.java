package com.martelleur.spelauppyoutube_filmer.model;

/**
 * A class representing a youtube video.
 */
public class YoutubeVideo {
    private final String videUrl;
    private final String videoThumbnail;
    private final String name;

    public YoutubeVideo (String id , String name) {
        this.videUrl = "https://www.youtube.com/embed/" + id;
        this.videoThumbnail = "https://i.ytimg.com/vi/" + id + "/hqdefault.jpg";
        this.name = name;
    }

    /**
     * Used to get the name of this video.
     */
    public String getName() {
        return name;
    }

    /**
     * Used to get the url of this video.
     */
    public String getVideoUrl() {
        return videUrl;
    }

    /**
     * Used to get the thumbnail url of this video.
     */
    public String getVideoThumbnailUrl() { return videoThumbnail; }
}

# Java Android Prototypes

Små Android applikationer skrivna i Java som kan användas som prototyper för att bygga något större. Repot innehåller också Java applikationer, e.g., en simpel server, som bland annat kan användas för att testa Android applikationerna lokalt.

## WiFi Direct Chat App

- __[WiFi Direct Chat App](Gesällprov/app/Joel_Martelleur_Gesällprov_MPROG.pdf)__

## 1. Bygga, Paketera och Signera en app

- __[Lägga ut komponenter samt Koppla till händelsehantering](1.2/app/README.md)__

- __[Paketera och signera en android app lokalt](1.3/README.md)__

## 2. Lokal datalagring

- __[Spara till fil](2.1/app/README.md)__

- __[Använda en SQLite databas](2.2/app/README.md)__

## 3. Multimedia

- __[Ta och visa kort](3.1/app/README.md)__ 

- __[Spela in och upp ljud](3.2/app/README.md)__

- __[Spela in och visa film](3.3/app/README.md)__

## 4. Nätverk

- __[Spela upp YouTube filmer](4.1.1/app/README.md)__

- __[Öpnna webbsidor](4.1.2/app/README.md)__

- __[Sända och ta emot epost](4.1.3/app/README.md)__

- __[Använda databaskopplingar](4.1.4/app/README.md)__

- __[Använda stream sockets](4.2.1/app/README.md)__

- __[Använda datagram sockets](4.2.2/app/README.md)__

## 6. Telefoni

- __[Ringa och ta emot samtal](6.1/app/README.md)__

- __[Ta emot sms](6.2/receive_sms/app/README.md)__

- __[Sända sms](6.2/send_sms/app/README.md)__

## 7. Systemfunktioner

- __[Använda copy and paste](7.1.1/app/README.md)__

- __[Använda notifikationer](7.2.1/app/README.md)__

- __[Använda vibrationer](7.2.2/app/README.md)__

- __[Få tag på anslutningsinformation](7.3.1/app/README.md)__

- __[Använda WiFi P2P](7.3.3/AnvandaWiFiP2P/app/README.md)__

## Java applikationer

- __[java_apps_src/README.md](java_apps_src/README.md)__

- __[java_apps_binary/README.md](java_apps_binary/README.md)__
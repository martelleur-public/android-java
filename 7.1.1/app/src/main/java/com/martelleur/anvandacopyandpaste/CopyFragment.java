package com.martelleur.anvandacopyandpaste;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.martelleur.anvandacopyandpaste.databinding.FragmentCopyBinding;

/**
 * A fragment used for copying text.
 */
public class CopyFragment extends Fragment {
    private FragmentCopyBinding binding;

    /**
     * Used to create an instance of this fragment.
     */
    public static CopyFragment newInstance() {
        return new CopyFragment();
    }

    /**
     * Used to bind layout using view binding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentCopyBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created setup navigation listener that
     * copy text before navigating to paste fragment.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupNavigateToPasteFragmentListener();
    }

    /**
     * Used to setup navigation to paste fragment listener.
     */
    private void setupNavigateToPasteFragmentListener() {
        binding.navigateToPasteFragment.setOnClickListener(view -> {
            String textContent =
                    (binding.copyContent.getText() != null) &&
                    !(binding.copyContent.getText().toString().equals("")) ?
                    binding.copyContent.getText().toString() :
                    getText(R.string.forgot_message).toString();
            copyTextToClipboard(textContent);
            navigateToPasteFragment();
        });
    }

    /**
     * Used to copy text to clipboard using a ClipboardManager
     */
    private void copyTextToClipboard(String textContent) {
        ClipboardManager clipboardManager = (ClipboardManager)
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("User text input", textContent);
        clipboardManager.setPrimaryClip(clipData);
        notifyCopyToUser();
    }

    /**
     * Notify copy to the user. This is only for Android 12 (API level 32)
     * and lower, if higher version this is done by the system.
     */
    private void notifyCopyToUser() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            Toast.makeText(requireContext(),
                    getText(R.string.text_copied_msg),
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Used to navigate to paste fragment.
     */
    private void navigateToPasteFragment() {
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, PasteFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

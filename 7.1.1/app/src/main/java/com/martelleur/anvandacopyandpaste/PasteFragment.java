package com.martelleur.anvandacopyandpaste;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.martelleur.anvandacopyandpaste.databinding.FragmentPasteBinding;

/**
 * A fragment used for pasting text.
 */
public class PasteFragment extends Fragment {
    private FragmentPasteBinding binding;

    /**
     * Used to create an instance of this fragment.
     */
    public static PasteFragment newInstance() {
        return new PasteFragment();
    }

    /**
     * Used to bind layout using view binding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPasteBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created setup
     * 1) Paste listener.
     * 2) Navigation listener.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupPasteListener();
        setupNavigateToCopyFragmentListener();
    }

    /**
     * Used to setup a paste listener.
     */
    private void setupPasteListener() {
        binding.pasteTextContent.setOnClickListener(view -> pasteTextFromClipboard());
    }

    /**
     * Used to paste clipboard text content to layout using a ClipboardManager.
     */
    private void pasteTextFromClipboard() {
        ClipboardManager clipboardManager =
                (ClipboardManager) requireActivity().getSystemService(Context.CLIPBOARD_SERVICE);

        // Enable paste option if clipboard contain text data
        binding.pasteContent.setEnabled(isClipboardContainingTextData(clipboardManager));

        // Paste clipboard text content.
        ClipData clipData = clipboardManager.getPrimaryClip();
        String textContent = clipData.getItemAt(0).getText().toString();
        binding.pasteContent.setText(textContent);
    }

    /**
     * Check if clipboard contain text data.
     */
    private boolean isClipboardContainingTextData(ClipboardManager clipboardManager) {
        if (!clipboardManager.hasPrimaryClip()) {
            return false;
        } else return clipboardManager
                .getPrimaryClipDescription()
                .hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN);
    }

    /**
     * Used to setup navigation to copy fragment listener.
     */
    private void setupNavigateToCopyFragmentListener() {
        binding.navigateToCopyFragment.setOnClickListener(view -> navigateToCopyFragment());
    }

    /**
     * Used to navigate to copy fragment.
     */
    private void navigateToCopyFragment() {
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager.popBackStack();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

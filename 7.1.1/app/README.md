# Använda copy and paste

Prototypen introducerar till hur man använder copy and paste.

## Prototyp

Gör en app som använder copy and paste.

## Beskrivning implementation

Applikationen använder en fragment ```CopyFragment``` för att kopiera
text till _clipboard_ och en fragment ```PasteFragment``` för att klistra 
in texten från _clipboard_.
import java.util.*;
import java.rmi.*;

public interface RemoteClient extends Remote {
  public void showServerClients(String message) throws RemoteException;
}
# 3.3.3 RMI callbacks

Uppgiften belyser hur man använder callbacks med ```Java Remote Method Invocation (RMI)```.

## Uppgift

Gör om RMI-programmen i uppgifterna 3.3.1 RMI på klientsidan och 3.3.2 RMI på serversidan så att de använder callbacks. När en RMI-klient ansluter eller avlsutar sin uppkoppling till RMI-servern så ska RMI-servern informera alla anslutna klienter om vilka klienter som är anslutna och RMI-klienten ska skriva ut denna information i kommandofönstret.

## Exempel 1

Hämta filerna:

- ```RemoteClient.class```
- ```Client.class```
- ```ClientGUI.class```
- ```ClientGUI$L.class```
- ```RemoteServer.class```
- ```Server_Stub.class```

Kör igång RMI-klientprogrammet med:

```java Client atlas.dsv.su.se```

## Exempel 2

I detta program kan man även trycka på e-knappen för att avsluta programmet. Eftersom detta program enbart fungerar på datorer med direkt anslutning till Internet (det fungerar inte bakom routrar som ger klienterna egna interna IP-adresser) så kan man även testa det lokalt på den egna datorn om man först startar upp RMI-servern. Hämta filerna:

- ```RemoteServer.class```
- ```Server.class```
- ```Server$Ball.class```
- ```Server$ClientSender.class```
- ```RemoteClient.class```
- ```Client_Stub.class```

Kör igång RMI-serverprogrammet med:

- ```java Server run_local```

Kör igång RMI-klientprogrammet med:

- ```java Client localhost```

Bygg gärna en version som fungerar bättre än denna.

## Tips

Man bör börja med ett minimalt exempel innan man bygger hela denna uppgift. Exempelvis så kan man börja med att låta RMI-servern anropa en metod hos RMI-klienten med en String som argument som sen RMI-klienten skriver ut i kommandofönstret.

I exemplet ovan har följande två interface använts:

- ```RemoteServer.java```
- ```RemoteClient.java```

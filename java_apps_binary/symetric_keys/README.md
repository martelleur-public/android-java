## Encryption symmetric keys:

- Create key: ```java KeyHandler <secretKey>```

- Encrypt: ```java EncryptHandler data <secretKey> <encryptedData>```

- Decrypt: ```java DecryptHandler <encryptedData> <secretKey> <decryptetData>```

# 3.3.2 RMI på serversidan

Uppgiften belyser distribuerade processer med ```Java Remote Method Invocation (RMI)``` på serversidan.

## Uppgift

Gör RMI-serversidesprogrammet som passar till uppgiften 3.3.1 RMI på klientsidan.

Man behöver bara testa sitt RMI-klientprogram och RMI-serverprogram lokalt på den egna maskinen men det är extra kul om man kör på flera olika maskiner.

De som valt att implementera något annat eller Mandelbrots mängd har redan gjort denna uppgift och kan gå vidare till nästa uppgift.

## Exempel 1

Hämta filerna:

- ```RemoteServer.class```
- ```Server.class```
- ```Server$Ball.class```

Kör igång RMI-serverprogrammet med:

- ```java Server```

## Exempel 2

Ett annat exempel är ett RMI-serverprogram för Mandelbrots mängd. Hämta filerna:

- ```RemoteServer.class```
- ```Server.class```

Kör igång RMI-serverprogrammet med:

- ```java Server```


## Tips

Istället för att starta ett ```rmiregistry``` manuellt, innan man startar sitt RMI-serverprogram, så kan man starta ett ```rmiregistry``` direkt i Java-koden med:

```LocateRegistry.createRegistry(1099);```

Om man redan har gjort det i en annan process så kan man istället hitta detta rmiregistry med:

```Registry registry = LocateRegistry.getRegistry(1099);```

Man kan även ha flera rmiregistry igång på samma maskin om man väljer olika portnummer för dessa.

Angående hanteringen av bollar så kan servern ha alla bollar i en instansvariabel balls:

```private Vector balls = new Vector();```

Sedan har servern metoder för att lägga till, ta bort, pausa och returnera bollarna:

```
public synchronized void addBall() throws RemoteException {
  // ...
}

private synchronized void removeBall(Ball ball) {
  // ...
}

public synchronized void pauseBalls() throws RemoteException {
  // ...
}

public synchronized Vector getBalls() throws RemoteException {
  Vector ballsRaw = new Vector();
  for(int i = 0; i < balls.size(); i++) {
    Ball ball = (Ball)balls.elementAt(i);
    ballsRaw.add(new Integer(ball.x));
    ballsRaw.add(new Integer(ball.y));
    ballsRaw.add(new Integer(ball.r));
  }

  return ballsRaw;
}
```

Varje boll kan exempelvis vara av klassen Ball implementerad enligt följande:

```
class Ball extends Thread {
  private boolean alive = true, active = true;
  private Random rnd = new Random();
  public int rMax = 70, r = rnd.nextInt(rMax), x = xLim / 2 - r / 2, y = yLim / 2 - r / 2, dx, dy, dMax = 5, ddx = 1, ddy = 1;

  Ball() {
    if(rnd.nextInt(2) == 1) ddx = -1;
    dx = (1 + rnd.nextInt(dMax)) * ddx;
    if(rnd.nextInt(2) == 1) ddy = -1;
    dy = (1 + rnd.nextInt(dMax)) * ddy;

    start();
  }

  public void run() {
    while(alive) {
      while(active) {
        if((x <= 0) || (x >= xLim - r)) { dx *= -1; r -= 2; }
        if((y <= 30) || (y >= yLim - r)) { dy *= -1; r -= 3; }

        if(r <= 0) {
          active = false;
          alive = false;
          removeBall(this);
        } else {
          x += dx;
          y += dy;
        }
        try { sleep(10 + r); } catch(Exception e) {}
      }
      try { sleep(50); } catch(Exception e) {}
    }
  }
}
```
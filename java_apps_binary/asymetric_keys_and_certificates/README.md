# Asymetric key verification with certificates.

- Create keypair with the tool ```keytool```

- Sign file with key from keystore: ```java SignHandler keystore abcdef pierre fedcba data <certificate> <signature>```

- Verify file: ```java VerifyHandler data <certificate> <signature>```

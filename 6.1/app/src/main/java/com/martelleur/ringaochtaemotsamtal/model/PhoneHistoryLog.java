package com.martelleur.ringaochtaemotsamtal.model;

import android.content.Context;
import android.database.Cursor;
import android.provider.CallLog;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing phone history log.
 */
public class PhoneHistoryLog {

    /**
     * Used to iterate over phone book history data and
     * add phone call items to a list. This is done by using a
     * Cursor that provides read-write access to the result set
     * returned by a database query, in this case a query to
     * CallLog.Calls.CONTENT_URI.
     */
    public List<PhoneCallItem> getHistory(Context context, String defaultCaller) {
        List<PhoneCallItem> history = new ArrayList<>();
        Cursor cursor = context.getContentResolver()
                .query(CallLog.Calls.CONTENT_URI,
                        null,
                        null,
                        null,
                        CallLog.Calls.DATE + " DESC");

        if (cursor != null) {
            while (cursor.moveToNext()) {
                addPhoneItem(cursor, history, defaultCaller);
            }
            cursor.close();
        }
        return history;
    }

    /**
     * Add a phone call item to a list.
     */
    private void addPhoneItem(Cursor cursor,
                              List<PhoneCallItem> history,
                              String defaultCaller) {

        int numberColumnIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int nameColumnIndex = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        int typeColumnIndex = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int dateColumnIndex = cursor.getColumnIndex(CallLog.Calls.DATE);

        String number = numberColumnIndex >= 0 ?
                cursor.getString(numberColumnIndex) : null;
        String name = nameColumnIndex >= 0 ?
                cursor.getString(nameColumnIndex) : null;
        String type = numberColumnIndex >= 0 ?
                cursor.getString(typeColumnIndex) : null;
        Long date = nameColumnIndex >= 0 ?
                Long.valueOf(cursor.getString(dateColumnIndex)) : null;

        if (name == null) name = defaultCaller;
        if (number != null &&
                type != null
                && date != null) {
            history.add(new PhoneCallItem(number,
                    name, processCallType(Integer.parseInt(type)), date));
        }
    }

    /**
     * Used to parse the call type.
     */
    private String processCallType(int callType) {
        String callTypeString;
        switch (callType) {
            case CallLog.Calls.INCOMING_TYPE:
                callTypeString = "incoming call";
                break;
            case CallLog.Calls.OUTGOING_TYPE:
                callTypeString = "outgoing call";
                break;
            case CallLog.Calls.MISSED_TYPE:
                callTypeString = "missed call";
                break;
            case CallLog.Calls.VOICEMAIL_TYPE:
                callTypeString = "voicemail call";
                break;
            case CallLog.Calls.REJECTED_TYPE:
                callTypeString = "rejected call";
                break;
            case CallLog.Calls.BLOCKED_TYPE:
                callTypeString = "blocked call";
                break;
            case CallLog.Calls.ANSWERED_EXTERNALLY_TYPE:
                callTypeString = "externally call";
                break;
            default:
                callTypeString = "";
        }
        return callTypeString;
    }
}

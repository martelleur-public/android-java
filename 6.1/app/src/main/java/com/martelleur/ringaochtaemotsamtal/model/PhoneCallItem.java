package com.martelleur.ringaochtaemotsamtal.model;

/**
 * A model class representing a phone call item
 * with number, name, type, and date.
 */
public class PhoneCallItem {
    private final String number;
    private final String name;
    private final String type;
    private final Long date;

    public PhoneCallItem(String number, String name, String type, Long date) {
        this.number = number;
        this.name = name;
        this.type = type;
        this.date = date;
    }

    /**
     * Used to get the number.
     */
    public String getNumber() {
        return number;
    }

    /**
     * Used to get the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Used to get the type.
     */
    public String getType() {
        return type;
    }

    /**
     * Used to get the date.
     */
    public Long getDate() {
        return date;
    }
}

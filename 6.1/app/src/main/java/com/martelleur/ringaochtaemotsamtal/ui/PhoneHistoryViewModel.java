package com.martelleur.ringaochtaemotsamtal.ui;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.martelleur.ringaochtaemotsamtal.model.PhoneHistoryLog;
import com.martelleur.ringaochtaemotsamtal.model.PhoneCallItem;
import com.martelleur.ringaochtaemotsamtal.model.PhoneHistoryRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * A view model storing state for the UI.
 */
public class PhoneHistoryViewModel extends ViewModel {
    private final MutableLiveData<List<PhoneCallItem>> phoneCallItemsLiveData =
            new MutableLiveData<>(new ArrayList<>());
    private final PhoneHistoryRepository repository;

    public PhoneHistoryViewModel(PhoneHistoryRepository repository) {
        this.repository = repository;
    }

    /**
     * Used to update phone call items livedata.
     */
    public void updatePhoneCallItems(Context context, String defaultCaller) {
        List<PhoneCallItem> phoneCallItems = repository.getPhoneCallItems(context, defaultCaller);
        phoneCallItemsLiveData.setValue(phoneCallItems);
    }

    /**
     * Used to get phone call items livedata.
     */
    public MutableLiveData<List<PhoneCallItem>> getPhoneCallItemsLiveData() {
        return phoneCallItemsLiveData;
    }

    /**
     * Used to create an instance of this view model.
     * Ref: https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1.
     */
    static final ViewModelInitializer<PhoneHistoryViewModel> initializer = new ViewModelInitializer<>(
            PhoneHistoryViewModel.class,
            creationExtras -> {
                PhoneHistoryLog phoneHistoryLog = new PhoneHistoryLog();
                PhoneHistoryRepository repository = new PhoneHistoryRepository(phoneHistoryLog);
                return new PhoneHistoryViewModel(repository);
            }
    );
}

package com.martelleur.ringaochtaemotsamtal.model;

import android.content.Context;

import java.util.List;

/**
 * Used as an abstraction for getting phone log history.
 */
public class PhoneHistoryRepository {
    private final PhoneHistoryLog phoneHistoryLog;

    public PhoneHistoryRepository(PhoneHistoryLog phoneHistoryLog) {
        this.phoneHistoryLog = phoneHistoryLog;
    }

    public List<PhoneCallItem> getPhoneCallItems(Context context, String defaultCaller) {
        return phoneHistoryLog.getHistory(context, defaultCaller);
    }
}

package com.martelleur.ringaochtaemotsamtal.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.ringaochtaemotsamtal.R;
import com.martelleur.ringaochtaemotsamtal.databinding.FragmentPhoneHistoryBinding;
import com.martelleur.ringaochtaemotsamtal.model.PhoneCallItem;

import java.util.List;

/**
 * A fragment used to display phone call history
 * from the phones phone history log. A user
 * can also call the listed phone numbers by
 * clicking on the the specific item the user
 * want to call.
 */
public class PhoneHistoryFragment
        extends Fragment {
    private FragmentPhoneHistoryBinding binding;
    private BroadcastReceiver phoneCallReceiver;
    private PhoneHistoryAdapter phoneHistoryAdapter;
    private PhoneHistoryViewModel phoneHistoryViewModel;


    public PhoneHistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static PhoneHistoryFragment newInstance() {
        return new PhoneHistoryFragment();
    }

    /**
     * Used to bind layout using view binding, setup view model,
     * and a broadcast receiver for listening on incoming calls.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        phoneHistoryViewModel = getPhoneHistoryViewModel();
        updatePhoneCallItems();
        phoneCallReceiver = getPhoneCallReceiver();
        binding = FragmentPhoneHistoryBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    /**
     * Used to get the view model instance.
     */
    private PhoneHistoryViewModel getPhoneHistoryViewModel() {
        return new ViewModelProvider(this,
                ViewModelProvider.Factory.from(PhoneHistoryViewModel.initializer))
                .get(PhoneHistoryViewModel.class);
    }

    /**
     * Used for getting a context-registered broadcast receiver
     * that listen on state of phone get idle (call is ended)
     * and then update the phone history log.
     */
    private BroadcastReceiver getPhoneCallReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                String validAction = TelephonyManager.ACTION_PHONE_STATE_CHANGED; // "android.intent.action.PHONE_STATE"
                if (action == null || !action.equals(validAction)) {
                    return;
                }

                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                if (state != null && state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    updatePhoneCallItems(); // Update call items when call ends
                }
            }
        };
    }

    /**
     * Update data from cal log history with 1 seconds delay
     * so the phone history log get time to update.
     */
    private void updatePhoneCallItems() {
        final long delayMillis = 1000;

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            String defaultCaller = getString(R.string.unknown_caller);
            phoneHistoryViewModel.updatePhoneCallItems(requireContext(), defaultCaller);
        }, delayMillis);
    }

    /**
     * When view is created setup:
     * 1) Register context-registered broadcast receiver,
     * 2) Recycle view for displaying phone history items,
     * 3) Observer that updates the recycle view adapter when
     *    phone history change.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        requireContext().registerReceiver(phoneCallReceiver, new IntentFilter("android.intent.action.PHONE_STATE"));
        setupPhoneHistoryRecycler();
        setupPhoneCallHistoryObserver();
    }

    /**
     * Used to configure and setup a recycler view that display
     * phone history call items with an adapter.
     */
    private void setupPhoneHistoryRecycler() {
        List<PhoneCallItem> phoneCallItems = phoneHistoryViewModel.getPhoneCallItemsLiveData().getValue();
        phoneHistoryAdapter = new PhoneHistoryAdapter(
                R.layout.phone_call_item,
                phoneCallItems,
                phoneHistoryCallback);
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(phoneHistoryAdapter);
    }

    /**
     * Callback used to initiate a phone call.
     */
    private final PhoneHistoryCallback phoneHistoryCallback = phoneNumber -> {
        String uriScheme = "tel";
        String uriString = uriScheme + ":" +phoneNumber;
        Uri phoneUri = Uri.parse(uriString);
        Intent callIntent = new Intent(Intent.ACTION_CALL, phoneUri);
        if (callIntent.resolveActivity(requireActivity().getPackageManager()) != null) {
            startActivity(callIntent);
        }
    };

    /**
     * Used to set up an observer that updates the recycle view
     * adapter when data from phone history change.
     */
    private void setupPhoneCallHistoryObserver() {
        phoneHistoryViewModel.getPhoneCallItemsLiveData().observe(
                        getViewLifecycleOwner(),
                        phoneCallItems -> phoneHistoryAdapter.setGuestBooks(phoneCallItems));
    }

    /**
     * Used to register broadcast receiver for incoming calls
     * and update data from phone history.
     */
    @Override
    public void onResume() {
        super.onResume();
        requireContext().registerReceiver(
                phoneCallReceiver,
                new IntentFilter("android.intent.action.PHONE_STATE"));
        updatePhoneCallItems();
    }

    /**
     * Used to unregister receiver for incoming calls.
     */
    @Override
    public void onPause() {
        super.onPause();
        requireContext().unregisterReceiver(phoneCallReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

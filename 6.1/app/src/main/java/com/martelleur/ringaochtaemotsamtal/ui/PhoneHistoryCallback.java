package com.martelleur.ringaochtaemotsamtal.ui;

/**
 * A callback interface used to communicate between
 * {@link PhoneHistoryFragment} and {@link PhoneHistoryAdapter}.
 */
public interface PhoneHistoryCallback {
    void initiateCall(String phoneNumber);
}

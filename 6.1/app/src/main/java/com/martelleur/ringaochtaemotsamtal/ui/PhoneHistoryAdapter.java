package com.martelleur.ringaochtaemotsamtal.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.ringaochtaemotsamtal.R;
import com.martelleur.ringaochtaemotsamtal.model.PhoneCallItem;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * An adapter used for displaying phone call history items.
 */
public class PhoneHistoryAdapter extends RecyclerView.Adapter<PhoneHistoryAdapter.ViewHolder> {
    private final int layout;
    private List<PhoneCallItem> phoneCallItems;
    private final PhoneHistoryCallback phoneHistoryCallback;

    public PhoneHistoryAdapter(int layoutId,
                               List<PhoneCallItem> phoneCallItems,
                               PhoneHistoryCallback phoneHistoryCallback) {
        this.layout = layoutId;
        this.phoneCallItems = phoneCallItems;
        this.phoneHistoryCallback = phoneHistoryCallback;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setGuestBooks(List<PhoneCallItem> phoneCallItems) {
        this.phoneCallItems = phoneCallItems;
        notifyDataSetChanged();
    }

    /**
     * Used to get the number of phone call items.
     */
    @Override
    public int getItemCount() {
        return phoneCallItems == null ? 0 : phoneCallItems.size();
    }

    /**
     * Set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update phone history items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView callerView = holder.callerView;
        TextView numberView = holder.numberView;
        TextView dateView = holder.dateView;
        ImageView phoneView = holder.phoneView;
        PhoneCallItem phoneCallItem = phoneCallItems.get(listPosition);
        String nameWithTitle = phoneCallItem.getName() + " " + phoneCallItem.getType();
        callerView.setText(nameWithTitle);
        numberView.setText(phoneCallItem.getNumber());
        dateView.setText(getFormattedLocalDate(phoneCallItem.getDate()));
        setupInitiateCallListener(phoneView, phoneCallItem.getNumber());
    }

    /**
     * Used to get a local formatted date that is used as a
     * property for the phone history items.
     */
    private String getFormattedLocalDate(long epochTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        Date date = new Date(epochTime);
        return formatter.format(date);
    }

    /**
     * Used to initiate a phone call.
     */
    private void setupInitiateCallListener(ImageView phone, String number) {
        phone.setOnClickListener(view ->
                phoneHistoryCallback.initiateCall(number)
        );
    }

    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView callerView;
        TextView numberView;
        TextView dateView;
        ImageView phoneView;
        ViewHolder(View itemView) {
            super(itemView);
            callerView = itemView.findViewById(R.id.caller);
            numberView = itemView.findViewById(R.id.number);
            dateView = itemView.findViewById(R.id.date);
            phoneView = itemView.findViewById(R.id.phone);
        }
    }
}

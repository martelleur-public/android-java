# Ringa och ta emot samtal

## Protyp

Gör en app som visar samtalshistoriken med möjlighet att ringa upp ett nummer i denna.

## Funktionalitet

Rent allmänt kan det vara bra att känna till att det går att lyssna på händelser för att 
ta emot samtal men att det alltid är den inbyggda telefonapplikationen som används.

Notera att vi inte gör någon app som tar emot samtal eftersom android.permission.MODIFY_PHONE_STATE inte längre är tillåtet efter Android 2.3.


# Sända och ta emot sms

## Användning av programmet med emulatorer

1. Starta [Send sms app](6.2/send_sms/app/src/main) 

2. Starta [Receive sms app](6.2/receive_sms/app/src/main)

3. Send sms using emulator portnumber, e.g. the last numbers of ```adb devices -l```

## Prototyp

- Gör först en app som kan sända SMS.

- Gör sedan en annan app som kan ta emot SMS.

## Funktionalitet

Tips Gör en BroadcastReciver som lyssnar på android.provider.Telephony.SMS_RECEIVED.
Öppna två emulatorer och sänd SMS:et från den ena till den andra.


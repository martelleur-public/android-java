package com.martelleur.sendsms.model;

import android.content.ContentResolver;
import android.net.Uri;

import com.martelleur.sendsms.infrastructure.ContactStore;

import java.util.HashMap;

/**
 * An repository used to get persistent contact data
 * relevant for the application. Specifically name
 * and phone number for contacts.
 */
public class SmsRepository {

    private final ContactStore contactStore;

    public SmsRepository(ContactStore contactStore) {
        this.contactStore = contactStore;
    }

    /**
     * Used to update a sms object from contacts store.
     */
    public void updateSmsObject(Uri contactUri, ContentResolver contentResolver, SmsObject smsObject) {
        HashMap<String, String> contactMap = contactStore.getContactMap(contactUri, contentResolver);
        smsObject.setReceiverName(contactMap.get(ContactStore.KEY_RECEIVER_NAME));
        smsObject.setPhoneNumber(contactMap.get(ContactStore.KEY_PHONE_NUMBER));
    }
}

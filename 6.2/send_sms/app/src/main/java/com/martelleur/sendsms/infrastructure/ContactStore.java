package com.martelleur.sendsms.infrastructure;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.HashMap;

/**
 * A store used to get persistent contacts data
 * from the phones contact database.
 */
public class ContactStore {
    public static final String KEY_RECEIVER_NAME = "receiver_name";
    public static final String KEY_PHONE_NUMBER = "phone_number";

    /**
     * Used to get a map with contact info, specifically
     * receiver name and phone number.
     */
    public HashMap<String, String> getContactMap(Uri contactUri, ContentResolver contentResolver) {
        // Create a default map.
        HashMap<String, String> contactMap = new HashMap<>();
        contactMap.put(KEY_RECEIVER_NAME, "");
        contactMap.put(KEY_PHONE_NUMBER, "");

        // Create a projections filter
        String[] projectionFilter = {
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME};

        // Use a cursor for reading columns from the contact database.
        Cursor cursor = contentResolver
                .query(contactUri, projectionFilter, null, null, null);
        if (cursor == null || !cursor.moveToFirst()) {
            return contactMap;
        }

        // Get data from db lookup.
        String phoneNumber = getNumberFromContacts(cursor);
        String receiver = getReceiverFromContacts(cursor);
        cursor.close();

        // Update and return map;
        contactMap.put(KEY_RECEIVER_NAME, receiver);
        contactMap.put(KEY_PHONE_NUMBER, phoneNumber);
        return contactMap;
    }

    /**
     * Used to get phone number from the cursor lookup.
     */
    private String getNumberFromContacts(Cursor cursor) {
        int columnPhoneNumber = cursor
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
        return columnPhoneNumber != -1 ?
                cursor.getString(columnPhoneNumber):
                "";
    }

    /**
     * Used to get receiver name from the cursor lookup.
     */
    private String getReceiverFromContacts(Cursor cursor) {
        int columnReceiver = cursor
                .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        return columnReceiver != -1 ?
                cursor.getString(columnReceiver):
                "";
    }
}

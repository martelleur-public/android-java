package com.martelleur.sendsms;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.martelleur.sendsms.ui.PermissionWarningDialogFragment;
import com.martelleur.sendsms.ui.SendSmsFragment;


/**
 * Launch activity for the application. The activity is
 * used to launch the main fragment and update app permissions.
 */
public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSIONS = 1; // Used to control permission mgmt.
    private final String[] REQUIRED_PERMISSIONS = new String[] {
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_CONTACTS
    };

    /**
     * Launch main fragment if all necessary permission is allowed
     * else show permission warning dialog.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        boolean permissionsGranted = checkPermissions();
        if (permissionsGranted) {
            startMainFragment();
        } else {
            ActivityCompat.requestPermissions(this,
                    REQUIRED_PERMISSIONS,
                    REQUEST_PERMISSIONS);
        }
    }

    /**
     * Used to start main fragment.
     */
    private void startMainFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, SendSmsFragment.newInstance())
                .commitNow();
    }

    /**
     * Used to check if all necessary permissions is allowed.
     */
    private boolean checkPermissions() {
        boolean permissionsGranted = true;
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) !=
                    PackageManager.PERMISSION_GRANTED) {
                permissionsGranted = false;
                break;
            }
        }
        return permissionsGranted;
    }

    /**
     * Used to request all necessary permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean permissionsGranted = true;
        if (requestCode == REQUEST_PERMISSIONS) {
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    permissionsGranted = false;
                    break;
                }
            }
        }

        if (!permissionsGranted) {
            showWarningDialog();
        } else {
            startMainFragment();
        }
    }

    /**
     * Used to show permission warning dialog if not all
     * necessary permission is allowed.
     */
    private void showWarningDialog() {
        PermissionWarningDialogFragment warningFragment =
                PermissionWarningDialogFragment.newInstance();
        warningFragment.show(getSupportFragmentManager(), "Warning Dialog");
    }
}
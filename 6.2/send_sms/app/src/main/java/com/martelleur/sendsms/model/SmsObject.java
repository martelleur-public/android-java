package com.martelleur.sendsms.model;

/**
 * A class representing a sms object with
 * receiver name, phone number, and a sms body.
 */
public class SmsObject {
    private String receiverName;
    private String phoneNumber;
    private String body;

    public SmsObject(String receiverName, String phoneNumber, String body) {
        this.receiverName = receiverName;
        this.phoneNumber = phoneNumber;
        this.body = body;
    }

    /**
     * Default getters
     */
    public String getReceiverName() {
        return receiverName;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public String getBody() {
        return body;
    }

    /**
     * Default setters.
     */
    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

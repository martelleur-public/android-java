package com.martelleur.sendsms.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.martelleur.sendsms.R;
import com.martelleur.sendsms.databinding.FragmentSendSmsBinding;
import com.martelleur.sendsms.model.SmsObject;

/**
 * A fragment used for sending SMS.
 */
public class SendSmsFragment extends Fragment {
    private FragmentSendSmsBinding binding;
    private SendSmsViewModel sendSmsViewModel;

    /**
     * Used to create an instance of this fragment.
     */
    public static SendSmsFragment newInstance() {
        return new SendSmsFragment();
    }

    /**
     * Used to bind layout using view binding and setup view model.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentSendSmsBinding.inflate(inflater, container, false);
        sendSmsViewModel = getSendSmsViewModel();
        return binding.getRoot();
    }

    /**
     * Used to get the view model instance.
     */
    private SendSmsViewModel getSendSmsViewModel() {
        return new ViewModelProvider(this,
                ViewModelProvider.Factory.from(SendSmsViewModel.initializer))
                .get(SendSmsViewModel.class);
    }

    /**
     * When view is created setup:
     * 1) Update layout from view model.
     * 2) Set up send sms send listener.
     * 3) Set up change phone number manual listener.
     * 4) Set up get contact information listener to get
     *    number and name about receiver from contacts.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateLayoutMinHeight();
        updateLayoutFromViewModel();
        setupSendSmsListener();
        setupChangePhoneNumberManuallyListener();
        setupGetContactInfoListener();
    }

    /**
     * Used to update min height for constraint
     * layout inside a scrollview.
     */
    private void updateLayoutMinHeight() {
        if (binding.smsConstraintLayout == null) return;

        int screenHeight;
        WindowManager windowManager = (WindowManager) requireActivity().getSystemService(Context.WINDOW_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowMetrics windowMetrics = windowManager.getCurrentWindowMetrics();
            screenHeight = windowMetrics.getBounds().height();
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Display display = windowManager.getDefaultDisplay();
            display.getMetrics(displayMetrics); // populate DisplayMetrics object with default display.
            screenHeight = displayMetrics.heightPixels;
        }
        binding.smsConstraintLayout.setMinHeight(screenHeight);
    }

    /**
     * Used to update layout from view model
     */
    private void updateLayoutFromViewModel() {
        SmsObject smsObject = sendSmsViewModel.getSmsObject();
        binding.smsReceiverNumber.setText(smsObject.getPhoneNumber());
        binding.smsSendTitle.setText(getUpdatedTitle(smsObject.getReceiverName()));
        binding.smsContent.setText(smsObject.getBody());
    }

    /**
     * Use to update title when receiver from contacts is selected.
     */
    private String getUpdatedTitle(String receiverName) {
        String title;
        if (receiverName.equals("")) {
            title = getString(R.string.send_sms);
        } else {
            title = getString(R.string.send_sms_to_known_receiver, receiverName);
        }
        return title;
    }

    /**
     * Used to setup a listener for sending sms events using a sms manager.
     * OBS Android emulator: Use port number "adb devices -l".
     */
    private void setupSendSmsListener() {
        binding.sendSms.setOnClickListener(view -> {
            SmsManager smsManager = SmsManager.getDefault();

            String phoneNumber = binding.smsReceiverNumber.getText() != null ?
                    binding.smsReceiverNumber.getText().toString() : "";
            if (phoneNumber.equals("")) {
                Toast.makeText(requireContext(), R.string.enter_receiver, Toast.LENGTH_LONG).show();
                return;
            }

            String smsContent =  binding.smsContent.getText() != null ?
                    binding.smsContent.getText().toString() : "";
            if (smsContent.equals("")) {
                Toast.makeText(requireContext(), R.string.empty_sms, Toast.LENGTH_LONG).show();
                return;
            }

            smsManager.sendTextMessage(phoneNumber, null, smsContent, null, null);
            handleSmsSent(phoneNumber);
        });
    }

    /**
     * Used to reset sms view after sending SMS
     * and toast sms sent.
     */
    private void handleSmsSent(String phoneNumber) {
        binding.smsContent.setText("");
        binding.smsReceiverNumber.setText("");
        binding.smsSendTitle.setText(getString(R.string.send_sms));
        String msg = getString(R.string.sms_sent_to_phone_number, phoneNumber);
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Used to setup a listener for update phone number manually events.
     */
    private void setupChangePhoneNumberManuallyListener() {
        binding.smsReceiverNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Not implemented.
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Not implemented.
            }
            @Override
            public void afterTextChanged(Editable s) {
                String newPhoneNumber = s.toString();
                String savedPhoneNumber = sendSmsViewModel.getSmsObject().getPhoneNumber();
                if (!newPhoneNumber.equals(savedPhoneNumber)) {
                    sendSmsViewModel.updateReceiverPhoneNumber(newPhoneNumber);
                    sendSmsViewModel.updateReceiverName("");
                    binding.smsSendTitle.setText(getUpdatedTitle(sendSmsViewModel.getSmsObject().getReceiverName()));
                }
            }
        });
    }

    /**
     * Used to setup a listener for getting contact info events.
     */
    private void setupGetContactInfoListener() {
        binding.contacts.setOnClickListener(view -> {
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            contactPickerLauncher.launch(contactPickerIntent);
        });
    }

    /**
     * Used to register activity result launcher used when getting phone number from contacts.
     * A uri is then extracted from the result and then a Cursor is used for reading the database.
     */
    private final ActivityResultLauncher<Intent> contactPickerLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {
                    Uri contactUri = result.getData().getData(); // Contact URI from the intent result
                    sendSmsViewModel.updateSmsContactInfo(contactUri, requireContext().getContentResolver());
                    SmsObject smsObject = sendSmsViewModel.getSmsObject();
                    updateViewFromNewContactValues(smsObject.getReceiverName(),
                            smsObject.getPhoneNumber());
                }
            });

    /**
     * Update view from contact values.
     **/
    private void updateViewFromNewContactValues(String receiver, String phoneNumber) {
        binding.smsReceiverNumber.setText(phoneNumber);
        binding.smsSendTitle.setText(getUpdatedTitle(receiver));
    }

    /**
     * Used to save state of sms body to view model.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String body = binding.smsContent.getText() != null ?
                binding.smsContent.getText().toString()
                : "";
        sendSmsViewModel.updateSmsBody(body);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
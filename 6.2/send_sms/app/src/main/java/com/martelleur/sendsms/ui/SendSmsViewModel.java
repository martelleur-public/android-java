package com.martelleur.sendsms.ui;

import android.content.ContentResolver;
import android.net.Uri;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.martelleur.sendsms.infrastructure.ContactStore;
import com.martelleur.sendsms.model.SmsObject;
import com.martelleur.sendsms.model.SmsRepository;

/**
 * A view model storing the state of ui, specifically
 * the state of a new sms. The class use sms repository
 * to request contact data from underlying infrastructure.
 */
public class SendSmsViewModel extends ViewModel {

    private static SendSmsViewModel sendSmsViewModel = null;
    private final SmsObject smsObject = new SmsObject(
            "",
            "",
            "");
    private final SmsRepository smsRepository;
    public SendSmsViewModel(SmsRepository smsRepository) {
        this.smsRepository = smsRepository;
    }

    public void updateSmsContactInfo(Uri contactUri,
                                     ContentResolver contentResolver) {
        smsRepository.updateSmsObject(contactUri, contentResolver, smsObject);
    }

    public void updateSmsBody(String body) {
        this.smsObject.setBody(body);
    }

    public void updateReceiverName(String receiverName) {
        this.smsObject.setReceiverName(receiverName);
    }

    public void updateReceiverPhoneNumber(String phoneNumber) {
        this.smsObject.setPhoneNumber(phoneNumber);
    }

    public SmsObject getSmsObject() {
        return smsObject;
    }

    /**
     * Used to create an instance of this view model.
     * Ref: <a href="https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1">...</a>.
     */
    static final ViewModelInitializer<SendSmsViewModel> initializer = new ViewModelInitializer<>(
            SendSmsViewModel.class,
            creationExtras -> {
                ContactStore contactStore = new ContactStore();
                SmsRepository smsRepository = new SmsRepository(contactStore);
                if (sendSmsViewModel == null) {
                    sendSmsViewModel = new SendSmsViewModel(smsRepository);
                }
                return sendSmsViewModel;
            }
    );
}

package com.martelleur.receivesms.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.receivesms.R;
import com.martelleur.receivesms.infrastructure.SmsMessageWrapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * An adapter used when displaying sms items.
 */
public class SmsAdapter extends RecyclerView.Adapter<SmsAdapter.ViewHolder> {

    private final int layout;
    private List<SmsMessageWrapper> messages;
    private final ReceiveSmsCallback receiveSmsCallback;

    public SmsAdapter(int layoutId,
                      List<SmsMessageWrapper> messages,
                      ReceiveSmsCallback receiveSmsCallback) {
        this.layout = layoutId;
        this.messages = messages;
        this.receiveSmsCallback = receiveSmsCallback;
    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setGuestBooks(List<SmsMessageWrapper> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    /**
     * Inflate layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update sms view items.
     */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int listPosition) {
        SmsMessageWrapper message = messages.get(listPosition);
        String senderName = receiveSmsCallback.getContactName(message.getPhoneNumber());
        if (senderName == null) {
            holder.senderView.setText(R.string.unknown_sender);
        } else {
            holder.senderView.setText(senderName);
        }
        holder.numberView.setText(message.getPhoneNumber());
        holder.dateView.setText(getFormattedLocalDate(message.getTimeStamp()));
        holder.messageView.setText(message.getMessageBody());
    }

    /**
     * Used to get a local formatted date that is used as a
     * property for the sms items.
     */
    private String getFormattedLocalDate(long epochTime) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        Date date = new Date(epochTime);
        return formatter.format(date);
    }

    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView senderView;
        TextView numberView;
        TextView dateView;
        TextView messageView;
        ViewHolder(View itemView) {
            super(itemView);
            senderView = itemView.findViewById(R.id.sender);
            numberView = itemView.findViewById(R.id.number);
            dateView = itemView.findViewById(R.id.date);
            messageView = itemView.findViewById(R.id.message);
        }
    }
}

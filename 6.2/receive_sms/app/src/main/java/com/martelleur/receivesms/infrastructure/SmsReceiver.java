package com.martelleur.receivesms.infrastructure;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * A broad cast receiver
 * listening on incoming sms.
 */
public class SmsReceiver extends BroadcastReceiver {

    public static final String KEY_NEW_SMS = "com.martelleur.anvandanotifikationer.NEW_SMS";
    public static final String ACTION_SMS_RECEIVED = "com.martelleur.anvandanotifikationer.SMS_RECEIVED";

    /**
     * used to listen on intent with action "android.provider.Telephony.SMS_RECEIVED".
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        String validAction = Telephony.Sms.Intents.SMS_RECEIVED_ACTION; // "android.provider.Telephony.SMS_RECEIVED"
        if (action == null || !action.equals(validAction)) {
            return;
        }
        SmsMessage[] messages =  Telephony
                .Sms.Intents
                .getMessagesFromIntent(intent);
        SmsMessageWrapper[] wrapperMessages = getWrapperSmsMessages(messages);
        publishSmsReceived(wrapperMessages, context);
    }

    /**
     * Used to broadcast a received sms using a sms message
     * wrapper added to a bundle object.
     */
    private void publishSmsReceived(SmsMessageWrapper[] wrapperMessages,
                                    Context context) {

        Intent intent = new Intent(ACTION_SMS_RECEIVED);
        Bundle bundle = new Bundle();
        bundle.putParcelableArray(KEY_NEW_SMS, (Parcelable[]) wrapperMessages);
        intent.putExtras(bundle);
        context.sendBroadcast(intent);
    }

    /**
     * Used to wrap sms messages in a wrapper class that
     * implement parcelable.
     */
    private SmsMessageWrapper[] getWrapperSmsMessages(SmsMessage[] messages) {
        List<SmsMessageWrapper> wrapperMessages = new ArrayList<>();

        for (SmsMessage message : messages) {
            String phoneNumber = message.getOriginatingAddress();
            String messageBody = message.getMessageBody();
            long timeStamp = message.getTimestampMillis();
            wrapperMessages.add(new SmsMessageWrapper(
                    phoneNumber, messageBody, timeStamp));
        }

        return wrapperMessages.toArray(new SmsMessageWrapper[0]);
    }
}

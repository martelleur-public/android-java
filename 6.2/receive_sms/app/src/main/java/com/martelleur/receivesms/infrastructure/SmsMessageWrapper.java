package com.martelleur.receivesms.infrastructure;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * A wrapper class for sms messages used for sending sms
 * with an bundle object.
 */
public class SmsMessageWrapper implements Parcelable {
    private final String phoneNumber;
    private final String messageBody;

    private final long timeStamp;

    public SmsMessageWrapper(String phoneNumber,
                             String messageBody,
                             long timeStamp) {
        this.phoneNumber = phoneNumber;
        this.messageBody = messageBody;
        this.timeStamp = timeStamp;
    }

    protected SmsMessageWrapper(Parcel in) {
        phoneNumber = in.readString();
        messageBody = in.readString();
        timeStamp = in.readLong();
    }

    public static final Creator<SmsMessageWrapper> CREATOR = new Creator<SmsMessageWrapper>() {
        @Override
        public SmsMessageWrapper createFromParcel(Parcel in) {
            return new SmsMessageWrapper(in);
        }

        @Override
        public SmsMessageWrapper[] newArray(int size) {
            return new SmsMessageWrapper[size];
        }
    };

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public long getTimeStamp() { return timeStamp; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(phoneNumber);
        dest.writeString(messageBody);
        dest.writeLong(timeStamp);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}

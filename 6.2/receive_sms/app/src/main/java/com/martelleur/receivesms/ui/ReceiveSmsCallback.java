package com.martelleur.receivesms.ui;

/**
 * A callback interface used for communicating
 * between {@link ReceiveSmsFragment} and {@link SmsAdapter}.
 */
public interface ReceiveSmsCallback {
    String getContactName(String phoneNumber);
}

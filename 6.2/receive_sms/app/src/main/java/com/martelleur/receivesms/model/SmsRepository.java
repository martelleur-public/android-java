package com.martelleur.receivesms.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.martelleur.receivesms.infrastructure.SmsMessageWrapper;
import com.martelleur.receivesms.infrastructure.SmsReceiver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A repository used to listen on new received sms
 * sms messages.
 */
public class SmsRepository {

    private final MutableLiveData<List<SmsMessageWrapper>>
            smsMessagesLiveData = new MutableLiveData<>();

    public SmsRepository() {
        // private constructor
    }

    public LiveData<List<SmsMessageWrapper>> getSmsMessagesLiveData() {
        return smsMessagesLiveData;
    }


    /**
     * A broadcast receiver used to listen on new received sms.
     */
    public final BroadcastReceiver smsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean validAction = SmsReceiver.ACTION_SMS_RECEIVED.equals(intent.getAction());
            if (!validAction) return;

            Bundle bundle = intent.getExtras();
            if (bundle == null) return;

            Parcelable[] parcelableSms = getParcelableSms(bundle);
            if (getParcelableSms(bundle) != null) {
                SmsMessageWrapper[] wrapperSms = new SmsMessageWrapper[parcelableSms.length];
                for (int i=0; i <= parcelableSms.length -1; i++) {
                    wrapperSms[i] = (SmsMessageWrapper) parcelableSms[i];
                }
                updateSmsMessagesLiveData(wrapperSms);
            }
        }
    };

    /**
     * Used to get parcelable messages from a bundle object
     * dependent on different system API.
     */
    private Parcelable[] getParcelableSms(Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
            return bundle.getParcelableArray(
                    SmsReceiver.KEY_NEW_SMS, SmsMessageWrapper.class);
        } else {
            return bundle.getParcelableArray(SmsReceiver.KEY_NEW_SMS);
        }
    }

    /**
     * Used to update sms messages live data.
     */
    private void updateSmsMessagesLiveData(SmsMessageWrapper[] sms) {
        List<SmsMessageWrapper> currentSms = smsMessagesLiveData.getValue();
        if (currentSms == null) {
            currentSms = new ArrayList<>();
        }
        currentSms.addAll(0, Arrays.asList(sms));
        smsMessagesLiveData.setValue(currentSms);
    }
}

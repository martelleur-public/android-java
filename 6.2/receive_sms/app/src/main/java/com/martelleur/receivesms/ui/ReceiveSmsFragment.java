package com.martelleur.receivesms.ui;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.receivesms.R;
import com.martelleur.receivesms.databinding.FragmentReceiveSmsBinding;
import com.martelleur.receivesms.infrastructure.SmsMessageWrapper;
import com.martelleur.receivesms.infrastructure.SmsReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment used to display received sms.
 */
public class ReceiveSmsFragment extends Fragment {

    private ReceiveSmsViewModel receiveSmsViewModel;
    private FragmentReceiveSmsBinding binding;
    private final List<SmsMessageWrapper> messages = new ArrayList<>();
    private SmsAdapter smsAdapter;
    private BroadcastReceiver smsReceiver;

    public ReceiveSmsFragment() {
        // Required empty public constructor
    }

    /**
     * Used to create an instance of this fragment.
     */
    public static ReceiveSmsFragment newInstance() {
        return new ReceiveSmsFragment();
    }

    /**
     * Used to bind layout using view binding and setup view model.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentReceiveSmsBinding.inflate(inflater, container, false);
        receiveSmsViewModel = getReceiveSmsViewModel();
        smsReceiver = receiveSmsViewModel.getSmsReceiver();
        return  binding.getRoot();
    }

    /**
     * Used to get the view model instance.
     */
    private ReceiveSmsViewModel getReceiveSmsViewModel() {
        return new ViewModelProvider(this,
                ViewModelProvider.Factory.from(ReceiveSmsViewModel.initializer))
                .get(ReceiveSmsViewModel.class);
    }

    /**
     * When view is created setup:
     * 1) Recycle view for displaying received sms.
     * 2) Observer that updates the received sms when new sms are updated.
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupSmsRecycler();
        setupSmsObserver();
    }

    /**
     * Used to configure and setup a recycler view that display
     * sms items with an adapter.
     */
    private void setupSmsRecycler() {
        smsAdapter = new SmsAdapter(
                R.layout.sms_item,
                messages,
                receiveSmsCallback);
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setAdapter(smsAdapter);
    }

    /**
     * Callback used to get contact name if exist from a specif phone number.
     */
    private final ReceiveSmsCallback receiveSmsCallback = phoneNumber -> {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contactName = null;

        try (Cursor cursor = requireContext()
                .getContentResolver()
                .query(uri, projection, null, null, null)) {

            if (cursor != null && cursor.moveToFirst()) {
                int nameColumnIndex = cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
                contactName = nameColumnIndex >= 0 ? cursor.getString(nameColumnIndex) : null;
            }
        }
        return contactName;
    };

    /**
     * Used for setting upp an observer that updates the
     * received sms when new sms are updated.
     */
    private void setupSmsObserver () {
        receiveSmsViewModel.getSmsMessagesLiveData().observe(
                getViewLifecycleOwner(), messages ->
                        smsAdapter.setGuestBooks(messages));
    }

    /**
     * Used to register broadcast receiver for received sms.
     */
    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(
                SmsReceiver.ACTION_SMS_RECEIVED);
        requireContext().registerReceiver(smsReceiver, filter);
    }

    /**
     * Used to unregister broadcast receiver for received sms.
     */
    @Override
    public void onPause() {
        super.onPause();
        requireContext().unregisterReceiver(smsReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}

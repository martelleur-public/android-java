package com.martelleur.receivesms.ui;

import android.content.BroadcastReceiver;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import com.martelleur.receivesms.infrastructure.SmsMessageWrapper;
import com.martelleur.receivesms.model.SmsRepository;

import java.util.List;


/**
 * A view model used to communicate with repository
 * and receive updates sms.
 *
 * OBS! Sms from repository are updated from a broadcast
 * and are only stored per application session and not in a
 * persistent approach.
 */
public class ReceiveSmsViewModel extends ViewModel {
    private static ReceiveSmsViewModel receiveSmsViewModel;
    private final SmsRepository smsRepository;

    private ReceiveSmsViewModel (SmsRepository smsRepository) {
        this.smsRepository = smsRepository;
    }

    /**
     * Used to get broadcast receiver from repository.
     */
    public BroadcastReceiver getSmsReceiver() {
        return smsRepository.smsReceiver;
    }

    /**
     * Used to get sms livedata from repository.
     */
    public LiveData<List<SmsMessageWrapper>> getSmsMessagesLiveData() {
        return smsRepository.getSmsMessagesLiveData();
    }

    /**
     * Used to create an instance of this view model.
     * Ref: <a href="https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1">...</a>.
     */
    static final ViewModelInitializer<ReceiveSmsViewModel> initializer = new ViewModelInitializer<>(
            ReceiveSmsViewModel.class,
            creationExtras -> {
                SmsRepository smsRepository = new SmsRepository();
                if (receiveSmsViewModel == null) {
                    receiveSmsViewModel = new ReceiveSmsViewModel(smsRepository);
                }
                return receiveSmsViewModel;
            }
    );
}
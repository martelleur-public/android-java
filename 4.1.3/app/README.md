# Sända och ta emot epost

Prototypen introducerar till hur man sänder och tar emot epost.

## Prototyp

Gör först en app där användaren kan skriva in/ange:

- Mottagare
- Ämne
- Meddelande
- Bifogad fil

Detta ska sändas som epost till mottagaren vid ett tryck på en knapp eller liknande.

Frivillig överkurs är att även göra en annan app som kan ta emot och visa epost.

## Funktionalitet

Sänd meddelandet med en Intent.
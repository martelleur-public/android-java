package com.martelleur.sandaochtaemotepost;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.martelleur.sandaochtaemotepost.ui.EmailSenderFragment;

/**
 * Launch activity for the application. The activity is
 * used to launch the main fragment and update app permissions.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Launch fragment for recording and displaying video. Also, store
     * permission state in a view model.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, EmailSenderFragment.newInstance())
                    .commitNow();
        }
    }
}

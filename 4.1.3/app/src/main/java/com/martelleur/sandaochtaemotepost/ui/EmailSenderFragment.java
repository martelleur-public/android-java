package com.martelleur.sandaochtaemotepost.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.widget.Toast;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;

import java.util.Objects;

import com.martelleur.sandaochtaemotepost.databinding.FragmentEmailSenderBinding;

/**
 * A Fragment used for sending emails with implicit intent
 * for email and document handling.
 */
public class EmailSenderFragment extends Fragment {

    private FragmentEmailSenderBinding binding;
    private ActivityResultLauncher<Intent> emailLauncher;
    private ActivityResultLauncher<Intent> fileLauncher;
    private EmailViewModel emailViewModel;

    public EmailSenderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static EmailSenderFragment newInstance() {
        return new EmailSenderFragment();
    }

    /**
     * Used to bind layout using view binding and set upp activity result
     * launchers for adding attachment and sending email using explicit intents.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmailSenderBinding.inflate(inflater,container, false);
        emailViewModel = getEmailViewModel();
        emailLauncher = getEmailLauncher();
        fileLauncher = getFileLauncher();
        return binding.getRoot();
    }

    /**
     * Used to get the email view model instance.
     */
    private EmailViewModel getEmailViewModel() {
        return new ViewModelProvider(this,
                ViewModelProvider.Factory.from(EmailViewModel.initializer))
                .get(EmailViewModel.class);
    }

    /**
     * Used to create an "ActivityResultLauncher" for the email intent.
     * The result callback toast result to the user.
     */
    private ActivityResultLauncher<Intent> getEmailLauncher() {
        return registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
            renderEmailSent();
        });
    }

    /**
     * Used to render view after a email have been sent.
     */
    private void renderEmailSent() {
        String emptyText = "";
        emailViewModel.resetAttachmentUris();
        binding.receiver.setText(emptyText);
        binding.subject.setText(emptyText);
        binding.message.setText(emptyText);
        renderAttachments();
        Toast.makeText(getActivity(), "Email sent", Toast.LENGTH_SHORT).show();
    }

    /**
     * Used to create an "ActivityResultLauncher" to be used with
     * a document intent. The result callback adding selected
     * file to the attachment list.
     */
    private ActivityResultLauncher<Intent> getFileLauncher() {
        return registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
            if (result.getData() != null &&
                    result.getResultCode() == Activity.RESULT_OK) {
                Uri uri = result.getData().getData();
                emailViewModel.addAttachmentUri(uri);
                renderAttachments();
                String msg = "Attachment " + uri.getPath() + " added.";
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
            } else {
                String msg = "No attachment added.";
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Used to render the attachments to the screen.
     */
    private void renderAttachments() {
            StringBuilder uriPaths = new StringBuilder();
            emailViewModel.getAttachmentUris().forEach((uri ->
                    uriPaths.append(uri.getPath()).append(" ")));
            binding.attachmentList.setText(uriPaths);
    }

    /**
     * Used to setup listeners for adding attachment and sending the email.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        renderAttachments();
        setupAttachmentListener();
        setupSendEmailListener();
    }

    /**
     * Used to setup click listener for adding attachments.
     * The event handler is launching an implicit intent used
     * for selecting a document.
     */
    private void setupAttachmentListener() {
        binding.attachment.setOnClickListener( view -> {
            Intent documentIntent = getDocumentIntent();
            fileLauncher.launch(documentIntent);
        });
    }

    /**
     * Used to get an intent to be used for selecting document.
     */
    private Intent getDocumentIntent() {
        Intent documentIntent = new Intent(Intent.ACTION_GET_CONTENT);
        documentIntent.setType("*/*");
        return documentIntent;
    }

    /**
     * Used to setup click listener for sending the email.
     * The event handler is launching an implicit intent
     * with a chooser dialog used for email app selection
     * and sending a mail.
     */
    private void setupSendEmailListener() {
        binding.send.setOnClickListener(view -> {
            Intent emailIntent = getEmailIntent();
            emailLauncher.launch(Intent.createChooser(emailIntent, "Send Email"));
        });
    }

    /**
     * Used to get an intent to be used for email messaging.
     */
    private Intent getEmailIntent() {
        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE); // Multiple Attachments.
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL,
                new String[] { Objects.requireNonNull(binding.receiver.getText()).toString() });
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                Objects.requireNonNull(binding.subject.getText()).toString());
        emailIntent.putExtra(Intent.EXTRA_TEXT,
                Objects.requireNonNull(binding.message.getText()).toString());
        if (!emailViewModel.getAttachmentUris().isEmpty()) {
            emailIntent.putParcelableArrayListExtra(
                    Intent.EXTRA_STREAM, emailViewModel.getAttachmentUris());
        }
        return emailIntent;
    }
}

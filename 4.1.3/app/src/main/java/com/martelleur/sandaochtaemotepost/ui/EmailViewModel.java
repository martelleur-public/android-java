package com.martelleur.sandaochtaemotepost.ui;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.viewmodel.ViewModelInitializer;

import android.net.Uri;

import java.util.ArrayList;

/**
 * A email view model used for storing the state for the UI.
 */
public class EmailViewModel extends ViewModel {
    private ArrayList<Uri> attachmentUris = new ArrayList<>();

    /**
     * Used to adding a new attachment to the attachment uris.
     */
    public void addAttachmentUri(Uri attachment) {
        attachmentUris.add(attachment);
    }

    /**
     * Used to get the attachments.
     */
    public ArrayList<Uri> getAttachmentUris() {
        return attachmentUris;
    }

    /**
     * Used to reset attachments.
     */
    public void resetAttachmentUris() {
        attachmentUris = new ArrayList<>();
    }

    /**
     * Used to create an instance of this view model.
     * Ref: https://developer.android.com/topic/libraries/architecture/viewmodel/viewmodel-factories#java_1.
     */
    static final ViewModelInitializer<EmailViewModel> initializer = new ViewModelInitializer<>(
            EmailViewModel.class,
            creationExtras -> new EmailViewModel()
    );
}

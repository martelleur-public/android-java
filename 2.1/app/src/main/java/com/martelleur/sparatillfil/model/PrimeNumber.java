package com.martelleur.sparatillfil.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A Class representing a valid prime number with a date and an ID.
 */
public class PrimeNumber {
    private final long value;
    private final Date date;
    public static final long MIN_PRIME_NUMBER = 2L;

    public PrimeNumber(long value, Date date) throws NotAPrimeNumberException {
        if (!isPrimeNr(value)) {
            throw new NotAPrimeNumberException("Value must be a prime number");
        }
        this.value = value;
        this.date = date;
    }

    /**
     * Used to get the value of this prime number.
     */
    public long getValue() {
        return value;
    }

    /**
     * Used to get the date when this prime number where mined.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Used to get a formatted date when this prime number where mined.
     */
    public String getFormattedDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy-MM-dd-HH:mm:ss", Locale.getDefault());
        return formatter.format(date);
    }

    /**
     * Test if an number is a prime number.
     * @param value - the value of the number to be tested.
     * @return - true if the number is a prime number, otherwise it return false.
     */
    private boolean isPrimeNr (long value) {
        // Prime number can not be 0, 1, or negative.
        if (value < MIN_PRIME_NUMBER) {
            return false;
        }

        // Prime number can not be a product of two smaller numbers.
        double halfTheValue = value / 2d;
        for (long i = 2; i < halfTheValue; i++) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }
}

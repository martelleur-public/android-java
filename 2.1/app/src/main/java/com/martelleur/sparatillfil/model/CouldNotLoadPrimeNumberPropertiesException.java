package com.martelleur.sparatillfil.model;

public class CouldNotLoadPrimeNumberPropertiesException extends IllegalStateException {

    public CouldNotLoadPrimeNumberPropertiesException() {
        super("Could not load property file.");
    }
}

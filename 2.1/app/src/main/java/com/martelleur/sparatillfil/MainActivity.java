package com.martelleur.sparatillfil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.martelleur.sparatillfil.ui.PrimeNumberFragment;

/**
 * Launch activity for the application. The activity is
 * used to launch the main fragment.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, PrimeNumberFragment.newInstance())
                    .commitNow();
        }
    }
}
package com.martelleur.sparatillfil.ui;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.sparatillfil.model.CouldNotLoadPrimeNumberPropertiesException;
import com.martelleur.sparatillfil.model.PrimeNumber;
import com.martelleur.sparatillfil.model.PrimeNumberCallback;
import com.martelleur.sparatillfil.model.PrimeNumberExecutor;
import com.martelleur.sparatillfil.model.PrimeNumberMiner;
import com.martelleur.sparatillfil.model.PrimeNumberRepository;

import java.util.Date;

/**
 * A view model class used to store the state of the
 * UI and communicating with the repository for reading
 * and writing persistent data related to mining prime
 * numbers.
 */
public class PrimeNumberViewModel extends ViewModel {
    private final PrimeNumberRepository repository;
    public final MutableLiveData<PrimeNumber> primeNumberInfo =
            new MutableLiveData<>(new PrimeNumber(2L, new Date()));
    private final PrimeNumberExecutor primeNumberExecutor;
    private final PrimeNumberMiner miner;
    private final PrimeNumberCallback primeNumberCallback =
            primeNumberInfo::postValue;

    public PrimeNumberViewModel(PrimeNumberRepository repository,
                                PrimeNumberExecutor primeNumberExecutor,
                                PrimeNumberMiner miner) {
        this.repository = repository;
        this.primeNumberExecutor = primeNumberExecutor;
        this.miner = miner;
    }

    /**
     * Used to load the latest prime number.
     */
    public void loadLatestMinedPrimeNumber() {
        try {
            PrimeNumber primeNumberValueAndDate = repository.getLastMinedPrimeNumber();
            primeNumberInfo.setValue(primeNumberValueAndDate);
        } catch (CouldNotLoadPrimeNumberPropertiesException e) {
            String TAG = "PrimeNumberViewModel";
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * Used to start mining prime numbers.
     */
    public void startMinePrimeNumbers() {
        primeNumberExecutor.execute(miner);
        primeNumberExecutor.setPrimeNumberCallback(primeNumberCallback);
    }

    /**
     * Used to stop mining prime numbers.
     */
    public void stopMinePrimeNumbers() { primeNumberExecutor.stopExecute(); }

    /**
     * Used to control if to what/observe livedata
     * when mining prime number.
     */
    public void updateWatchPrimeNumberMining(boolean watchLiveData) {
        primeNumberExecutor.setObservePrimeNumberMining(watchLiveData);
    }

    /**
     * Used to get prime numbers livedata.
     */
    public LiveData<PrimeNumber> getPrimeNumberLiveData() {
        return primeNumberInfo;
    }
}
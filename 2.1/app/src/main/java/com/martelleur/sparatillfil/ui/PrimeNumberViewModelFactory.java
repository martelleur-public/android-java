package com.martelleur.sparatillfil.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.sparatillfil.model.PrimeNumberExecutor;
import com.martelleur.sparatillfil.model.PrimeNumberMiner;
import com.martelleur.sparatillfil.model.PrimeNumberRepository;


/**
 * Class used to build an instance of type [PrimeNumberViewModel].
 */
public class PrimeNumberViewModelFactory extends AbstractSavedStateViewModelFactory {

    private final PrimeNumberRepository repository = PrimeNumberRepository.getInstance();
    private final PrimeNumberExecutor primeNumberExecutor = new PrimeNumberExecutor();
    private final PrimeNumberMiner miner = new PrimeNumberMiner(repository);

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new PrimeNumberViewModel(repository, primeNumberExecutor, miner);
    }
}

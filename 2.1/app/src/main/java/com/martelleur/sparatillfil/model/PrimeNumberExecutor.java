package com.martelleur.sparatillfil.model;

import java.util.concurrent.Executor;

/**
 * A Executor class used for start and stop mining in a separated thread.
 */
public class PrimeNumberExecutor implements Executor {
    private PrimeNumberMiner miner;

    /**
     * Used to start mining prime numbers in a separated thread.
     */
    @Override
    public void execute(Runnable primeNumberMiner) {
        new Thread(primeNumberMiner).start();
        if (!(primeNumberMiner instanceof PrimeNumberMiner)) {
            throw  new IllegalArgumentException("Runner should be an instance of PrimeNumberRunner.");
        }
        this.miner = (PrimeNumberMiner) primeNumberMiner;

    }

    /**
     * Used to get live data result from mining of prime numbers.
     */
    public void setPrimeNumberCallback(PrimeNumberCallback callback) {
        miner.setPrimeNumberCallback(callback);
    }

    /**
     * Used to stop mining prime numbers.
     */
    public void stopExecute() {
        miner.stop();
    }

    /**
     * Used to slow down mining of prime numbers when a user watch live data update.
     */
    public void setObservePrimeNumberMining(boolean observeLiveData) {
        miner.setObservePrimeNumberMining(observeLiveData);
    }
}

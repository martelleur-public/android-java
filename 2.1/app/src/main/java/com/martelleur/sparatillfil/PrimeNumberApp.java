package com.martelleur.sparatillfil;

import android.app.Application;

import com.martelleur.sparatillfil.infrastructure.FileManager;
import com.martelleur.sparatillfil.model.PrimeNumberRepository;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Extending application and initialize PrimeNumberRepository.
 * This class is registered in the AndroidManifest.xml file, and an
 * instance of this class will be created when the application is started.
 * Ref: <a href="https://developer.android.com/guide/topics/manifest/application-element#nm">...</a>.
 */
public class PrimeNumberApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FileManager fileManager = new FileManager();
        PrimeNumberRepository.initialize(this, fileManager);
    }
}

package com.martelleur.sparatillfil.model;

import android.util.Log;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A Runnable class used for mining prime numbers.
 */
public class PrimeNumberMiner implements Runnable {
    public static final long ABSOLUT_START_VALUE = 0;
    private final String TAG = "PrimeNumberMiner";
    private long currentNumberToCheck = ABSOLUT_START_VALUE; // Used to check if it is a prime number.
    private PrimeNumber lastMinedPrimeNumber;
    private final AtomicBoolean isMining = new AtomicBoolean(false);
    private final PrimeNumberRepository repository;
    private final AtomicBoolean observePrimeNumberMining =
            new AtomicBoolean(false);
    private PrimeNumberCallback callback = primeNumber -> {};

    public PrimeNumberMiner (PrimeNumberRepository repository) {
        this.repository = repository;
        try {
            this.currentNumberToCheck = repository.getStartValueForMining();
            this.lastMinedPrimeNumber = repository.getLastMinedPrimeNumber();
        } catch (CouldNotLoadPrimeNumberPropertiesException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * Used to stop mining prime numbers.
     */
    public void stop() {
        isMining.set(false);
    }

    /**
     * Used to start mining prime numbers.
     */
    @Override
    public void run() {
        try {
            startMining();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Used to get live data result from mining of prime numbers.
     */
    public void setPrimeNumberCallback(PrimeNumberCallback callback) {
        this.callback = callback;
    }

    /**
     * Used to control state of mining when a user watch live data update or not.
     */
    public void setObservePrimeNumberMining(boolean observeLiveData) {
        this.observePrimeNumberMining.set(observeLiveData);
    }

    /**
     * Method used for mining/obtaining prime numbers by
     * testing if an number is a prime number.
     */
    private void startMining() throws InterruptedException {
        isMining.set(true);
        while (isMining.get()) {
            Log.d(TAG, "Checking value: " + currentNumberToCheck);
            try { // Only valid prime number can be created.
                Date date = new Date();
                lastMinedPrimeNumber = new PrimeNumber(currentNumberToCheck, date);
                notifyOnMinedPrimedNumber(); // Used when user watch live update.
            } catch (NotAPrimeNumberException ignored) {}
            repository.storeLastMinedPrimeNumberWithLastCheckedNumber(
                    lastMinedPrimeNumber,
                    currentNumberToCheck);
            currentNumberToCheck++;
        }
    }

    /**
     * Used to slow down mining and notify user when user watch live update.
     */
    private void notifyOnMinedPrimedNumber() throws InterruptedException {
        if (observePrimeNumberMining.get()) { // Used when user watch live update.
            Thread.sleep(50);
            callback.onMinedPrimedNumber(lastMinedPrimeNumber);
        }
    }
}

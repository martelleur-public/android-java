package com.martelleur.sparatillfil.model;

import android.app.Application;

import com.martelleur.sparatillfil.infrastructure.FileManager;

import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * A repository class used for accessing and writing persistent data.
 */
public class PrimeNumberRepository {
    private static PrimeNumberRepository INSTANCE;
    private final FileManager fileManager;
    private final String appStorageFile = "app_storage.env";
    private final String LAST_CHECKED_NUMBER_KEY = "LAST_CHECKED_NUMBER";
    private final String LAST_MINED_PRIME_NUMBER_KEY = "LAST_MINED_PRIME_NUMBER";
    private final String LAST_MINED_PRIME_NUMBER_DATE_KEY = "LAST_MINED_PRIME_NUMBER_DATE";
    private HashMap<String, String> lastMinedPrimeNumberWithLastCheckedNumber =
            new HashMap<>();

    private PrimeNumberRepository(Application application,
                                 FileManager fileManager) {
        this.fileManager = fileManager;
        fileManager.initialize(application, appStorageFile);
        loadLastMinedPrimeNumberAttempt();
    }

    public void loadLastMinedPrimeNumberAttempt () {
        try {
            lastMinedPrimeNumberWithLastCheckedNumber =
                    fileManager.getMapFromPropertyFile(appStorageFile);
        } catch (CouldNotLoadPrimeNumberPropertiesException | NullPointerException e) {
            lastMinedPrimeNumberWithLastCheckedNumber =
                    getMapFromPrimeNumberMiningStartValues();
        }
        controlPropertyFile();
    }

    private void controlPropertyFile () {
        if (!lastMinedPrimeNumberWithLastCheckedNumber.containsKey(LAST_CHECKED_NUMBER_KEY)) {
            lastMinedPrimeNumberWithLastCheckedNumber =
                    getMapFromPrimeNumberMiningStartValues();
        } else if (!lastMinedPrimeNumberWithLastCheckedNumber.containsKey(LAST_MINED_PRIME_NUMBER_KEY)) {
            lastMinedPrimeNumberWithLastCheckedNumber =
                    getMapFromPrimeNumberMiningStartValues();
        } else if (!lastMinedPrimeNumberWithLastCheckedNumber.containsKey(LAST_MINED_PRIME_NUMBER_DATE_KEY)) {
            lastMinedPrimeNumberWithLastCheckedNumber =
                    getMapFromPrimeNumberMiningStartValues();
        }
    }

    private HashMap<String, String> getMapFromPrimeNumberMiningStartValues() {
        HashMap<String, String> startValues = new HashMap<>();
        startValues.put(LAST_MINED_PRIME_NUMBER_KEY,
                String.valueOf(PrimeNumber.MIN_PRIME_NUMBER));
        startValues.put(LAST_MINED_PRIME_NUMBER_DATE_KEY,
                String.valueOf(new Date().getTime()));
        startValues.put(LAST_CHECKED_NUMBER_KEY,
                String.valueOf(PrimeNumberMiner.ABSOLUT_START_VALUE));
        return startValues;
    }

    /**
     * Store and cache last checked number and last mined prime number.
     */
    public void storeLastMinedPrimeNumberWithLastCheckedNumber(PrimeNumber lastMinedPrimeNumber,
                                                               long lastCheckedNumber) {
        cacheLastMinedPrimeNumber(lastMinedPrimeNumber, lastCheckedNumber);
        storeLastCachedMinedPrimeNumberWithLastCheckedNumber();
    }

    /**
     * Cache last checked number and last mined prime number.
     */
    private void cacheLastMinedPrimeNumber(PrimeNumber lastMinedPrimeNumber,
                                           long lastCheckedNumber) {
        lastMinedPrimeNumberWithLastCheckedNumber.replace(LAST_CHECKED_NUMBER_KEY,
                String.valueOf(lastCheckedNumber));
        lastMinedPrimeNumberWithLastCheckedNumber.replace(LAST_MINED_PRIME_NUMBER_KEY,
                String.valueOf(lastMinedPrimeNumber.getValue()));
        lastMinedPrimeNumberWithLastCheckedNumber.replace(LAST_MINED_PRIME_NUMBER_DATE_KEY,
                String.valueOf(lastMinedPrimeNumber.getDate().getTime()));
    }

    /**
     * Store last cached mined prime number and last checked number to file.
     */
    private void storeLastCachedMinedPrimeNumberWithLastCheckedNumber() {
        fileManager.writeMapToPropertyFile(lastMinedPrimeNumberWithLastCheckedNumber);
    }

    /**
     * Get last mined prime number from file.
     */
    public PrimeNumber getLastMinedPrimeNumber() throws CouldNotLoadPrimeNumberPropertiesException {
        long lastMinedPrimeNumberValue = getLastPrimeNumberValue();
        long lastMinedPrimeNumberDate = getLastPrimeNumberDate();
        return new PrimeNumber(lastMinedPrimeNumberValue,
                new Date(lastMinedPrimeNumberDate));
    }

    private long getLastPrimeNumberValue() throws CouldNotLoadPrimeNumberPropertiesException {
        return Long.parseLong(Objects.requireNonNull(
                lastMinedPrimeNumberWithLastCheckedNumber
                        .get(LAST_MINED_PRIME_NUMBER_KEY)));
    }

    private long getLastPrimeNumberDate() throws CouldNotLoadPrimeNumberPropertiesException {
        return Long.parseLong(Objects.requireNonNull(
                lastMinedPrimeNumberWithLastCheckedNumber
                        .get(LAST_MINED_PRIME_NUMBER_DATE_KEY)));
    }

    /**
     * Get start value for mining.
     */
    public long getStartValueForMining() {
        return getLastCheckedNumber() + 1;
    }

    /**
     * Get last checked number from file.
     */
    private long getLastCheckedNumber() throws CouldNotLoadPrimeNumberPropertiesException {
        return Long.parseLong(
                Objects.requireNonNull(
                        lastMinedPrimeNumberWithLastCheckedNumber.
                                get(LAST_CHECKED_NUMBER_KEY)));
    }

    /**
     * Use to initialize this repository.
     */
    public static void initialize(Application application,
                                  FileManager fileManager) {
        if (INSTANCE == null) {
            INSTANCE = new PrimeNumberRepository(application, fileManager);
        }
    }

    /**
     * Used to get a single instance of this repository.
     */
    public static PrimeNumberRepository getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("Prime number rpo not initialized.");
        }
        return INSTANCE;
    }
}

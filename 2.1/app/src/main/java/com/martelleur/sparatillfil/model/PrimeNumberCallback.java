package com.martelleur.sparatillfil.model;

public interface PrimeNumberCallback {
    void onMinedPrimedNumber(PrimeNumber primeNumber);
}

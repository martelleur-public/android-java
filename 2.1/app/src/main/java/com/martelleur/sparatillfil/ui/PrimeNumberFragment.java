
package com.martelleur.sparatillfil.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.martelleur.sparatillfil.R;
import com.martelleur.sparatillfil.databinding.PrimeNumberFragmentBinding;

/**
 * A fragment used to display mined prime numbers.
 */
public class PrimeNumberFragment extends Fragment {
    private PrimeNumberViewModel primeNumberViewModel;
    public PrimeNumberFragmentBinding binding;

    public PrimeNumberFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static PrimeNumberFragment newInstance() {
        return new PrimeNumberFragment();
    }


    /**
     * Used to get view model and bind layout using view binding.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        PrimeNumberViewModelFactory factory = new PrimeNumberViewModelFactory();
        primeNumberViewModel = new ViewModelProvider(requireActivity(), factory).get(PrimeNumberViewModel.class);
        binding = PrimeNumberFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * When view is created setup.
     * 1) Start mine prime numbers.
     * 2) Setup lister for watching live mining or not.
     * 3) Setup listener for getting latest mined prime number.
     * 4) Setup observer for prime number livedata.
     * 5) Load latest mined prime number.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        primeNumberViewModel.startMinePrimeNumbers();
        setupPrimeNumberLiveDataListener();
        setupPrimeNumberListener();
        setupPrimNumberObserver();
        primeNumberViewModel.loadLatestMinedPrimeNumber();
    }

    /**
     * Setup listener on live data update.
     */
    private void setupPrimeNumberLiveDataListener() {
        binding.primeNumberLiveResultCheckbox.setOnClickListener(view -> {
            if (view instanceof CheckBox) {
                boolean watchLiveData = ((CheckBox) view).isChecked();
                updateViewOnWatchPrimeNumberLiveData(watchLiveData);
            }
        });
    }

    /**
     * Used to update view if watch live data is on or off.
     */
    private void updateViewOnWatchPrimeNumberLiveData(boolean watchLiveData) {
        if (watchLiveData) {
            binding.viewPrimeNumber.setVisibility(View.INVISIBLE);
            showLiveDatUpdateInformation(true);
            primeNumberViewModel.updateWatchPrimeNumberMining(true);
        } else {
            binding.viewPrimeNumber.setVisibility(View.VISIBLE);
            showLiveDatUpdateInformation(false);
            primeNumberViewModel.updateWatchPrimeNumberMining(false);
        }
    }

    /**
     * Display information concerning live data update.
     */
    private void showLiveDatUpdateInformation (boolean livedataIsOn) {
        String msg = livedataIsOn ?
                getString(R.string.live_data_on_information) :
                getString(R.string.live_data_off_information);
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Used to setup listener for displaying latest mined prime number.
     */
    private void setupPrimeNumberListener() {
        binding.viewPrimeNumber.setOnClickListener(view -> {
            primeNumberViewModel.loadLatestMinedPrimeNumber();
        });
    }

    /**
     * Used to observe mined prime numbers.
     */
    private void setupPrimNumberObserver() {
        primeNumberViewModel.getPrimeNumberLiveData().observe(
                getViewLifecycleOwner(),
                primeNumber -> {
                    String outputPrimeNumberValue = String.valueOf(
                            primeNumber.getValue());
                    String outputPrimeNumberDate = getString(R.string.prime_number_content_date) +
                            " " + primeNumber.getFormattedDate();
                    binding.primeNumberContentValue.setText(outputPrimeNumberValue);
                    binding.primeNumberContentDate.setText(outputPrimeNumberDate);
                });
    }

    /**
     * Used to stop mine prime numbers.
     */
    @Override
    public void onSaveInstanceState (@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        primeNumberViewModel.stopMinePrimeNumbers();
    }

    /**
     * Used destroy the view binding.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
# Spara till fil

Prototypen introducerar till filhantering.

## Prototyp
Gör en app som försöker hitta större och större primtal. När ett nytt primtal är hittat så ska det lagras på fil. Enbart det senaste funna primtalet behöver lagras. Användaren ska ha möjlighet att se detta primtal. När appen startas så ska den fortsätta att leta där den var när appen stängdes.

## Funktionalitet

Ett primtal kan hittas genom att räkna upp en mängd ojämna tal och testa varje tal om det är ett primtal med:

```
private boolean isPrime(long candidate) {
  long sqrt = (long)Math.sqrt(candidate);
  for(long i = 3; i <= sqrt; i += 2)
    if(candidate % i == 0) return false;
  return true;
}
```

Ett alternativ är att använda klassen BigInteger:

```
BigInteger prime = new BigInteger(digits, 0, new Random());
Här anger digits hur många binära siffror som primtalet ska ha.```
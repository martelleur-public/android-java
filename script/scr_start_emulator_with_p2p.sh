#!/bin/bash


# Ref: https://android-developers.googleblog.com/2019/01/android-studio-33.html

EMULATOR=$1

if [ -z "$EMULATOR" ]; then
	echo "Input argument 1 should be an emulator."
	exit 0
fi


emulator -avd ${EMULATOR} -wifi-server-port 9998 -wifi-client-port 9999 &


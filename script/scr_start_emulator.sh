#!/bin/bash

EMULATOR=$1

if [ -z "$EMULATOR" ]; then
	echo "Input argument should be an emulator."
	exit 0
fi

emulator -avd $EMULATOR


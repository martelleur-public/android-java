#!/bin/bash

# Used to start app "com.martelleur.anvndadatabaskopplingar" on running emulator.

adb shell am start \
	-n "com.martelleur.anvndadatabaskopplingar/com.martelleur.anvndadatabaskopplingar.MainActivity" \
	-a android.intent.action.MAIN \
	-c android.intent.category.LAUNCHER

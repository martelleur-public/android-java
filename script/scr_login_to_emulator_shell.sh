#!/bin/bash

EMULATOR=$1

if [ -z "$EMULATOR" ]; then
	echo "Add emulator as first paramter."
	exit 0
fi

adb -s $EMULATOR shell

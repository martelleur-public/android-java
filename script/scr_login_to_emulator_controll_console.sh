#!/bin/bash

EMULATOR_PORT=$1

if [ -z "$EMULATOR_PORT" ]; then
	echo "Add emulator port number as first parameter."
	exit 0;
fi

telnet localhost $EMULATOR_PORT

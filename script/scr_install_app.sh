#!/bin/bash

# Emulator id can be found in extended-controls(three dots)->help->about.

BUNDLE_TOOL=$1
DEVICE_ID=$2
APKS_PATH=$3

# java -jar $BUNDLE_TOOL install-apks --apks={APKS_PATH} --device-id=${DEVICE_ID}

java -jar bundletool-all-1.13.2.jar install-apks --apks=app/release/app_v1.0.3.apks --device-id=emulator-5554

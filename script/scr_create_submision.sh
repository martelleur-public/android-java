#!/bin/bash

# TODO!
# I varje katalog ska enbart finnas egengjorda filer, bilder samt 
# eventuella hjälpmedel (som tex jar-filer eller jQuery), 
# för kursen Programmering för mobiler java-filer, AndroidManifest.xml och layout-xml-filer

# Submission dir
SUBMISSION_DIR=redovisning_joel_martelleur_MPROG

# Build a new sumbission directory.
if [ -d ${SUBMISSION_DIR} ]; then
    rm -Rf ${SUBMISSION_DIR}
fi
mkdir -p ${SUBMISSION_DIR}

# Task directories.
TASKS=(
"1.2/app/src/main"
"1.3"
"2.1/app/src/main"    
"2.2/app/src/main"
"3.1/app/src/main"
"3.2/app/src/main"
"3.3/app/src/main"    
"4.1.1/app/src/main"    
"4.1.2/app/src/main"    
"4.1.3/app/src/main"    
"4.1.4/app/src/main"    
"4.2.1/app/src/main"    
"4.2.2/app/src/main"    
"6.1/app/src/main"
"6.2/receive_sms/app/src/main"
"6.2/send_sms/app/src/main"
"7.1.1/app/src/main"
"7.2.1/app/src/main"
"7.2.2/app/src/main"
"7.3.1/app/src/main"    
"7.3.3/AnvandaWiFiP2P/app/src/main"
"Gesällprov/app/src/main"
)

# Create a submission directory for each task, and copy.
# Java, Resource, Assets, and AndroidManifest.
for TASK_DIR in "${TASKS[@]}"; do
    SUBMISSION_TASK_DIR=${SUBMISSION_DIR}/${TASK_DIR}
    mkdir -p ${SUBMISSION_TASK_DIR}
    cp -r ${TASK_DIR}/* ${SUBMISSION_TASK_DIR}
done

# Build files.
BUILD_FILES=(    
"1.2/app/build.gradle"
"2.1/app/build.gradle"    
"2.2/app/build.gradle"
"3.1/app/build.gradle"
"3.2/app/build.gradle"
"3.3/app/build.gradle"    
"4.1.1/app/build.gradle"    
"4.1.2/app/build.gradle"    
"4.1.3/app/build.gradle"    
"4.1.4/app/build.gradle"    
"4.2.1/app/build.gradle"    
"4.2.2/app/build.gradle"    
"6.1/app/build.gradle"
"6.2/receive_sms/app/build.gradle"
"6.2/send_sms/app/build.gradle"
"7.1.1/app/build.gradle"
"7.2.1/app/build.gradle"
"7.2.2/app/build.gradle"
"7.3.1/app/build.gradle"    
"7.3.3/AnvandaWiFiP2P/app/build.gradle"
"Gesällprov/app/build.gradle"
)

# Copy module build file for each task
# to submission directories.
for BUILD_FILE in "${BUILD_FILES[@]}"; do
    SUBMISSION_BUILD_FILE=${SUBMISSION_DIR}/${BUILD_FILE}
    cp ${BUILD_FILE} ${SUBMISSION_BUILD_FILE}
done

# Readme files, obs '1.3/README.md' already copied.
README_FILES=(    
"1.2/app/README.md"
"2.1/app/README.md"    
"2.2/app/README.md"
"3.1/app/README.md"
"3.2/app/README.md"
"3.3/app/README.md"    
"4.1.1/app/README.md"    
"4.1.2/app/README.md"    
"4.1.3/app/README.md"    
"4.1.4/app/README.md"    
"4.2.1/app/README.md"    
"4.2.2/app/README.md"    
"6.1/app/README.md"
"6.2/receive_sms/app/README.md"
"6.2/send_sms/app/README.md"
"7.1.1/app/README.md"
"7.2.1/app/README.md"
"7.2.2/app/README.md"
"7.3.1/app/README.md"    
"7.3.3/AnvandaWiFiP2P/app/README.md"
"Gesällprov/app/Joel_Martelleur_Gesällprov_MPROG.pdf"
"README.md"
)

# Copy readme file for each task
# to submission directories.
for README_FILE in "${README_FILES[@]}"; do
    SUBMISSION_README_FILE=${SUBMISSION_DIR}/${README_FILE}
    cp ${README_FILE} ${SUBMISSION_README_FILE}
done


# Zip dir using my name and cource code as name for zip-file.
zip -r joel_martelleur_redovisning_MPROG.zip ${SUBMISSION_DIR}

#!/bin/bash


# E.g., adb -s emulator-5556 emu sms send "+46744112538" "Meddelande 4"

# Check if argument 1 for android emulator is set.
if [ -z "$1" ]; then
      	echo "Argument 1 android emulator is not set."
	exit 1
fi

EMULATOR=$1
# Send 10 sms.
for i in {1..10}; do
	adb -s $EMULATOR emu sms send "+4674411253${i}" "Nytt meddelande ${i}"
done


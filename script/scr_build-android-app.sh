#!/bin/bash

BUMDLE 

PROJECT=$1
BUNDLE_PATH=$2
APK_PATH=$3
KEY_PATH=$4
KEY_ALIAS_NAME=$5
BUNDLE_TOOL='bundletool-all-1.13.2.jar'

# Create Android App Bundle bundle publishing format
cp $BUNDLE_TOOL $PROJECT
cd $PROJECT
./gradlew :app:bundle

# Create signed Android Package Kit installation format
java -jar $BUNDLE_TOOL build-apks --bundle=${BUNDLE_PATH} --output=${APK_PATH} --ks=${KEY_PATH} --ks-key-alias=${KEY_ALIAS_NAME}

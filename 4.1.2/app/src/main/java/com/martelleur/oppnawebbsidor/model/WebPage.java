package com.martelleur.oppnawebbsidor.model;

/**
 * A model class representing a webpage
 * with a title and an url.
 */
public class WebPage {
    private final String title;
    private final String url;

    public WebPage(String title, String url) {
        this.title = title;
        this.url = url;
    }

    /**
     * Used to get the title of the instance.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Used to get the url of the instance.
     */
    public String getUrl() {
        return url;
    }
}

package com.martelleur.oppnawebbsidor.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.oppnawebbsidor.R;
import com.martelleur.oppnawebbsidor.model.WebPage;

import java.util.List;

/**
 * An adapter used to display webpage items.
 */
public class WebPageListAdapter extends RecyclerView.Adapter<WebPageListAdapter.ViewHolder> {
    final private int layout;
    private final List<WebPage> webPageItems;
    private final WebPageCallback webPageCallback;

    public WebPageListAdapter(int layoutId,
                              List<WebPage> webPageItems,
                              WebPageCallback webPageCallback) {
        layout = layoutId;
        this.webPageItems = webPageItems;
        this.webPageCallback = webPageCallback;
    }

    /**
     * Used to get the number of items in the adapter.
     */
    @Override
    public int getItemCount() {
        return webPageItems == null ? 0 : webPageItems.size();
    }

    /**
     * Used to set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update webpage item views.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        WebView webPageView = holder.webPage;
        Button openWebPage = holder.openWebPage;
        WebPage webPageItem = webPageItems.get(listPosition);
        setupWebView(webPageView, webPageItem.getUrl());
        setupOpenWebPageTextContent(openWebPage, webPageItem.getTitle());
        setupOpenWebPageListener(openWebPage, webPageItem.getUrl());
    }

    /**
     * Used to set up a web view with a provided link.
     */
    private void setupWebView(WebView webView, String link) {
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        webView.loadUrl(link);
    }

    /**
     * Used to update text view by communicate back to the caller.
     */
    private void setupOpenWebPageTextContent(Button playButton, String videoName) {
        webPageCallback.updateOpenWebPageTextContent(playButton, videoName);
    }

    /**
     * Used to add a listener that communicate back to the caller that
     * play have been selected for a webpage item.
     */
    private void setupOpenWebPageListener(Button playButton, String url) {
        playButton.setOnClickListener(view -> {
            webPageCallback.onSelect(url);
        });
    }

    /**
     * A view holder that describes item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        WebView webPage;
        Button openWebPage;
        ViewHolder(View itemView) {
            super(itemView);
            webPage = itemView.findViewById(R.id.web_page);
            openWebPage = itemView.findViewById(R.id.open_webpage);
        }
    }
}

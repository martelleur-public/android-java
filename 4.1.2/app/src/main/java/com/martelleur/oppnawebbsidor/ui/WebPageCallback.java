package com.martelleur.oppnawebbsidor.ui;

import android.widget.Button;

/**
 * A callback interface used for communicating between
 * fragment {@link WebPageFragment} and adapter {@link WebPageListAdapter}.
 */
public interface WebPageCallback {
    void onSelect(String url);
    void updateOpenWebPageTextContent(Button openWebPage, String title);
}

package com.martelleur.oppnawebbsidor.ui;


import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.martelleur.oppnawebbsidor.R;
import com.martelleur.oppnawebbsidor.databinding.FragmentWebPageBinding;
import com.martelleur.oppnawebbsidor.model.WebPage;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A fragment used for displaying webpages that can
 * be selected to be viewed in a single page.
 */
public class WebPageFragment extends Fragment {
    private FragmentWebPageBinding binding;
    private WebPageListAdapter webPageListAdapter;
    static final String transactionFromWebPageFragmentToWebPageSingleFragmentFragment
            = "fromWebPageFragmentToWebPageSingleFragmentFragment";

    /* Static select webpages for testing the application */
    private final ArrayList<WebPage> webPageItems = new ArrayList<>(Arrays.asList(
            new WebPage("Eagle", "https://en.wikipedia.org/wiki/Eagle"),
            new WebPage("Ostrich", "https://en.wikipedia.org/wiki/Common_ostrich"),
            new WebPage("Albatross", "https://en.wikipedia.org/wiki/Albatross"),
            new WebPage("Eagle", "https://en.wikipedia.org/wiki/Eagle"),
            new WebPage("Ostrich", "https://en.wikipedia.org/wiki/Common_ostrich"),
            new WebPage("Albatross", "https://en.wikipedia.org/wiki/Albatross"),
            new WebPage("Eagle", "https://en.wikipedia.org/wiki/Eagle"),
            new WebPage("Ostrich", "https://en.wikipedia.org/wiki/Common_ostrich"),
            new WebPage("Albatross", "https://en.wikipedia.org/wiki/Albatross"),
            new WebPage("Eagle", "https://en.wikipedia.org/wiki/Eagle"),
            new WebPage("Ostrich", "https://en.wikipedia.org/wiki/Common_ostrich"),
            new WebPage("Albatross", "https://en.wikipedia.org/wiki/Albatross"),
            new WebPage("Eagle", "https://en.wikipedia.org/wiki/Eagle"),
            new WebPage("Ostrich", "https://en.wikipedia.org/wiki/Common_ostrich"),
            new WebPage("Albatross", "https://en.wikipedia.org/wiki/Albatross")
    ));

    public WebPageFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method to create a new instance of this fragment.
     */
    public static WebPageFragment newInstance() {
        return new WebPageFragment();
    }

    /**
     * Bind layout using view binding instance FragmentYoutubeBinding.
     */
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        binding = FragmentWebPageBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Create view model instance ...
     */
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupYoutubeRecycler();
    }

    /**
     * Set up a recycler view with an adapter
     * that displays youtube items.
     */
    private void setupYoutubeRecycler() {
        webPageListAdapter = new WebPageListAdapter(R.layout.webpage_item,
                webPageItems,
                webPageCallback);
        RecyclerView recyclerView = binding.recycler;
        int orientation = this.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        }
        recyclerView.setAdapter(webPageListAdapter);
    }

    /**
     * Callback used to open a youtube link and update play button names.
     */
    private final WebPageCallback webPageCallback = new WebPageCallback() {
        @Override
        public void onSelect(String url) {
            navigateToSingleWebPageFragment(url);
        }

        @Override
        public void updateOpenWebPageTextContent(Button openWebPage, String title) {
            openWebPage.setText(getString(R.string.open_webpage, title));
        }
    };

    /**
     * Used to navigate to SingleWebPageFragment.
     */
    private void navigateToSingleWebPageFragment(String url) {
        FragmentManager fragmentManager = getParentFragmentManager();
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, SingleWebPageFragment.newInstance(url))
                .addToBackStack(transactionFromWebPageFragmentToWebPageSingleFragmentFragment)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
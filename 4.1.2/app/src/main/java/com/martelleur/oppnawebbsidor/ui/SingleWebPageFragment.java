package com.martelleur.oppnawebbsidor.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.martelleur.oppnawebbsidor.databinding.FragmentSingleWebPageBinding;

/**
 * Fragment used for displaying a single webpage.
 */
public class SingleWebPageFragment extends Fragment {

    private static final String ARG_URL = "";
    private FragmentSingleWebPageBinding binding;
    private String url;

    public SingleWebPageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param url - An url to a webpage.
     * @return A new instance of fragment SingleWebPageFragment.
     */
    public static SingleWebPageFragment newInstance(String url) {
        SingleWebPageFragment fragment = new SingleWebPageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_URL, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            url = getArguments().getString(ARG_URL);
        }
    }

    /**
     * Used to set the layout of this fragment using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSingleWebPageBinding.inflate(inflater, container, false);
        setupWebView();
        return binding.getRoot();
    }

    /**
     * Used to setup a web view with a provided url.
     */
    private void setupWebView() {
        binding.webPage.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        binding.webPage.loadUrl(url);
    }

    /**
     * When view is created setup listener for
     * navigating back.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupOnNavigateBackListener();
    }

    /**
     * Add listener for navigating back from this fragment.
     */
    private void setupOnNavigateBackListener() {
        binding.returnFromYoutubeVideo.setOnClickListener(view -> {
            FragmentManager fragmentManager =  getParentFragmentManager();
            fragmentManager.popBackStack();
            fragmentManager.popBackStack(
                    WebPageFragment.transactionFromWebPageFragmentToWebPageSingleFragmentFragment,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
# Öppna webbsidor

Prototypen introducerar till hur man når webbsidor.

## Prototyp

Gör en app där användaren kan välja att klicka på en av flera länkar (eller knappar) och som vid ett klick öppnar respektive webbsida i ett fönster inom appen. Man ska alltså inte delegera vidare via en Intent den här gången.

## Funktionalitet

Använd WebView.
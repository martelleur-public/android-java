# Använda stream sockets

Chat applikation som anävnder server klient paradigmen med tcp.

## Starta Applikationen

- __Starta backend :__
    - Byt katalog: ```cd 4.2.1/app/backend```
    - Starta backend: ```java -jar Server.jar 2000```, där 2000 är ett angivet portnummer.
    - Tillåt nätverkskommunikation till servern, e.g: ```sudo ufw allow from 172.120.10.2 to any port 2000 proto tcp```

- __Starta frontend__
    - Starta android app: ```4.2.1/app/src/main```
    
- __Ange server parameterar i första fragment:__
    - ```Host```: Ipv4 address för servern, e.g. ```172.120.10.2``` 
    - ```Port```: Port för servern, e.g. ```2000```

## Prototyp

Gör en app som implementerar en chattklient som kan sända och ta emot textmeddelanden. Chattklienten ska koppla upp sig till en chattserver via en stream socket. När en chattklient sänder ett textmeddelande till chattservern så sänder chattservern detta textmeddelande till alla anslutna chattklienter. Chattklienten ska alltså både kunna:

Sända textmeddelanden från användaren till chattservern
Ta emot textmeddelanden från chattservern och visa dessa textmeddelanden för användaren
Frivillig utökning är att även ta med så att chattklienten klarar av om kopplingen bryts till chattservern (exempelvis om chattservern avslutas) genom att exempelvis meddela användaren och fråga om återanslutning skall försökas.

## Notis backend

Chattservern implementerar ett minimalt protkoll som man kan leka med. Om en chattklient skickar ```wwhhoo``` så returnerar chattservern alla uppkopplade chattklienters IP-adresser (den inleder svaret med ```WWHHOO:``` för varje chattklient).
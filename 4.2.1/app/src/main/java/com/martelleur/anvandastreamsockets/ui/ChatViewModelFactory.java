package com.martelleur.anvandastreamsockets.ui;

import androidx.annotation.NonNull;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvandastreamsockets.infrastructure.ChatSocket;
import com.martelleur.anvandastreamsockets.infrastructure.ChatSocketExecutor;
import com.martelleur.anvandastreamsockets.model.Chat;
import com.martelleur.anvandastreamsockets.model.ChatRepository;

/**
 * Used to create a ChatViewModel.
 */
public class ChatViewModelFactory extends AbstractSavedStateViewModelFactory {
    private final ChatRepository repository;

    public ChatViewModelFactory(String host, int port) {
        ChatSocketExecutor chatSocketExecutor = new ChatSocketExecutor();
        ChatSocket chatSocket = new ChatSocket(host, port);
        Chat chat = new Chat(host, port);
        chatSocket.addChatHandlerCallback(chat);
        this.repository = new ChatRepository(
                chat, chatSocket, chatSocketExecutor);
    }

    @SuppressWarnings("unchecked")
    @NonNull
    @Override
    protected <T extends ViewModel> T create(@NonNull String key, @NonNull Class<T> modelClass, @NonNull SavedStateHandle handle) {
        return (T) new ChatViewModel(repository);
    }
}

package com.martelleur.anvandastreamsockets.ui;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martelleur.anvandastreamsockets.R;
import com.martelleur.anvandastreamsockets.model.Message;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * An adapter used when displaying messages.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    final private int layout;
    private List<Message> messages;

    public MessagesAdapter(int layoutId,
                                List<Message> messages) {
        layout = layoutId;
        this.messages = messages;

    }

    /**
     * Update and notify the adapter that a change
     * have occurred.
     */
    public void setGuestBooks(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    /**
     * Used to get number of message items.
     */
    @Override
    public int getItemCount() {
        return messages == null ? 0 : messages.size();
    }

    /**
     * Used to set layout for the view holder.
     */
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(layout,
                parent, false);
        return new ViewHolder(view);
    }

    /**
     * Update message items.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {
        TextView messageTitle = holder.messageTitle;
        TextView messageContent = holder.messageContent;
        Message message = messages.get(listPosition);
        message.setTitle(getFormattedLocalDate());
        messageTitle.setText(message.getTitle());
        messageContent.setText(message.getContent());
    }

    /**
     * Used to get a local formatted date that is used as a
     * title for the messages.
     */
    private String getFormattedLocalDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "yyyy/MM/dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return formatter.format(date);
    }


    /**
     * A view holder that describes each item in the adapter.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView messageTitle;
        TextView messageContent;
        ViewHolder(View itemView) {
            super(itemView);
            messageTitle = itemView.findViewById(R.id.message_title);
            messageContent = itemView.findViewById(R.id.message_content);
        }
    }
}


package com.martelleur.anvandastreamsockets.model;


import com.martelleur.anvandastreamsockets.infrastructure.ChatSocket;
import com.martelleur.anvandastreamsockets.infrastructure.ChatSocketExecutor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Used as an abstract layer for chat socket operations.
 */
public class ChatRepository {

    private ChatSocket chatSocket;
    private Chat chat;
    private final ExecutorService executorService
            = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());
    private ChatSocketExecutor chatSocketExecutor;

    /**
     * Repository with ChatSocketExecutor and Context
     * start chat socket when created.
     */
    public ChatRepository(Chat chat,
                          ChatSocket chatSocket,
                          ChatSocketExecutor chatSocketExecutor) {
        this.chat = chat;
        this.chatSocket = chatSocket;
        this.chatSocketExecutor = chatSocketExecutor;
        chatSocketExecutor.execute(this.chatSocket);
    }

    /**
     * Used to get a new chat.
     */
    public Chat getChat() {
        return chat;
    }

    /**
     * Used to send a chat message on separated thread using a
     * executor service.
     */
    public void sendMessage(Message message) {
        Runnable sendMessage = () -> chatSocket.sendMessage(message.getContent());
        executorService.execute(sendMessage);
    }

    /**
     * Used to update the chat socket if new host or port is provided.
     */
    public void updateChat(String host, int port) {
        if (isNewChat(host, port)) {
            closeChat();
            chat = new Chat(host, port);
            chatSocket = new ChatSocket(host, port);
            chatSocket.addChatHandlerCallback(chat);
            chatSocketExecutor.execute(chatSocket);
        } else {
            chatSocketExecutor.restartSocket();
        }
    }

    /**
     * Used to check if new host or port is provided.
     */
    private boolean isNewChat(String host, int port) {
        return !host.equals(chatSocket.getHost()) ||
                port != chatSocket.getPort();
    }

    /**
     * Used to pause chat socket.
     */
    public void pauseChat() {
        chatSocketExecutor.pauseSocket();
    }

    /**
     * Used to close chat socket.
     */
    public void closeChat() {
        chatSocketExecutor.closeSocket();
    }
}

package com.martelleur.anvandastreamsockets.infrastructure;

import java.util.concurrent.Executor;

/**
 * Used to control, start and close, the chat socket.
 */
public class ChatSocketExecutor implements Executor {
    private ChatSocket chatSocket;

    /**
     * Used to start start chat socket in a separated thread.
     */
    @Override
    public void execute(Runnable chatSocket) {
        if (!(chatSocket instanceof ChatSocket)) {
            throw  new IllegalArgumentException("Runner should be an instance of ChatSocket.");
        }
        this.chatSocket = (ChatSocket) chatSocket;
        new Thread(this.chatSocket).start();
    }

    /**
     * Used to close chat socket.
     */
    public void closeSocket() {
        chatSocket.closeSocket();
    }

    /**
     * Used to pause chat socket.
     */
    public void pauseSocket() {
        chatSocket.pauseSocket();
    }

    /**
     * Used to restart chat socket.
     */
    public void restartSocket() {
        chatSocket.restartSocket();
    }
}

package com.martelleur.anvandastreamsockets.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.martelleur.anvandastreamsockets.R;
import com.martelleur.anvandastreamsockets.databinding.FragmentChatBinding;
import com.martelleur.anvandastreamsockets.model.Message;

import java.util.List;

/**
 * A fragment used to display a chat, chat messages
 * and write new messages.
 */
public class ChatFragment extends Fragment {
    FragmentChatBinding binding;
    ChatViewModel chatViewModel;
    private MessagesAdapter messagesAdapter;
    private static final String ARG_HOST = "chat_fragment_host";
    private static final String ARG_PORT = "chat_fragment_port";
    private String host = "";
    private String port = "";

    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters host and port.
     */
    public static ChatFragment newInstance(String host, String port) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_HOST, host);
        args.putString(ARG_PORT, port);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Used to restore host and port stored in bundle object.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            host = getArguments().getString(ARG_HOST);
            port = getArguments().getString(ARG_PORT);
        }
        chatViewModel = getChatViewModel();
        chatViewModel.updateChat(host, Integer.parseInt(port));
    }

    /**
     * Used to get an instance of chat view model.
     */
    private ChatViewModel getChatViewModel() {
        ChatViewModelFactory factory = new ChatViewModelFactory(host, Integer.parseInt(port));
        return new ViewModelProvider(requireActivity(), factory).get(ChatViewModel.class);
    }

    /**
     * Bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentChatBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    /**
     * Add listeners, observers
     * and a recycler view used for displaying messages.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setChatTitle();
        setupIsChatOpenObserver();
        setupChatErrorObserver();
        setupMessagesRecycler();
        setupMessagesObserver();
        setupSendMessageListener();
    }

    /**
     * Used to set a chat title.
     */
    private void setChatTitle() {
        String title = chatViewModel.getChatTitle();
        binding.chatTitle.setText(title);
    }

    /**
     * Used to observe chat connection status.
     */
    private void setupIsChatOpenObserver() {
        chatViewModel.isChatOpenLiveData().observe(
                requireActivity(),
                this::toastChatConnectionState);
    }

    /**
     * Used to toast chat connection status.
     */
    private void toastChatConnectionState(boolean isOpen) {
        String msg;
        String chatTitle = chatViewModel.getChatTitle();
        if (!isOpen) msg = getString(R.string.connecting_to_chat, chatTitle);
        else msg = getString(R.string.welcome_to_chat, chatTitle);
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Used to observe chat error messages.
     */
    private void setupChatErrorObserver() {
        chatViewModel.getChatErrorLiveData().observe(
                requireActivity(),
                this::toastChatErrorMsg);
    }

    /**
     * Used to toast chat error messages.
     */
    private void toastChatErrorMsg(String msg) {
        if (!msg.isEmpty()) {
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Used to configure and setup a recycler view that display
     * chat messages items with an adapter.
     */
    private void setupMessagesRecycler() {
        List<Message> messages = chatViewModel.getMessagesLiveData().getValue();
        messagesAdapter = new MessagesAdapter(
                R.layout.message_item, messages);
        RecyclerView recyclerView = binding.recycler;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(messagesAdapter);
        int size = messagesAdapter.getItemCount();
        recyclerView.smoothScrollToPosition(size);
    }

    /**
     * Update adapter and recycler when messages are updated.
     */
    private void setupMessagesObserver() {
        chatViewModel.getMessagesLiveData().observe(requireActivity(), messages -> {
            messagesAdapter.setGuestBooks(messages);
            scrollToLastMessage();
        });
    }

    /**
     * Used to scroll down to last message.
     */
    private void scrollToLastMessage() {
        try {
            int size = messagesAdapter.getItemCount();
            binding.recycler.smoothScrollToPosition(size);
        } catch (NullPointerException ignore) {}
    }

    /**
     * Add a click listener for sending a new message.
     */
    private void setupSendMessageListener() {
        binding.sendMessage.setOnClickListener(view -> {
            String message = getUserMessage();
            if (message.isEmpty()) toastMessageStatus();
            else chatViewModel.sendMessage(message);;
        });
    }

    private String getUserMessage() {
        String message = "";
        EditText input = binding.newMsgContainer.getEditText();
        if (input != null) {
            message = input.getText().toString();
        }
        return message;
    }

    /**
     * Toast a message to the user describing if the status of
     * the message.
     */
    private void toastMessageStatus() {
        Toast.makeText(requireContext(),
                getString(R.string.no_empty_message),
                Toast.LENGTH_LONG).show();
    }

    /**
     * Used to save port and host to bundle object.
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_HOST, host);
        outState.putString(ARG_PORT, port);
    }

    /**
     * Use to pause chat when fragment is destroyed.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        chatViewModel.pauseChat();
        binding = null;
    }
}
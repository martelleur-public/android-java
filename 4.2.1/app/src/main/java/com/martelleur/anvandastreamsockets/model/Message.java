package com.martelleur.anvandastreamsockets.model;

/**
 * A class used to represent a chat message
 * with a title and content.
 */
public class Message {
    private final String content;
    private String title = "";

    public Message(String content) {
        this.content = content;
    }

    /**
     * Used to get message content.
     */
    public String getContent() { return content; }

    /**
     * Used to get title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Used to set title.
     */
    public void setTitle(String title) {
        if (this.title.isEmpty()) {
            this.title = title;
        }
    }
}

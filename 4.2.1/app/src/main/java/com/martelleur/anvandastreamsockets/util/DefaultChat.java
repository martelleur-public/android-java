package com.martelleur.anvandastreamsockets.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Utility class used to load default chat properties.
 */
public class DefaultChat {
    private final String host;
    private final String port;

    public DefaultChat (Context context) {
        Properties defaultChat = loadChatProperties(context);
        this.host = defaultChat.getProperty("chat.host");
        this.port = defaultChat.getProperty("chat.port");
    }

    /**
     * Used to load api properties from api property file.
     */
    private Properties loadChatProperties(Context context) {
        Properties properties;
        String CHAT_PROPERTY_FILE = "chat.properties";
        try {
            properties = new Properties();
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(CHAT_PROPERTY_FILE);
            properties.load(inputStream);

        } catch (IOException e) {
            throw new RuntimeException("Could not load property: " + CHAT_PROPERTY_FILE);
        }
        return properties;
    }

    /**
     * Used to get default host.
     */
    public String getHost() {
        return this.host;
    }

    /**
     * Used to get default port.
     */
    public String getPort()  {
        return this.port;
    }
}

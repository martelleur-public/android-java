package com.martelleur.anvandastreamsockets.ui;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.martelleur.anvandastreamsockets.R;
import com.martelleur.anvandastreamsockets.databinding.FragmentStartChatBinding;
import com.martelleur.anvandastreamsockets.util.DefaultChat;

import java.util.Objects;

/**
 * A fragment used to start a chat with default
 * host and port or user defined host and port.
 */
public class StartChatFragment extends Fragment {
    FragmentStartChatBinding binding;
    public StartChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    public static StartChatFragment newInstance() {
        return new StartChatFragment();
    }

    /**
     * Bind layout using view binding.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentStartChatBinding.inflate(inflater, container, false);
        return  binding.getRoot();
    }

    /**
     * Get view model instance with factory class, add listeners,
     * and a recycler view used for displaying messages.
     */
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupNavigateToChatListener();
    }

    /**
     * Used to navigate to default chat or chat with
     * user defined host and port.
     */
    private void setupNavigateToChatListener() {
        DefaultChat defaultChat = new DefaultChat(requireContext());
        String defaultHost = defaultChat.getHost();
        String defaultPort = defaultChat.getPort();
        binding.startChat.setOnClickListener(view -> {
            try {
                String host = Objects.requireNonNull(binding.hostInput.getText()).toString();
                String port = Objects.requireNonNull(binding.portInput.getText()).toString();
                if (port.isEmpty() || host.isEmpty()) {
                    navigateToChat(defaultHost, defaultPort);
                }
                else { navigateToChat(host, port); }
            } catch (NullPointerException e) {
                navigateToChat(defaultHost, defaultPort);
            }
        });
    }

    /**
     * Used to navigate to chat with user defined host and port.
     */
    private void navigateToChat(String host, String port) {
        int enter = R.anim.slide_in;
        int exit = R.anim.fade_out;
        int popEnter = R.anim.fade_in;
        int popExit = R.anim.slide_out;
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager
                .beginTransaction()
                .setCustomAnimations(enter, exit, popEnter, popExit)
                .add(R.id.container, ChatFragment.newInstance(host, port))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
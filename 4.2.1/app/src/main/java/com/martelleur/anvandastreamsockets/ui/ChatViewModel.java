package com.martelleur.anvandastreamsockets.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.martelleur.anvandastreamsockets.model.Chat;
import com.martelleur.anvandastreamsockets.model.ChatRepository;
import com.martelleur.anvandastreamsockets.model.Message;

import java.util.List;

/**
 * View model class used to keep current
 * state of the ui. The class is also used to
 * communicate app functionality with the chat repository
 * for network communication (chatting).
 */
public class ChatViewModel extends ViewModel {
    private Chat chat;
    private final ChatRepository repository;

    ChatViewModel(ChatRepository repository) {
        this.repository = repository;
        chat = this.repository.getChat();
    }

    public String getChatTitle() {
        return chat.getTitle();
    }

    /**
     * Used to observe if chat is open.
     */
    public MutableLiveData<Boolean> isChatOpenLiveData() {
        return chat.isConnectedLiveData();
    }

    /**
     * Used to observe if chat have error.
     */
    public MutableLiveData<String> getChatErrorLiveData() {
        return chat.getErrorLiveData();
    }

    /**
     * Used to get observable chat messages.
     */
    public LiveData<List<Message>> getMessagesLiveData() {
        return chat.getMessagesLiveData();
    }

    /**
     * Used to send a chat message.
     */
    public void sendMessage(String content) {
        Message msg = chat.createNewMsg(content);
        repository.sendMessage(msg);
    }

    /**
     * Used to update the chat with new host and port.
     */
    public void updateChat(String host, int port) {
        repository.updateChat(host, port);
        chat = this.repository.getChat();
    }

    /**
     * Used to pause the chat.
     */
    public void pauseChat() {
        repository.pauseChat();
    }
}

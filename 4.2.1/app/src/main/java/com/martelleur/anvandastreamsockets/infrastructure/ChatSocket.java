package com.martelleur.anvandastreamsockets.infrastructure;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A Chat socket used to communicate with the chat server, e.g., broadcast
 * messages and listen on incoming messages.
 */
public class ChatSocket implements Runnable {
    private final String HOST;
    private final int PORT;
    private Socket socket;
    private PrintWriter socketWriter;
    private BufferedReader socketReader;
    private final AtomicBoolean socketIsOpen = new AtomicBoolean(false);
    private ChatHandlerCallback chatHandlerCallback = null;

    /**
     * Used to initiate user defined HOST
     * and PORT for the socket.
     */
    public ChatSocket(String host, int port)  {
        HOST = host;
        PORT = port;
    }

    /**
     * Used to setup a callback used when for incoming messages.
     */
    public void addChatHandlerCallback(ChatHandlerCallback chatHandlerCallback) {
        this.chatHandlerCallback = chatHandlerCallback;
    }

    /**
     * Used to listen and read incoming messages
     * while the socket is open
     */
    @Override
    public void run() {
        openSocket();
        while (socketIsOpen.get()) {
            String message = "";
            try {
                message = this.socketReader.readLine();
            } catch (NullPointerException | IOException e) {
                closeSocket();
            }
            chatHandlerCallback.handleIncomingMsg(message);
        }
    }

    /**
     * Used to open socket on given host and port.
     */
    private void openSocket() {
        try {
            socket = new Socket(HOST, PORT);
            socketWriter = new PrintWriter(socket.getOutputStream(), true);
            socketReader = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
            socketIsOpen.set(true);
            chatHandlerCallback.handleOnConnected(socketIsOpen.get());
        } catch (IOException e) {
            closeSocket();
            chatHandlerCallback.handleOnConnectionError(HOST, PORT);
        }
    }

    /**
     * Used to send a chat message.
     */
    public void sendMessage(String message) {
        if (socketIsOpen.get()) {
            try {
                this.socketWriter.println(message);
            } catch (NullPointerException e) {
                chatHandlerCallback.handleOnConnectionError(HOST, PORT);
            }
        }
    }

    /**
     * Used to get the host of this socket.
     */
    public String getHost() {
        return HOST;
    }

    /**
     * Used to get the port of this socket.
     */
    public int getPort() {
        return PORT;
    }

    /**
     * Use to pause the socket.
     */
    public void pauseSocket() {
        socketIsOpen.set(false);
    }

    /**
     * Use to restart the socket.
     */
    public void restartSocket() {
        socketIsOpen.set(true);
    }

    /**
     * Used to close socket resources.
     */
    public void closeSocket() {
        socketIsOpen.set(false);
        String TAG = "ChatSocket";
        try {
            if (socket != null) {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            }
            if (socketReader != null) { socketReader.close(); }
            if (socketWriter != null) { socketWriter.close();}
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }
}

package com.martelleur.anvandastreamsockets;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.martelleur.anvandastreamsockets.ui.StartChatFragment;


/**
 * Launch activity for the application.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Used to launch main fragment.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, StartChatFragment.newInstance())
                    .commitNow();
        }
    }
}
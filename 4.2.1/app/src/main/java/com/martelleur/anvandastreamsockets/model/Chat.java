package com.martelleur.anvandastreamsockets.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.martelleur.anvandastreamsockets.infrastructure.ChatHandlerCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A class used to represent a chat.
 * This class is implementing ChatHandlerCallback
 * used in ChatSocket when incoming messages arrives
 * on the chat socket.
 */
public class Chat implements ChatHandlerCallback {
    private final String chatTitle;
    private final MutableLiveData<List<Message>> messagesLiveData = new MutableLiveData<>(new ArrayList<>());
    private final MutableLiveData<Boolean> isConnectedLiveData = new MutableLiveData<>(false);
    private final MutableLiveData<String> errorLiveData = new MutableLiveData<>("");

    public Chat (String host, int port) {
        this.chatTitle = "Chat@" + host + ":" + port;
    }

    /**
     * Used to get the chat title ."Chat@inet:port".
     */
    public String getTitle() {
        return chatTitle;
    }

    /**
     * Used to get chat messages.
     */
    public LiveData<List<Message>> getMessagesLiveData() { return messagesLiveData; }

    /**
     * Used to get connection status.
     */
    public MutableLiveData<Boolean> isConnectedLiveData() {
        return isConnectedLiveData;
    }

    /**
     * Used to get error status.
     */
    public MutableLiveData<String> getErrorLiveData() {
        return errorLiveData;
    }

    /**
     * Used to create new messages.
     */
    public Message createNewMsg(String content) {
        return new Message(content);
    }

    /**
     * Used to handle incoming messages.
     */
    @Override
    public void handleIncomingMsg(String content) {
        Message message = new Message(content);
        List<Message> messages = messagesLiveData.getValue();
        Objects.requireNonNull(messages).add(message);
        messagesLiveData.postValue(messages);
    }

    /**
     * Used to handle and update connection status.
     */
    @Override
    public void handleOnConnected(boolean isConnected) {
        this.isConnectedLiveData.postValue(isConnected);
    }

    /**
     * Used to handle and update connection error status.
     */
    @Override
    public void handleOnConnectionError(String host, int port) {
        String msg = "Could not connect to chat " + host + ":" + port;
        this.errorLiveData.postValue(msg);
    }
}

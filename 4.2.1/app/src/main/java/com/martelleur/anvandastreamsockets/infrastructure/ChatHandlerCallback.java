package com.martelleur.anvandastreamsockets.infrastructure;

/**
 * Callback interface used to communicate between
 * {@link ChatSocket} and {@link com.martelleur.anvandastreamsockets.model.Chat}.
 */
public interface ChatHandlerCallback {
    void handleIncomingMsg(String content);
    void handleOnConnected(boolean isConnected);
    void handleOnConnectionError(String host, int port);
}
